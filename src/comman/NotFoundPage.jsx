import React from "react";
import logo from "../assets/images/logo.png";

export default function NotFoundPage() {
  return (
    <div className="text-center">
      <img
        src={logo}
        style={{ width: "200px", height: "100px", objectFit: "contain" }}
      />
      <h1>Page Not Found</h1>
    </div>
  );
}
