import React from 'react';

import Select from 'react-select';
import makeAnimated from 'react-select/animated';


export const colourOptions = [
    { value: 'African', label: 'African' },
    { value: 'American', label: 'American' },
    { value: 'Argentinian', label: 'Argentinian' },
    { value: 'Asian', label: 'Asian' },
    { value: 'Australian', label: 'Australian' },
    { value: 'British', label: 'British', },
    { value: 'Caribbean', label: 'Caribbean', },
    { value: 'Chinese', label: 'Chinese', },
    { value: 'European', label: 'European', },
    { value: 'French', label: 'French', },
    { value: 'Greek', label: 'Greek', },
    { value: 'Indian', label: 'Indian', },
    { value: 'International', label: 'International', },
    { value: 'Italian', label: 'Italian', },
    { value: 'Japanese', label: 'Japanese', },
    { value: 'Korean', label: 'Korean', },
    { value: 'Malaysian', label: 'Malaysian', },
    { value: 'Mediterranean', label: 'Mediterranean', },
    { value: 'Mexican', label: 'Mexican', },
    { value: 'Middle', label: 'Middle', },
    { value: 'Western', label: 'Western', },
    { value: 'Pan-Asian', label: 'Pan-Asian', },
    { value: 'Peruvian', label: 'Peruvian', },
    { value: 'Pizza', label: 'Pizza', },
    { value: 'Portuguese', label: 'Portuguese', },
    { value: 'Sea Food,', label: 'Sea Food,', },
    { value: 'Spanish', label: 'Spanish', },
    { value: 'Steak', label: 'Steak', },
    { value: 'Sushi', label: 'Sushi', },
    { value: 'Thai', label: 'Thai', },
    { value: 'Turkish', label: 'Turkish', },
];

const animatedComponents = makeAnimated();

export default function AnimatedMulti(props) {
    const { getData, initialData } = props
    console.log(initialData)
    return (
        <div style={{ display: 'flex', flexDirection: 'column' }}>
            <span>Select Tags</span>
            <Select
                closeMenuOnSelect={true}
                components={animatedComponents}
                defaultValue={initialData ? [...initialData] : [colourOptions[1]]}
                isMulti
                onChange={(e) => getData(e)}
                options={colourOptions}
            />
        </div>
    );
}