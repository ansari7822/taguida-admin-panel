import React from "module";
import TextField from "@mui/material/TextField";
import FormControl from "@mui/material/FormControl";
import PropTypes from "prop-types";
import InputAdornment from "@mui/material/InputAdornment";
// import Visibility from "@mui/icons-material/Visibility";
// import VisibilityOff from "@mui/icons-material/VisibilityOff";
import IconButton from "@mui/material/IconButton";
import PlacesAutocomplete from "react-places-autocomplete";
import AccountCircle from "@mui/icons-material/AccountCircle";

import {
  geocodeByAddress,
  geocodeByPlaceId,
  getLatLng,
} from "react-places-autocomplete";

//----------------------------------------------------//
//-------------------- TEXT FIELD ---------------------//
//----------------------------------------------------//
const TEXTFIELD = (props) => {
  const {
    label,
    type,
    onChange,
    className,
    name,
    margin,
    Icon,
    MarkunreadIcon,
    value,
    helperText,
    notShowIcon,
    errors,
  } = props;
  return (
    <FormControl
      fullWidth
      sx={{ m: 1 }}
      variant="standard"
      className="btn_padding_none"
    >
      {/* <InputLabel htmlFor="standard-adornment-amount">Email</InputLabel> */}
      <TextField
        label={label}
        type={type}
        name={name}
        value={value}
        InputLabelProps={{ style: { fontSize: 17 } }}
        onChange={(event) => onChange(event)}
        onFocus={(event) => onChange(event)}
        onBlur={(event) => onChange(event)}
        className={className}
        margin={margin}
        InputProps={{
          startAdornment: (
            <InputAdornment>
              {!notShowIcon && (
                <IconButton className="business_bt">
                  {Icon ? <Icon /> : <AccountCircle />}
                </IconButton>
              )}
            </InputAdornment>
          ),
        }}
        {...props}
        error={errors[name] && errors[name].length > 0 ? true : false}
        helperText={errors[name] && errors[name].length > 0 ? errors[name] : ""}
        fullwidth
      />
    </FormControl>
  );
};

TEXTFIELD.defaultProps = {
  type: "text",
  className: "input-text",
  variant: "standard",
  label: "Text Input",
  name: "",
  margin: "normal",
  value: "",
  helperText: "",
  errors: {},
};
TEXTFIELD.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

const PASSWORDFIELD = (props) => {
  const {
    label,
    type,
    onChange,
    className,
    name,
    margin,
    value,
    helperText,
    Icon,
    errors,
  } = props;
  return (
    <FormControl
      fullWidth
      sx={{ m: 1 }}
      variant="standard"
      className="btn_padding_none"
    >
      <TextField
        label={label}
        type={type}
        name={name}
        value={value}
        InputLabelProps={{ style: { fontSize: 17 } }}
        onChange={(event) => onChange(event)}
        onFocus={(event) => onChange(event)}
        onBlur={(event) => onChange(event)}
        className={className}
        margin={margin}
        InputProps={{
          startAdornment: (
            <InputAdornment>
              <IconButton>{Icon ? <Icon /> : <AccountCircle />}</IconButton>
            </InputAdornment>
          ),
        }}
        {...props}
        error={errors[name] && errors[name].length > 0 ? true : false}
        helperText={errors[name] && errors[name].length > 0 ? errors[name] : ""}
        fullwidth
      />
    </FormControl>
  );
};
PASSWORDFIELD.defaultProps = {
  className: "input-text",
  label: "",
  fieldName: "",
  margin: "normal",
  value: "",
  note: "",
  edit: false,
  variant: "standard",
};

PASSWORDFIELD.propTypes = {
  classes: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.func.isRequired,
};

const GoogleAutocomplete = (props) => {
  const {
    address,
    lat,
    lng,
    onChange,
    label,
    className,
    margin,
    type,
    errors,
    helperText,
    name,
  } = props;
  const handleChange = (address) => {
    if (address == "") {
      onChange({ address: address, lat: "", lng: "" });
    } else {
      onChange({ address: address, lat: lat, lng: lng });
    }
  };
  const handleSelect = (address) => {
    geocodeByAddress(address)
      .then((results) => {
        return getLatLng(results[0]);
      })
      .then((latLng) => {
        onChange({
          address: address,
          lat: latLng.lat,
          lng: latLng.lng,
        });
      })
      .catch((error) => {
        onChange({
          address: "NA",
          lat: "NA",
          lng: "NA",
        });
      });
  };

  return (
    <PlacesAutocomplete
      value={address}
      onSelect={handleSelect}
      onChange={handleChange}
    >
      {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
        <>
          <TextField
            label={label}
            type={type}
            InputLabelProps={{ style: { fontSize: 17 } }}
            fullWidth
            error={errors[name] && errors[name].length > 0 ? true : false}
            helperText={
              errors[name] && errors[name].length > 0
                ? errors[name]
                : helperText
            }
            className={className}
            {...props}
            margin={margin}
            {...getInputProps({
              placeholder: "Search Places ...",
              className: "location-search-input",
            })}
          />
          <div className="autocomplete-dropdown-container">
            {loading && <div>Loading...</div>}
            {suggestions.map((suggestion) => {
              const className = suggestion.active
                ? "suggestion-item--active"
                : "suggestion-item";
              // inline style for demonstration purpose
              const style = suggestion.active
                ? { backgroundColor: "#fafafa", cursor: "pointer" }
                : { backgroundColor: "#ffffff", cursor: "pointer" };
              return (
                <div
                  {...getSuggestionItemProps(suggestion, {
                    className,
                    style,
                  })}
                >
                  <span>{suggestion.description}</span>
                </div>
              );
            })}
          </div>
        </>
      )}
    </PlacesAutocomplete>
  );
};
GoogleAutocomplete.defaultProps = {
  type: "text",
  className: "input-text",
  variant: "standard",

  label: "Text Input",
  name: "",
  margin: "normal",
  value: "",
  helperText: "",
  errors: {},
};

export { TEXTFIELD, PASSWORDFIELD, GoogleAutocomplete };
