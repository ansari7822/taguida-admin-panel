import React from "react";
import toast, { Toaster } from "react-hot-toast";

class Toastt {
  success = (message) => {
    return (
      <>
        {toast.success(message, {
          duration: 3000,
          position: "top-right",
        })}
      </>
    );
  };
  error = (message) => {
    return (
      <>
        {toast.error(message, {
          duration: 3000,
          position: "top-right",
        })}
      </>
    );
  };
}
export default new Toastt();
