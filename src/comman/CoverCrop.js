import React, { useEffect, useRef, useState } from "react"
import { Modal, Alert, Spinner } from "reactstrap"
import Cropper from "react-cropper"
import "cropperjs/dist/cropper.css"

const CoverCrop = props => {
  const { isOpen, toggle, image, onCoverCrop, imgRatio } = props
  const cropperRef = useRef(null)
  const onCrop = () => {
    const imageElement = cropperRef?.current
    const cropper = imageElement?.cropper

    if (!cropper) return
    onCoverCrop(cropper.getCroppedCanvas().toDataURL(), true)
    toggle()
  }

  return (
    <React.Fragment>
      <Modal
        id="adddeliveruadd"
        size="xl"
        style={{ position: 'relative', zIndex: 677676 }}
        isOpen={isOpen}
        autoFocus={true}
        centered={true}
        toggle={() => toggle()}
        className="oder-details-dialog pb-4"
        wrapClassName="myaddreess-modal"
        contentClassName="create-story-main border-0 rounded-top"
        fade
      >
        <div className="modal-header list-head bookmark-col crate-post-upload rounded-top">
          <div>
            <h4 className="modal-title" id="staticBackdropLabel">
              Preivew
            </h4>
          </div>
          <button
            type="button"
            className="btn-close"
            data-bs-dismiss="modal"
            aria-label="Close"
            onClick={() => toggle()}
          ></button>
        </div>

        <div className="modal-body crate-post-upload mb-4 mt-1">
          <div className="clearfix crop_div">
            <button className="btn btn-pink mb-2" onClick={onCrop}>
              Done
            </button>
          </div>

          {image && (
            <Cropper
              ref={cropperRef}
              style={{ height: 400, width: "100%", objectFit: "contain" }}
              src={image.preview}
              initialAspectRatio={imgRatio}
              guides={true}
              viewMode={2}
              zoomable={true}
              center={false}
              aspectRatio={imgRatio}
            // minCropBoxHeight={720}
            // minCropBoxWidth={1280}
            />
          )}
        </div>
      </Modal>
    </React.Fragment>
  )
}

export default CoverCrop