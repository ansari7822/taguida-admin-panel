import React from 'react'
import Pagination from "@mui/material/Pagination";
import Stack from "@mui/material/Stack";

export default function PaginationCom({ count, getNextPage, loading, usersList }) {
    return (
        <Stack spacing={2} className="pagination_area">
            <Pagination
                count={count}
                // defaultPage={1}
                onChange={getNextPage}
                variant="outlined"
                shape="rounded"
            />
            {loading && usersList.length > 0 && (
                <p className="m-0 p-0 text-muted">Loading...</p>
            )}
        </Stack>
    )
}
