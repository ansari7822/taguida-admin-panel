const Loading = (props) => {
  return (
    <>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        style={{
          margin: "auto",
          background: "none",
          position: props.absolute ? props.absolute : "relative",
          display: "block",
          top: "25%",
        }}
        width="100px"
        height="100px"
        viewBox="0 0 100 100"
        preserveAspectRatio="xMidYMid"
      >
        <g transform="translate(20 50)">
          <circle cx={0} cy={0} r={6} fill="#000000">
            <animateTransform
              attributeName="transform"
              type="scale"
              begin="-0.375s"
              calcMode="spline"
              keySplines="0.3 0 0.7 1;0.3 0 0.7 1"
              values="0;1;0"
              keyTimes="0;0.5;1"
              dur="1s"
              repeatCount="indefinite"
            />
          </circle>
        </g>
        <g transform="translate(40 50)">
          <circle cx={0} cy={0} r={6} fill="#000000">
            <animateTransform
              attributeName="transform"
              type="scale"
              begin="-0.25s"
              calcMode="spline"
              keySplines="0.3 0 0.7 1;0.3 0 0.7 1"
              values="0;1;0"
              keyTimes="0;0.5;1"
              dur="1s"
              repeatCount="indefinite"
            />
          </circle>
        </g>
        <g transform="translate(60 50)">
          <circle cx={0} cy={0} r={6} fill="#000000">
            <animateTransform
              attributeName="transform"
              type="scale"
              begin="-0.125s"
              calcMode="spline"
              keySplines="0.3 0 0.7 1;0.3 0 0.7 1"
              values="0;1;0"
              keyTimes="0;0.5;1"
              dur="1s"
              repeatCount="indefinite"
            />
          </circle>
        </g>
        <g transform="translate(80 50)">
          <circle cx={0} cy={0} r={6} fill="#000000">
            <animateTransform
              attributeName="transform"
              type="scale"
              begin="0s"
              calcMode="spline"
              keySplines="0.3 0 0.7 1;0.3 0 0.7 1"
              values="0;1;0"
              keyTimes="0;0.5;1"
              dur="1s"
              repeatCount="indefinite"
            />
          </circle>
        </g>
      </svg>
      <p
        className="text-center w-100 text-danger"
        style={{ paddingBottom: "-10px" }}
      >
        <b>{props?.deleting}</b>
      </p>
    </>
  );
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      style={{
        margin: "auto",
        // background: "rgb(241, 242, 243)",
        position: props.absolute ? props.absolute : "relative",
        display: "block",
      }}
      width="200px"
      height="200px"
      viewBox="0 0 100 100"
      preserveAspectRatio="xMidYMid"
    >
      <circle
        cx={50}
        cy={50}
        r={0}
        fill="none"
        stroke="#ebca44"
        strokeWidth={3}
      >
        <animate
          attributeName="r"
          repeatCount="indefinite"
          dur="1.0526315789473684s"
          values="0;26"
          keyTimes="0;1"
          keySplines="0 0.2 0.8 1"
          calcMode="spline"
          begin="0s"
        />
        <animate
          attributeName="opacity"
          repeatCount="indefinite"
          dur="1.0526315789473684s"
          values="1;0"
          keyTimes="0;1"
          keySplines="0.2 0 0.8 1"
          calcMode="spline"
          begin="0s"
        />
      </circle>
      <circle
        cx={50}
        cy={50}
        r={0}
        fill="none"
        stroke="#ebca44"
        strokeWidth={3}
      >
        <animate
          attributeName="r"
          repeatCount="indefinite"
          dur="1.0526315789473684s"
          values="0;26"
          keyTimes="0;1"
          keySplines="0 0.2 0.8 1"
          calcMode="spline"
          begin="-0.5263157894736842s"
        />
        <animate
          attributeName="opacity"
          repeatCount="indefinite"
          dur="1.0526315789473684s"
          values="1;0"
          keyTimes="0;1"
          keySplines="0.2 0 0.8 1"
          calcMode="spline"
          begin="-0.5263157894736842s"
        />
      </circle>
    </svg>
  );
};
export default Loading;
