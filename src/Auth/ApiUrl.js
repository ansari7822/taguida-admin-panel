// Api BASE URLs
// http://13.58.48.196:3001

export const API_URL = "http://api.taguida.com";
export const API_VERSION = "/api";
export const APP_NAME = "Taguida";
export const PANEL_NAME = "Admin";

//Admin or Login
export const AdminLogin = "/users/adminlogin"

//Upload image
export const UploadImage = "/images/upload"

//Users
export const GetUsersList = "/users"
export const GetUserById = "/users/"

//Businesses
export const GetBusinessesList = "/businesses"
export const GetBusinesseById = "/businesses/"
export const CreateBusinessByUser = "/users/create/business"

// categories
export const GetCategories = "/categories"
export const GetCategoriesById = "/categories/"
export const CreateCategory = "/categories"
export const CreateSubCategory = "/subCategories"
export const GetSubCategoryById = "/subCategories/"

//social hub
export const GetsocialHubVideosList = "/socialHubVideos"
export const PostsocialHubTypes = "/socialHubTypes"
export const GetsocialHubTypesById = "/socialHubTypes/"

//Date ideas
export const Add_Ideas = "/ideas"
export const GET_IDEAS_LIST = "/ideas"
export const GET_IDAES_ById = "/ideas/"

//Place
export const Add_Place = "/places"
export const GET_PLACE_LIST = "/places"
export const GET_PLACE_ById = "/places/"

//Business Panel
export const GET_BOOKING_LIST = "/businessBookings"
export const GET_REVIEWS = "/businessReviews?"

//Send Custom Notification
export const SEND_NOTIFICATOINS = "/users/send/notification"











