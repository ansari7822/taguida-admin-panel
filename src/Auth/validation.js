import React from 'react'

export default function validation(values) {
    let errors = {}
    if (!values.name) {
        errors.name = "Name is Required"
    }
    if (!values.businessName) {
        errors.businessName = "Business Name is Required"
    }
    if (!values.email) {
        errors.email = "Email is Required"
    } else if (/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(values.email)) {
        errors.email = "Email is in valid"
    }
    if (!values.password) {
        errors.password = "password is Required"
    } else if (values.password.length < 6) {
        errors.password = "password must be 6 Required"
    }
    if (!values.phone) {
        errors.phone = "phone is Required"
    }
    if (!values.countryCode) {
        errors.countryCode = "countryCode rquie"
    }
    return errors
}
