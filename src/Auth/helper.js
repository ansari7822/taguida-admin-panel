export const checkValidations = state => {
  let newState = {};
  let noTrim = ["undefined", "object", "boolean", "number"];
  let error = "";
  for (let [key, value] of Object.entries(state)) {
    if (!noTrim.includes(typeof value)) {
      value = value.trim();
      if (value === "") {
        key = key.charAt(0).toUpperCase() + key.slice(1);
        error += `${key},`;
      }
      newState[key] = value;
    } else {
      newState[key] = value;
    }
  }
  if (error !== "") {
    error = error.substring(0, error.length - 1);
    error = "All fields are required!";
  } else {
    error = false;
  }
  return { data: newState, error: error };
};

export const checkError = (errors, state) => {
  console.log(state, "-090-9")
  let err = "";
  let disabled = false;
  for (var key of Object.keys(errors)) {
    if (errors[key].length > 0) {
      err = "All fields are required!";
      disabled = true;
    }
  }
  for (var key of Object.keys(state)) {
    if (!state[key]) {
      disabled = true;
    }
  }
  return { error: err, disabled };
};

// export const addBusiness = (errors) => {
//   if (errors.email || !state.email) {
//     toast.error(errors.email, {
//       duration: 4000,
//       position: "top-right",
//     });
//   }
//   if (errors.password) {
//     toast.error(errors.password, {
//       duration: 4000,
//       position: "top-right",
//     });
//   }
//   if (errors.phone) {
//     toast.error(errors.phone, {
//       duration: 4000,
//       position: "top-right",
//     });
//   }
//   if (errors.name) {
//     toast.error(errors.name, {
//       duration: 4000,
//       position: "top-right",
//     });
//   }
// };
