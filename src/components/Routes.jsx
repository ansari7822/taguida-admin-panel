import React from "react";

import { Route, Switch } from "react-router-dom";

import Users from "../pages/users";
import Businesses from "../pages/businesses";
import SocicalHub from "../pages/socical-hub";
import Category from "../pages/category";
import Myplaces from "../pages/myplaces";
import NotFoundPage from "../comman/NotFoundPage";
import Ideas from "../pages/ideas";
import Places from "../pages/places";
import Bookings from "../pages/myplaces/component/Users";
import Reviews from "../pages/myplaces/component/Reviews";

const Routes = (props) => {
  let role = localStorage.getItem("role");
  return (
    <Switch>
      {role == "admin" ? (
        <>
          <Route path="/app/users" component={Users} />
          <Route path="/app/businesses" component={Businesses} />
          <Route path="/app/categories" component={Category} />
          <Route path="/app/social-hub" component={SocicalHub} />
          <Route path="/app/ideas" component={Ideas} />
          <Route path="/app/places" component={Places} />
        </>
      ) : (
        <>
          <Route path="/app/my-business" component={Myplaces} />
          <Route path="/app/bookings" component={Bookings} />
          <Route path="/app/reviews" component={Reviews} />
        </>
      )}
      <Route component={NotFoundPage} />
    </Switch>
  );
};

export default Routes;
