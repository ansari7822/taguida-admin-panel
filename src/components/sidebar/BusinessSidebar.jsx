import React, { useState } from "react";

import { Link, withRouter, useHistory } from "react-router-dom";

import "./sidebar.css";

import logo from "../../assets/images/logo.png";

import sidebar_items from "../../assets/JsonData/Business_sidebar_routes.json";

const SidebarItem = (props) => {
  const history = useHistory();
  const active = history.location.pathname.includes(props.title)
    ? "active"
    : "";

  console.log(props.title, "props.title");
  return (
    <div className={`sidebar__item ${active}`}>
      <div className="sidebar__item-inner position-relative ">
        <i className={props.icon}></i>
        <span className={active ? "active_sidebar" : "d-none"}>active</span>
        <span>{props.title}</span>
      </div>
    </div>
  );
};

const Sidebar = (props) => {
  const activeItem = sidebar_items.findIndex(
    (item) => item.route === props.location.pathname
  );
  const [activeIndex, setactiveIndex] = useState(0);
  console.log(activeIndex, activeItem, "");
  return (
    <div className="sidebar">
      <div className="sidebar__logo">
        <img src={logo} alt="company logo" />
      </div>
      {sidebar_items.map((item, index) => (
        <Link to={item.route} key={index}>
          <SidebarItem
            title={item.display_name}
            icon={item.icon}
            active={index === activeItem}
          />
        </Link>
      ))}
    </div>
  );
};

export default withRouter(Sidebar);
