import React, { useState, useEffect } from "react";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import "./topnav.css";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Popover from "@mui/material/Popover";
import { Link, withRouter } from "react-router-dom";
import Dropdown from "../dropdown/Dropdown";
import Loader2 from "../../comman/Loader2";
import ThemeMenu from "../thememenu/ThemeMenu";
import PersonIcon from "@mui/icons-material/Person";
import MessageIcon from "@mui/icons-material/Message";
import notifications from "../../assets/JsonData/notification.json";
import user_image from "../../assets/images/m_userimg@2x.png";
import user_menu from "../../assets/JsonData/user_menus.json";
import toast, { Toaster } from "react-hot-toast";
import { GetUsersList } from "../../Auth/ApiUrl";
import Axios from "../../Auth/Axios";
import { Co2Sharp } from "@mui/icons-material";
import { TEXTFIELD } from "../../comman/inputeFields";
import Loading from "../../comman/Loading";
import { SEND_NOTIFICATOINS } from "../../Auth/ApiUrl";
import { Modal } from "react-bootstrap";

const curr_user = {
  display_name: "Dev",
  image: user_image,
};

const renderNotificationItem = (item, index) => (
  <div className="notification-item" key={index}>
    <i className={item.icon}></i>
    <span>{item.content}</span>
  </div>
);

const renderUserToggle = (user) => (
  <div className="topnav__right-user">
    <div className="topnav__right-user__image">
      <img src={user.image} alt="" />
    </div>
    {/* <div className="topnav__right-user__name">{user.display_name}</div> */}
  </div>
);

const renderUserMenu = (item, index) => (
  <Link to="/" key={index}>
    <div className="notification-item">
      <i className={item.icon}></i>
      <span>{item.content}</span>
    </div>
  </Link>
);

const Topnav = (props) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [loader, setloader] = useState(false);
  const [fillterValue, setfillterValue] = useState("");
  const [status, setstatus] = useState("");
  const [disabled, setDisabled] = useState(true);
  const [loading, setloading] = useState(false);
  const [addNewBusiness, setaddNewBusiness] = useState(false);
  const [state, setState] = useState({
    title: "",
    message: "",
    errors: {
      title: "",
      message: "",
    },
  });
  const {
    searchBar,
    selectAll,
    sendNotificationBtn,
    routeText,
    navigationText,
  } = props;
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const logout = () => {
    localStorage.clear();
    props.history.push("/login");
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  const handleChange = (e) => {
    e.preventDefault();
    const { name, value } = e.target;
    if (name == "fillterValue") {
      setfillterValue(value);
    }
    if (name == "status") {
      setstatus(value);
    }
  };
  const handleChange2 = (e) => {
    e.preventDefault();
    let errors = state.errors;
    setState((prev) => ({ ...prev, errors, [e.target.name]: e.target.value }));
  };
  useEffect(() => {
    if (state.title && state.message) {
      setDisabled(false);
    }
  }, [state]);
  const { fillterUserListFun, setgetprevioususerListFun } = props;
  const fillterSearch = () => {
    let newUrl = props.ApiURL + `?name=${fillterValue}&status=${status}`;
    setloader(true);
    Axios.get(newUrl)
      .then((res) => {
        if (res) {
          console.log(res, "908098");
          fillterUserListFun(res.data);
        }
        setloader(false);
      })

      .catch((err) => {
        setloader(false);
        console.log(err, "er");
      });
  };
  const { setgetprevious } = props;
  const closeFillter = () => {
    setgetprevious();
    setfillterValue("");
  };
  const addBusinessCall = () => {
    const { title, message } = state;
    let data = {
      title,
      message,
    };

    Axios.post(SEND_NOTIFICATOINS, data)
      .then((res) => {
        console.log(res.data, "9090890890890890");
        if (res.data.isSuccess == true) {
          toast.success("Notification Send  Successfully", {
            duration: 4000,
            position: "top-right",
          });
          setTimeout(() => {
            setaddNewBusiness(false);
            setState("");
          }, 100);
        } else {
          toast.error(res.data.error, {
            duration: 4000,
            position: "top-right",
          });
        }
        setloading(false);
        console.log(res, "data");
      })
      .catch((err) => {
        setloading(false);
        return toast.success("Something went wrong", {
          duration: 4000,
          position: "top-right",
        });
      });
  };
  const { title, message, errors } = state;
  return (
    <div className="topnav">
      <Modal
        size="md"
        show={addNewBusiness}
        onHide={() => setaddNewBusiness(false)}
        centered
        className="business_modal"
        aria-labelledby="example-modal-sizes-title-sm"
      >
        <div
          className="business_modal_container"
          style={{ borderRadius: "60px" }}
        >
          <div className="add_new_business_form">
            <h5>Send Custom Notification</h5>
            <span>Enter the Custom notification details below</span>
          </div>
          {loading && <Loading />}
          <div className="business_input_field">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Title"
              name="title"
              value={title}
              onChange={handleChange2}
              notShowIcon={true}
              // placeholder="Name"
              errors={errors}
            />
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Message"
              name="message"
              value={message}
              onChange={handleChange2}
              // placeholder="Business Name"
              notShowIcon={true}
              errors={errors}
            />
          </div>
          <div className="btn_div ">
            <button
              className="business_btn2"
              onClick={() => setaddNewBusiness(false)}
            >
              Back
            </button>
            <Button
              className="business_btn "
              disabled={disabled}
              style={{ backgroundColor: "#1d1d1d", color: "white" }}
              onClick={addBusinessCall}
            >
              Send
            </Button>
          </div>
        </div>
      </Modal>
      <div className="d-flex  align-items-center ">
        {routeText && (
          <div className="d-flex">
            <h5>{routeText}</h5>{" "}
            <ArrowForwardIcon style={{ margin: "0 10px" }} />
            <h5>{navigationText}</h5>
          </div>
        )}
        {loader && <Loader2 />}
        {searchBar && (
          <div className="topnav__search">
            <i className="bx bx-search"></i>
            <input
              type="text"
              placeholder="Search here..."
              name={"fillterValue"}
              value={fillterValue}
              onChange={handleChange}
            />
          </div>
        )}
        {selectAll && (
          <div className="topnav__select">
            <select
              className="custom-select"
              type="text"
              name={"status"}
              placeholder="Select All"
              onChange={handleChange}
            >
              <option value="all">Status All</option>
              <option value="active">Active</option>
              <option value="block">Block</option>
            </select>
          </div>
        )}
      </div>
      <div>
        {fillterValue && <button onClick={fillterSearch}>Submit</button>}
        {fillterValue && <button onClick={() => closeFillter()}>Clear</button>}
      </div>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          // horizontal: "left",
        }}
        style={{ cursor: "pointer" }}
      >
        <Typography sx={{ p: 2 }} onClick={logout}>
          <i className={"bx bx-log-out-circle"}></i> <span>Logout</span>
        </Typography>
      </Popover>

      <div className="topnav__right">
        {!fillterValue && sendNotificationBtn && (
          <div
            className="send_notification_btn"
            onClick={() => setaddNewBusiness(true)}
          >
            <button>send custom notification</button>
          </div>
        )}
        <div className="topnav__right-item" onClick={handleClick}>
          {/* dropdown here */}
          <Dropdown
            customToggle={() => renderUserToggle(curr_user)}
            contentData={user_menu}
            renderItems={(item, index) => renderUserMenu(item, index)}
          />
        </div>
        {/* <div className="topnav__right-item">
                    <Dropdown
                        icon='bx bx-bell'
                        badge='12'
                        contentData={notifications}
                        renderItems={(item, index) => renderNotificationItem(item, index)}
                        renderFooter={() => <Link to='/'>View All</Link>}
                    />

                </div>
                <div className="topnav__right-item">
                    <ThemeMenu/>
                </div> */}
      </div>
      <Toaster />
    </div>
  );
};

export default withRouter(Topnav);
