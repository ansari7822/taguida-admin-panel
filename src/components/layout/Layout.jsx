import React, { useEffect, useState } from "react";

import "./layout.css";

import Sidebar from "../sidebar/Sidebar";
import BusinessSidebar from "../sidebar/BusinessSidebar";

import Routess from "../Routes";

import { HashRouter, Redirect, Route } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import ThemeAction from "../../redux/actions/ThemeAction";
const Layout = (props) => {
  const themeReducer = useSelector((state) => state.ThemeReducer);

  const dispatch = useDispatch();

  useEffect(() => {
    const themeClass = localStorage.getItem("themeMode", "theme-mode-light");
    const colorClass = localStorage.getItem("colorMode", "theme-mode-light");
    dispatch(ThemeAction.setMode(themeClass));
    dispatch(ThemeAction.setColor(colorClass));
  }, [dispatch]);

  const [state, setState] = useState(false);
  let role = localStorage.getItem("role");
  return (
    <div className={`layout ${themeReducer.mode} ${themeReducer.color}`}>
      {role == "admin" ? (
        <Sidebar {...props} />
      ) : (
        <BusinessSidebar {...props} />
      )}
      <div className="layout__content">
        <div className="layout__content-main">
          <Routess auth={props.auth} />
        </div>
      </div>
    </div>
  );
};

export default Layout;
