import React, { useState } from "react";
import nextIcImg from "../../assets/images/nextIcon.png";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import NavigateBeforeIcon from "@mui/icons-material/NavigateBefore";
import prevIcImg from "../../assets/images/prevIcon.png";
import "./table.css";

const Table = (props) => {
  console.log(props.limit, "renderBody");
  const initDataShow =
    props.limit && props.bodyData
      ? props.bodyData.slice(0, Number(props.limit))
      : props.bodyData;
  const [nextIcon, setnextIcon] = useState({
    nextIconBtn: (
      <span className="slider_btns">
        <NavigateNextIcon />
      </span>
    ),
    prevIconBtn: (
      <span className="slider_btns">
        <NavigateBeforeIcon />
      </span>
    ),
  });
  const [dataShow, setDataShow] = useState(initDataShow);

  let pages = 1;

  let range = [];

  if (props.limit !== undefined) {
    let page = Math.floor(props.bodyData.length / Number(props.limit));
    pages = props.bodyData.length % Number(props.limit) === 0 ? page : page + 1;
    console.log(page, "page");
    range = [...Array(pages).keys()];
  }
  const [currPage, setCurrPage] = useState(0);

  const selectPage = (page) => {
    const start = Number(props.limit) * page;
    const end = start + Number(props.limit);
    setDataShow(props.bodyData.slice(start, end));
    setCurrPage(page);
  };
  console.log(pages, "09908");
  return (
    <div>
      <div className="table-wrapper">
        <table>
          {props.headData && props.renderHead ? (
            <thead>
              <tr>
                {props.headData.map((item, index) =>
                  props.renderHead(item, index)
                )}
              </tr>
            </thead>
          ) : null}
          {props.bodyData && props.renderBody ? (
            <tbody>
              {dataShow.map((item, index) => props.renderBody(item, index))}
            </tbody>
          ) : null}
        </table>
      </div>
      {/* {props.pageNumber ? ( */}
      {pages > 0 ? (
        <div className="table__pagination">
          <span
            className={currPage == 0 ? "opacity-25" : ""}
            onClick={() => (currPage == 0 ? "" : selectPage(currPage - 1))}
            style={{ padding: "0px 20px", cursor: "pointer" }}
          >
            {" "}
            {nextIcon.prevIconBtn}
          </span>
          {range.map((item, index) => (
            <div
              key={index}
              className={`table__pagination-item ${
                currPage === index ? "active" : ""
              }`}
              onClick={() => selectPage(index)}
            >
              {item + 1}
            </div>
          ))}
          {console.log(pages, currPage, "87")}
          <span
            className={pages == currPage + 1 ? "opacity-25" : ""}
            onClick={() =>
              pages == currPage + 1 ? "" : selectPage(currPage + 1)
            }
            style={{ padding: "0px 20px", cursor: "pointer" }}
          >
            {nextIcon.nextIconBtn}{" "}
          </span>
        </div>
      ) : null}
    </div>
  );
};

export default Table;
