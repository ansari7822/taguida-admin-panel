import React from "react";

import { Route, Switch, Redirect, withRouter } from "react-router-dom";
import Login from "../pages/Auth/Login";
import Routes from "./layout/Layout";
import NotFoundPage from "../comman/NotFoundPage";
const RestrictedRoute = ({ component: Component, authUser, ...rest }) => {
  return (
    <Switch>
      <Route
        {...rest}
        render={(props) =>
          authUser == "admin" ? (
            <Component {...props} />
          ) : authUser == "business" ? (
            <Routes auth={authUser} />
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: props.location },
              }}
            />
          )
        }
      />
    </Switch>
  );
};

const AuthRoutes = (props) => {
  const { match, location } = props;
  let authUser = localStorage.getItem("role");
  if (location.pathname === "/") {
    if (authUser === null) {
      return <Redirect to={"/login"} />;
    } else if (
      location.pathname === "" ||
      location.pathname === "/" ||
      location.pathname === "/signin"
    ) {
      return <Redirect to={"/app/users"} />;
    } else {
      return <Redirect to={location.pathname} />;
    }
  }
  return (
    <Switch>
      <RestrictedRoute path={`/app`} authUser={authUser} component={Routes} />
      <Route path="/login" component={Login} />
      <Route component={NotFoundPage} />
    </Switch>
  );
};

export default withRouter(AuthRoutes);
