import React, { useState, useEffect } from "react";
import { withRouter, Link } from "react-router-dom";
import avtar from "../../../assets/images/user_img@2x.png";
import im1 from "../../../assets/images/c_img_1.png";
import TopNav from "../../../components/topnav/TopNav";
import Axios from "../../../Auth/Axios";
import { GetCategoriesById, CreateSubCategory } from "../../../Auth/ApiUrl";
import toast, { Toaster } from "react-hot-toast";
import slidImg from "../../../assets/images/slider_img.png";
import { Button, Carousel, Modal } from "react-bootstrap";
import dummyImg from "../../../assets/images/user_img@2x.png";
export default function View(props) {
  const { id } = props.match.params;
  const [user, setUser] = useState();
  const [subCat, setsubcat] = useState();

  const [loading, setloading] = useState(false);
  useEffect(() => {
    setloading(true);
    Axios.get(GetCategoriesById + id)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setUser(res.data.data);
        }
        setloading(false);
        console.log(res, "data");
      })
      .catch((err) => {
        setloading(false);
        return toast.success("Something went wrong", {
          duration: 4000,
          position: "top-center",
        });
      });
    let newUrl = CreateSubCategory + `?categoryId=${id}`;
    Axios.get(newUrl)
      .then((res) => {
        if (res.data.isSuccess == true) {
          console.log(res.data, "data");
          setsubcat(res.data.items);
        }
      })
      .catch((err) => {
        console.log(err, "err");
      });
  }, []);
  console.log(subCat, "setsubcat");
  return (
    <>
      <TopNav routeText={"My Places"} navigationText={"Place Details"} />
      <div className="user_view_page mb-3 ">
        <div className="user_view_header d-flex justify-content-between align-items-center">
          <h5 className="">Places Details</h5>
          <Link
            to={`/app/my-places/create-subcategory/${props.match.params.id}`}
          >
            <button className="add_new_user_btn">
              <i className={"bx bx-plus"}></i> Add New Place
            </button>
          </Link>
        </div>
        {user && user && (
          <div className="row m-0 p-0">
            <div className="col-md-4 view_catergory">
              <img src={user?.imgUrl || im1} />
            </div>
            <div className="col-md-12 pt-4 pb-2 d-flex justify-content-between">
              <h6>{user.name}</h6>
              <div className="tags_container">
                <span className="tags">love</span>
                <span className="tags">logo</span>
              </div>
            </div>
            <div className="col-md-12">
              <p>{user.description || "Description N/A"}</p>
            </div>
            <h5>Saved By</h5>
            <div className="col-md-12 saved_places">
              <img src={dummyImg} />
              <h6 className="">
                <b>name</b>
              </h6>
              <p className="">dummy@gmail.com</p>
            </div>
          </div>
        )}
      </div>
      <Toaster />
    </>
  );
}
