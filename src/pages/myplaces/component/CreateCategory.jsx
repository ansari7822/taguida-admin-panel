import React, { useState, useEffect } from "react";
import { withRouter, Link, useParams } from "react-router-dom";
import im1 from "../../../assets/images/uploadImg.png";
import TopNav from "../../../components/topnav/TopNav";
import Axios from "../../../Auth/Axios";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";
import FormControl from "@mui/material/FormControl";
import { Rating } from "react-simple-star-rating";
import Popover from "@mui/material/Popover";
import editIcon from "../../../assets/images/ic_cross.svg";
import im12 from "../../../assets/images/uploadImg.png";
import Select from "@mui/material/Select";
import CoverCrop from "../../../comman/CoverCrop";
import DataURIToBlob from "../../../comman/DataBlob";
import { Modal } from "react-bootstrap";
import Select2 from "../../../comman/Selectt";
import makeAnimated from "react-select/animated";
import Dropzone from "react-dropzone";
import Mult from "../../../comman/Selectt";
import { TEXTFIELD, GoogleAutocomplete } from "../../../comman/inputeFields";
import {
  Add_Ideas,
  UploadImage,
  GetBusinesseById,
  GetCategories,
} from "../../../Auth/ApiUrl";
import toast, { Toaster } from "react-hot-toast";
import { checkError } from "../../../Auth/helper";
import Loader2 from "../../../comman/Loader2";
import { Button, Spinner } from "react-bootstrap";
import { FormHelperText } from "@mui/material";

/**
 * Formats the size
 */
function formatBytes(bytes, decimals = 2) {
  if (bytes === 0) return "0 Bytes";
  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

  const i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}
function View(props) {
  const [disabled, setDisabled] = useState(true);
  const [allImages, setallImages] = useState([]);
  const [updatecover, setupdatecover] = useState({
    isOpen: false,
    image: null,
  });
  const [updatecover2, setupdatecover2] = useState({
    isOpen: false,
    image: null,
  });
  const [updatecover3, setupdatecover3] = useState({
    isOpen: false,
    image: null,
  });
  const [addNewBusiness, setaddNewBusiness] = useState(false);
  const [state, setState] = useState({
    name: "",
    description: "",
    email: "",
    phone: "",
    instagramUrl: "",
    tags: [],
    cuisinesArr: [],
    facebookUrl: "",
    openingHours: "",
    type: [],
    menu: [],
    closingHours: "",
    parkingStatus: "yes",
    address: "",
    faq: "",
    cuisines: "",
    menDressCode: "",
    categoryId: "",
    htName: "",
    htDescription: "",
    womenDressCode: "",
    lat: "",
    amName: "",
    amDescription: "",
    lng: "",
    location: "",
    locationCoordinates: [],
    images: [],
    errors: {
      name: "",
      address: "",
      description: "",
      type: "",
    },
  });
  const [imgUrls, setimgUrls] = useState("");
  const [imgUrlsForMenu, setimgUrlsForMenu] = useState();
  let id = localStorage.getItem("businessId");
  console.log(id);
  // const { id } = useParams();
  const [rating, setRating] = useState(0); // initial rating value
  const [anchorEl, setAnchorEl] = useState(null);
  const [anchorEl2, setAnchorEl2] = useState(null);
  const [tagValue, settagValue] = useState("");
  const [hotelsMain, sethotelsMain] = useState([]);
  const [amenitiesMain, setamenitiesMain] = useState([]);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClick2 = (event) => {
    setAnchorEl2(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleClose2 = () => {
    setAnchorEl2(null);
  };
  const open = Boolean(anchorEl);
  const open2 = Boolean(anchorEl2);
  const popid = open ? "simple-popover" : undefined;
  const popid2 = open2 ? "simple-popover" : undefined;

  const deleteHotels = (valueOfindex) => {
    let filterd = hotelsMain.filter((d, i) => i !== valueOfindex);
    console.log(filterd);
    sethotelsMain(filterd);
  };
  const handleHotelAddItem = (event) => {
    const { htName, htDescription } = state;
    event.preventDefault();
    let data = {
      question: htName,
      answer: htDescription,
    };
    if (htName && htDescription) {
      sethotelsMain((prev) => [...prev, data]);
      setState((prev) => ({
        ...prev,
        htName: "",
        htDescription: "",
      }));
    }
  };
  const deleteAmenities = (valueOfindex) => {
    let filterd = amenitiesMain.filter((d, i) => i !== valueOfindex);
    setamenitiesMain(filterd);
  };
  const handleAmenitiesAddItem = (event) => {
    const { amName, amDescription } = state;
    event.preventDefault();
    let data = {
      name: amName,
      imgUrl: amDescription,
    };
    if (amName && amDescription) {
      setamenitiesMain((prev) => [...prev, data]);
      setState((prev) => ({
        ...prev,
        amName: "",
        amDescription: "",
      }));
    }
  };

  useEffect(() => {
    let getImages = [];
    if (id) {
      Axios.get(GetBusinesseById + id)
        .then((res) => {
          const response = res.data.data;
          if (res.data.isSuccess == true) {
            for (let i = 0; i < response?.businessImages.length; i++) {
              getImages.push(response?.businessImages[i].imgUrl);
              setallImages(getImages);
            }
            setloading(false);
            console.log(response.name, "kljjjkllkj");
            sethotelsMain([...response.businessFaqs]);
            setamenitiesMain([...response.businessAmenities]);
            setimgUrlsForMenu(response?.businessMenus[0]?.imgUrl);
            setState((prev) => ({ ...prev, name: response?.name }));

            setState({
              ...state,
              ...response,
              // name: response.name,
              address: response.location,
              lng: response.locationCoordinates[0],
              lat: response.locationCoordinates[1],
              menu: [response?.businessMenus[0]?.imgUrl],
              tags: response.tags ? response.tags.split(",") : [],
              cuisinesArr: response.cuisines
                ? response.cuisines.split(",")
                : [],
            });
            if (response?.cuisines) {
              let newArr = response.cuisines.split(",");
              setSelected(newArr.map((d, i) => ({ value: d, label: d })));
            }
            // if (response?.cuisines) {
            //   let newArr = response.tacuisinesgs.split(",");
            //   setSelected(newArr.map((d, i) => ({ value: d, label: d })));
            // }
            setimgUrls(response.imgUrl);
          } else {
            return (
              toast.error(res.data.error, {
                duration: 4000,
                position: "top-right",
              }),
              setloading(false)
            );
          }
        })
        .catch((err) => {
          console.log(err, "87987");
        });
    }
  }, []);

  // Catch Rating value
  const handleRating = (rate) => {
    setRating(rate);
    // other logic
  };
  const addTage = () => {
    if (tagValue !== "" || undefined || null) {
      setState({ ...state, tags: [...tags, tagValue] });
    }
    settagValue("");
  };
  const removeTage = (ind) => {
    const { tags } = state;
    const tagsNe = tags.filter((d, i) => i !== ind);
    setState({ ...state, tags: tagsNe });
  };

  console.log(rating, "reating");
  const handleChange = (e) => {
    e.preventDefault();
    // const { name, value } = e.traget;
    switch (e.target.name) {
      case "name":
        errors.name = e.target.value.length < 1 ? "Name Required" : "";
        break;
      case "description":
        errors.description =
          e.target.value.length < 1 ? "Description Required" : "";
        break;
      case "categoryId":
        errors.categoryId = e.target.value.length < 1 ? "Type Required" : "";
        break;
      case "tagValue":
        settagValue(e.target.value);
        break;
      default:
        break;
    }
    setState((prev) => ({ ...prev, errors, [e.target.name]: e.target.value }));
  };

  const [loading, setloading] = useState(false);

  useEffect(() => {
    Axios.get(GetCategories)
      .then((res) => {
        if (res.data.isSuccess == true) {
          // setUsersList(res.data.items);
          let data = res.data.items.filter((d, i) => d.type == "idea");
          if (data) {
            let type = data.map((d) => {
              return {
                [d.id]: d.name,
              };
            });
            setState((prev) => ({ ...prev, type: type }));
          }

          console.log(data, "data");
        }
      })
      .catch((err) => console.log("err"));
  }, []);

  const getFile = (e, fd) => {
    // setState({ ...state, imgUpload: e.target.files[0] });
    setloading(true);
    const formData = new FormData();
    formData.append("media", fd);
    Axios.post(UploadImage, formData)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setloading(false);
          setallImages([...allImages, res.data.data.url]);
          setimgUrls(res.data.data.url);
        } else {
          return (
            toast.error(res.data.error, {
              duration: 4000,
              position: "top-right",
            }),
            setloading(false)
          );
        }
      })
      .catch((err) => {
        console.log(err, "87987");
      });
  };
  const getFileForMenu = (e, fd) => {
    setloading(true);
    const formData = new FormData();
    formData.append("media", fd);
    Axios.post(UploadImage, formData)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setloading(false);
          setState((prev) => ({ ...prev, menu: [res.data.data.url] }));
          setimgUrlsForMenu(res.data.data.url);
        } else {
          return (
            toast.error(res.data.error, {
              duration: 4000,
              position: "top-right",
            }),
            setloading(false)
          );
        }
      })
      .catch((err) => {
        console.log(err, "87987");
      });
  };
  const getFileForAmenities = (e, fd) => {
    setloading(true);
    console.log("called");
    const formData = new FormData();
    formData.append("media", fd);
    Axios.post(UploadImage, formData)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setloading(false);
          setState((prev) => ({ ...prev, amDescription: res.data.data.url }));
          console.log(res, 9090890890);
        } else {
          console.log("res");
          return (
            toast.error(res.data.error, {
              duration: 4000,
              position: "top-right",
            }),
            setloading(false)
          );
        }
      })
      .catch((err) => {
        console.log(err, "87987");
      });
  };

  const submit = () => {
    const {
      name,
      description,
      address,
      lat,
      lng,
      tags,
      instagramUrl,
      womenDressCode,
      menDressCode,
      cuisines,
      openingHours,
      menu,
      parkingStatus,
      facebookUrl,
      cuisinesArr,
      closingHours,
    } = state;
    // let ctgid;
    // for (let i in categoryId) {
    //   ctgid = Number(i);
    // }
    let data = {
      name,
      images: allImages,
      // categoryId: categoryId,
      amenities: amenitiesMain,
      tags: tags ? tags.join(",") : "",
      location: address,
      facebookUrl: facebookUrl,
      // fa
      menu,
      faqs: hotelsMain,
      openingHours,
      closingHours,
      instagramUrl: instagramUrl,
      locationCoordinates: [lng, lat],
      description: description,
      womenDressCode,
      menDressCode,
      cuisines: cuisinesArr ? cuisinesArr.join(",") : "",
      showInApp: true,
      parkingStatus,
    };

    setloading(true);
    let actionUrl = Add_Ideas;
    if (id) {
      Axios.put(GetBusinesseById + id, data)
        .then((res) => {
          if (res.data.isSuccess == true) {
            return (
              (toast.success("Business Details Updated", {
                duration: 2000,
                position: "top-right",
              }),
              setTimeout(() => {
                props.history.push("/app/my-business/list");
              }, 100)),
              setloading(false)
            );
          } else {
            return (
              toast.error(res.data.error, {
                duration: 4000,
                position: "top-right",
              }),
              setloading(false)
            );
          }
        })
        .catch((err) => {
          console.log(err, "87987");
        });
    } else {
      Axios.post(Add_Ideas, data)
        .then((res) => {
          if (res.data.isSuccess == true) {
            return (
              (toast.success("Idea Added", {
                duration: 2000,
                position: "top-right",
              }),
              setTimeout(() => {
                props.history.push("/app/my-business/list");
              }, 100)),
              setloading(false)
            );
          } else {
            return (
              toast.error(res.data.error, {
                duration: 4000,
                position: "top-right",
              }),
              setloading(false)
            );
          }
        })
        .catch((err) => {
          console.log(err, "87987");
        });
    }
  };
  // for (let ke in state) {
  //   if (state[ke] !== "" || null || undefined) {
  //     console.log(state[ke], "90890980");
  //   }
  // }

  const handleAddress = (address) => {
    let errors = state.errors;
    console.log(address, errors);
    let add = address;
    errors.address = address.address < 1 ? "Required" : "";
    add.errors = errors;
    setState({ ...state, ...add });
    console.log(add, 123);
  };
  const removeImage = (i) => {
    let filterdata = allImages.filter((d) => d !== i);
    setallImages(filterdata);
  };
  const {
    errors,
    name,
    imgUrl,
    description,
    categoryId,
    address,
    type,
    amName,
    amDescription,
    parkingStatus,
    womenDressCode,
    menDressCode,
    cuisines,
    email,
    faq,
    phone,
    menu,
    openingHours,
    closingHours,
    facebookUrl,
    tags,
    instagramUrl,
    lat,
    cuisinesArr,
    htDescription,
    htName,
    lng,
  } = state;

  const scrlltoLeft = () => {
    if (allImages) {
      setTimeout(() => {
        const flavoursContainer = document.getElementById("neid");
        flavoursContainer.scrollLeft += 3000;
      }, 1000);
    }
  };
  useEffect(() => {
    scrlltoLeft();
  }, [allImages]);
  //croper
  const handleAcceptedFiles = (name) => (_files) => {
    const files = _files?.filter((file) => file.size < 5242880);
    if (files.length < _files.length) {
      // return toastr.error(props.t("max_file_size"));
    }
    files.map((file) =>
      Object.assign(file, {
        preview: URL.createObjectURL(file),
        formattedSize: formatBytes(file.size),
      })
    );
    if (name === "coverImage") {
      return setupdatecover((prevState) => ({
        isOpen: !prevState.isOpen,
        image: files[0],
      }));
    }
  };
  const handleAcceptedFiles2 = (name) => (_files) => {
    const files = _files?.filter((file) => file.size < 5242880);
    if (files.length < _files.length) {
      // return toastr.error(props.t("max_file_size"));
    }
    files.map((file) =>
      Object.assign(file, {
        preview: URL.createObjectURL(file),
        formattedSize: formatBytes(file.size),
      })
    );
    if (name === "coverImage") {
      return setupdatecover2((prevState) => ({
        isOpen: !prevState.isOpen,
        image: files[0],
      }));
    }
  };
  const handleAcceptedFiles3 = (name) => (_files) => {
    const files = _files?.filter((file) => file.size < 5242880);
    if (files.length < _files.length) {
      // return toastr.error(props.t("max_file_size"));
    }
    files.map((file) =>
      Object.assign(file, {
        preview: URL.createObjectURL(file),
        formattedSize: formatBytes(file.size),
      })
    );
    if (name === "coverImage") {
      return setupdatecover3((prevState) => ({
        isOpen: !prevState.isOpen,
        image: files[0],
      }));
    }
  };
  const onCoverCrop = (image_base64, data) => {
    const file = DataURIToBlob(image_base64);

    getFile("e", file);
  };
  const onCoverCrop2 = (image_base64, data) => {
    const file = DataURIToBlob(image_base64);
    getFileForMenu("e", file);
  };
  const onCoverCrop3 = (image_base64, data) => {
    const file = DataURIToBlob(image_base64);
    getFileForAmenities("e", file);
  };

  const [selected, setSelected] = useState();
  const getData = (e) => {
    let tagData = e.map((d) => d.value);
    let tagData2 = e.map((d) => ({ value: d.value, label: d.value }));
    setState({ ...state, cuisinesArr: tagData });
    setSelected(tagData2);
  };
  return (
    <>
      <Modal
        size="lg"
        show={addNewBusiness}
        onHide={() => setaddNewBusiness(false)}
        centered
        className="business_modal"
        aria-labelledby="example-modal-sizes-title-sm"
      >
        <div
          className="business_modal_container22"
          style={{ borderRadius: "60px" }}
        >
          <Mult getData={getData} initialData={selected} />
          <div className="btn_div ">
            <button
              className="business_btn2"
              onClick={() => setaddNewBusiness(false)}
            >
              Back
            </button>
            <Button
              className="business_btn "
              // disabled={disabled}
              style={{ backgroundColor: "#1d1d1d" }}
              onClick={() => setaddNewBusiness(false)}
            >
              Add
            </Button>
          </div>
        </div>
      </Modal>
      <CoverCrop
        {...updatecover}
        toggle={(image) =>
          setupdatecover((prevState) => ({ isOpen: !prevState.isOpen, image }))
        }
        onCoverCrop={onCoverCrop}
        imgRatio={4 / 3}
      />
      <CoverCrop
        {...updatecover2}
        toggle={(image) =>
          setupdatecover2((prevState) => ({ isOpen: !prevState.isOpen, image }))
        }
        onCoverCrop={onCoverCrop2}
        imgRatio={4 / 3}
      />
      <CoverCrop
        {...updatecover3}
        toggle={(image) =>
          setupdatecover3((prevState) => ({ isOpen: !prevState.isOpen, image }))
        }
        onCoverCrop={onCoverCrop3}
        imgRatio={4 / 3}
      />
      {loading && <Loader2 />}
      <TopNav
        routeText={"Business"}
        navigationText={id ? "Update Business" : "Update Business"}
      />
      <div className="user_view_page mb-3">
        <div
          className="user_view_header  align-items-center"
          style={{ paddingBottom: 18 }}
        >
          <h5 className="">{id ? "Update Business" : "Update Business"} </h5>
          <p>Enter the new Business details below</p>
        </div>
        <div
          className="d-flex align-items-center m-0 businessImageScroll"
          id="neid"
        >
          {allImages &&
            allImages.map((d, i) => {
              return (
                <div
                  className="col-md-3 position-relative "
                  style={{ marginRight: 20 }}
                  key={i}
                >
                  <>
                    <img
                      src={editIcon}
                      onClick={() => removeImage(d)}
                      // onClick={() => alert("yes")}
                      className="delete_icon_img"
                      style={{ width: 30, height: 30, objectFit: "contain" }}
                    />
                    <label
                      className=" create_catergory2"
                      htmlFor="fileImg"
                      style={{ borderRadius: "9px" }}
                    >
                      {/* {loading && <Loader2 color="yellow" />} */}
                      {imgUrls || allImages ? (
                        <img
                          src={allImages[i]}
                          style={{
                            width: "200px",
                            height: "100px",
                            objectFit: "cover",
                          }}
                        />
                      ) : (
                        <img src={im12} />
                      )}

                      {/* <input
                        type="file"
                        name="image"
                        // value={imgUrls}
                        onChange={getFile}
                        id="fileImg"
                      /> */}
                    </label>
                  </>
                </div>
              );
            })}
          {allImages.length !== 10 && (
            <div className="col-md-3 gap-1">
              <>
                <label className=" create_catergory2" htmlFor="fileImg">
                  {/* {loading && <Loader2 color="yellow" />} */}
                  <img src={im12} />
                  <span className="text-muted">Upload Image</span>
                  {/* <input
                    type="file"
                    name="image"
                    onChange={getFile}
                    id="fileImg"
                  /> */}
                  <Dropzone
                    onDrop={(acceptedFiles) => {
                      handleAcceptedFiles("coverImage")(acceptedFiles);
                    }}
                  >
                    {({ getRootProps, getInputProps }) => {
                      return (
                        <div
                          className="dropzone-single-image"
                          {...getRootProps()}
                        >
                          <a
                            href="/cover-image"
                            onClick={(e) => e.preventDefault()}
                          >
                            <input
                              {...getInputProps()}
                              id="formrow-profile-image-Input"
                              multiple={false}
                              accept="image/*"
                              id="fileImg"
                            />

                            {/* <div className="edit-iocn">Upload</div> */}
                          </a>
                        </div>
                      );
                    }}
                  </Dropzone>
                </label>
              </>
            </div>
          )}
        </div>
        <div className="row form_row">
          <div className="col-md-4 mt-0">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Business Name"
              name="name"
              className="mt-0"
              value={name}
              notShowIcon={true}
              onChange={handleChange}
              errors={errors}
            />
          </div>
          <div className="col-md-4">
            <GoogleAutocomplete
              label="Address"
              address={address}
              InputLabelProps={{
                shrink: true,
              }}
              id="standard-error-helper-text"
              lat={lat}
              lng={lng}
              style={{ marginTop: "8px" }}
              errors={errors}
              name="address"
              onChange={handleAddress}
            />
          </div>
          <div className="col-md-2">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Opening Hours"
              name="openingHours"
              value={openingHours}
              type="time"
              className="mt-0"
              notShowIcon={true}
              onChange={handleChange}
              errors={errors}
            />
          </div>
          <div className="col-md-2">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Closing Hours"
              name="closingHours"
              value={closingHours}
              type="time"
              className="mt-0"
              notShowIcon={true}
              onChange={handleChange}
              errors={errors}
            />
          </div>
          <div className="col-md-4">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Cuisines"
              name="cuisines"
              type="text"
              value={cuisines}
              className="mt-0"
              notShowIcon={true}
              onChange={handleChange}
              // errors={errors}
            />
          </div>
          <div className="col-md-4">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Men Dress Code"
              name="menDressCode"
              type="text"
              value={menDressCode}
              className="mt-0"
              notShowIcon={true}
              onChange={handleChange}
              // errors={errors}
            />
          </div>
          <div className="col-md-4">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Women Dress Code"
              name="womenDressCode"
              type="text"
              value={womenDressCode}
              className="mt-0"
              notShowIcon={true}
              onChange={handleChange}
              // errors={errors}
            />
          </div>
          <div className="col-md-4">
            <FormControl variant="standard" sx={{ m: 1, minWidth: "100%" }}>
              <InputLabel id="demo-simple-select-standard-label">
                Parking Status
              </InputLabel>
              <Select
                labelId="demo-simple-select-standard-label"
                id="demo-simple-select-standard"
                value={parkingStatus}
                name="parkingStatus"
                onChange={handleChange}
                label="Parking Status"
                InputLabelProps={{
                  shrink: true,
                }}
              >
                <MenuItem value={"yes"}>
                  <em>Available</em>
                </MenuItem>
                <MenuItem value={"no"}>
                  <em>Not Available</em>
                </MenuItem>
              </Select>
            </FormControl>
          </div>

          <div className="col-md-4 d-flex align-items-center">
            <button
              // onClick={addTage}
              onClick={() => setaddNewBusiness(true)}
              style={{
                padding: "10px 10px",
                display: "flex",
                alignItems: "center",
              }}
            >
              Add Cuisines<i className={"bx bx-plus"}></i>
            </button>
          </div>

          {cuisinesArr && cuisinesArr && cuisinesArr.length > 0 && (
            <div className="col-md-4 d-flex align-items-center">
              <span className="">
                {cuisinesArr && cuisinesArr && cuisinesArr.length > 0
                  ? cuisinesArr.map((d, ind) => {
                      return (
                        <span className="tags2 mb-2">
                          {d}
                          {/* <i
                            className="delete_tag"
                            onClick={() => removeTage(ind)}
                          >
                            <CloseRoundedIcon fontSize="small" />
                          </i> */}
                        </span>
                      );
                    })
                  : ""}
              </span>
            </div>
          )}

          <div className="col-md-3">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Tags"
              name="tagValue"
              value={tagValue}
              className="mt-0"
              notShowIcon={true}
              onChange={handleChange}
            />
          </div>
          <div className="col-md-1 d-flex align-items-center">
            <button
              onClick={addTage}
              style={{ padding: "10px 10px", display: "flex" }}
            >
              <i className={"bx bx-plus"}></i>
            </button>
          </div>
          {tags && tags && tags.length > 0 && (
            <div className="col-md-4 d-flex mt-2 align-items-center">
              <span className="">
                {tags && tags && tags.length > 0
                  ? tags.map((d, ind) => {
                      return (
                        <span className="tags2 mb-2">
                          {d}
                          <i
                            className="delete_tag"
                            onClick={() => removeTage(ind)}
                          >
                            <CloseRoundedIcon fontSize="small" />
                          </i>
                        </span>
                      );
                    })
                  : ""}
              </span>
            </div>
          )}
          <div className="col-md-4 d-flex align-items-center ">
            <div className="add_link_div">
              <p>Others Links</p>
              <ul className="add_links_container p-0 ">
                <li onClick={handleClick} aria-describedby={popid}>
                  <a href="javascript:void(0)">
                    <i className="bx bx-plus"></i> Add Instagram
                  </a>
                </li>
                <li
                  onClick={handleClick2}
                  aria-describedby={popid2}
                  style={{ paddingLeft: 15 }}
                >
                  <a href="javascript:void(0)">
                    <i className="bx bx-plus"></i> Add Facebook
                  </a>
                </li>
                <Popover
                  id={popid}
                  open={open}
                  className="popover_input"
                  anchorEl={anchorEl}
                  onClose={handleClose}
                  anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "left",
                  }}
                >
                  <input
                    placeholder="Instagram "
                    onChange={handleChange}
                    name="instagramUrl"
                    value={instagramUrl}
                  />
                </Popover>
                <Popover
                  id={popid2}
                  open={open2}
                  className="popover_input"
                  anchorEl={anchorEl2}
                  onClose={handleClose2}
                  anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "left",
                  }}
                >
                  <input
                    placeholder="Facebook"
                    onChange={handleChange}
                    name="facebookUrl"
                    value={facebookUrl}
                  />
                </Popover>
              </ul>
            </div>
          </div>

          <div className="col-md-12">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Description"
              name="description"
              value={description}
              className="mt-0"
              notShowIcon={true}
              multiline
              rows={2}
              maxRows={4}
              onChange={handleChange}
              errors={errors}
            />
          </div>

          <div className="col-md-4">
            <>
              <label
                className="col-md-12 upload_video upload_btn"
                htmlFor="fileVideo"
              >
                <span>Upload Menu</span>
                <Dropzone
                  onDrop={(acceptedFiles) => {
                    handleAcceptedFiles2("coverImage")(acceptedFiles);
                  }}
                >
                  {({ getRootProps, getInputProps }) => {
                    return (
                      <div
                        className="dropzone-single-image"
                        {...getRootProps()}
                      >
                        <a
                          href="/cover-image"
                          onClick={(e) => e.preventDefault()}
                        >
                          <input
                            {...getInputProps()}
                            id="formrow-profile-image-Input"
                            multiple={false}
                            accept="image/*"
                            id="fileVideo"
                          />
                        </a>
                      </div>
                    );
                  }}
                </Dropzone>
                {/* <input
                  type="file"
                  name="image"
                  onChange={getFileForMenu}
                  id="fileVideo"
                /> */}
              </label>
            </>
          </div>
          {imgUrlsForMenu && (
            <div>
              <a href={imgUrlsForMenu} target="_blank">
                {imgUrlsForMenu.slice(0, 30)} ...
              </a>
            </div>
          )}
          <div className="col-md-12 hotel">
            <div className="row">
              <div className="col-md-12 py-2" style={{ paddingLeft: "20px" }}>
                <b className="text-muted pl-2">FAQ </b>
              </div>
              <div className="col-md-4">
                <TEXTFIELD
                  id="standard-error-helper-text"
                  label="Question"
                  name="htName"
                  value={htName}
                  onChange={handleChange}
                  className="mt-0"
                  notShowIcon={true}
                  errors={errors}
                />
              </div>
              <div className="col-md-6">
                <TEXTFIELD
                  id="standard-error-helper-text"
                  label="Answer"
                  name="htDescription"
                  value={htDescription}
                  className="mt-0"
                  notShowIcon={true}
                  onChange={handleChange}
                  errors={errors}
                />
              </div>
              <div className="col-md-2 d-flex align-items-center">
                <button
                  // style={{ cursor: addButton ? "not-allowed" : "pointer" }}
                  classname="business_btn"
                  onClick={handleHotelAddItem}
                >
                  {" "}
                  Add
                </button>
              </div>
            </div>
            {hotelsMain.map((dk, i) => {
              return (
                <div className="row" key={i}>
                  <div className="col-md-8 faq_box ">
                    <span>
                      <b>Question:</b>
                      {dk.question.slice(0, 50)}
                    </span>
                    <small>
                      <b>Answer:</b> {dk.answer.slice(0, 100) + "..."}
                    </small>
                  </div>
                  <div className="col-md-2">
                    <button
                      classname="business_btn"
                      onClick={() => deleteHotels(i)}
                    >
                      <i className={"bx bx-trash"}></i>
                    </button>
                  </div>
                </div>
              );
            })}
          </div>
          <div className="col-md-12 hotel">
            <div className="row">
              <div className="col-md-12 py-2" style={{ paddingLeft: "20px" }}>
                <b className="text-muted pl-2">Amenities </b>
              </div>
              <div className="col-md-4">
                <TEXTFIELD
                  id="standard-error-helper-text"
                  label="Name"
                  name="amName"
                  value={amName}
                  onChange={handleChange}
                  className="mt-0"
                  notShowIcon={true}
                  errors={errors}
                />
              </div>
              <div className="col-md-2 d-flex  align-items-center">
                <div className="">
                  <label
                    className="col-md-12 upload_video upload_btn"
                    htmlFor="fileAm"
                  >
                    <span>Upload Image</span>
                    {/* <input
                      type="file"
                      name="image"
                      onChange={getFileForAmenities}
                      id="fileAm"
                    /> */}
                    <Dropzone
                      onDrop={(acceptedFiles) => {
                        handleAcceptedFiles3("coverImage")(acceptedFiles);
                      }}
                    >
                      {({ getRootProps, getInputProps }) => {
                        return (
                          <div
                            className="dropzone-single-image"
                            {...getRootProps()}
                          >
                            <a
                              href="/cover-image"
                              onClick={(e) => e.preventDefault()}
                            >
                              <input
                                {...getInputProps()}
                                id="formrow-profile-image-Input"
                                multiple={false}
                                accept="image/*"
                                id="fileAm"
                              />
                            </a>
                          </div>
                        );
                      }}
                    </Dropzone>
                  </label>
                  <small>{amDescription.slice(0, 20)} </small>
                </div>
              </div>
              <div className="col-md-2 d-flex align-items-center">
                <button
                  // style={{ cursor: addButton ? "not-allowed" : "pointer" }}
                  classname="business_btn"
                  onClick={handleAmenitiesAddItem}
                >
                  {" "}
                  Add
                </button>
              </div>
            </div>
            {amenitiesMain.map((dk, i) => {
              return (
                <div className="row" key={i}>
                  <div className="col-md-8 faq_box ">
                    <span>
                      <b>Name:</b>
                      {dk.name}
                    </span>
                    <small>
                      <b>Img:</b>{" "}
                      <a href={dk.imgUrl}>{dk.imgUrl.slice(0, 30)}...</a>
                    </small>
                  </div>
                  <div className="col-md-2">
                    <button
                      classname="business_btn"
                      onClick={() => deleteAmenities(i)}
                    >
                      <i className={"bx bx-trash"}></i>
                    </button>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <div className="row justify-content-center cat_btn_col2">
          <div className="col-md-3">
            <button
              className="business_btn2"
              onClick={() => props.history.goBack()}
            >
              Back
            </button>
          </div>
          <div className="col-md-3">
            <button onClick={submit}>{"Update Details"}</button>{" "}
            {loading && <i>Loading...</i>}
          </div>
        </div>
      </div>
      <Toaster />
    </>
  );
}
export default withRouter(View);
