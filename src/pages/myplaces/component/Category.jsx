import React, { useState, useEffect } from "react";
import avtar from "../../../assets/images/user_img@2x.png";
import customerList from "../../../assets/JsonData/customers-list.json";
import TopNav from "../../../components/topnav/TopNav";
import { GetBusinesseById, GetUserById } from "../../../Auth/ApiUrl";
import Axios from "../../../Auth/Axios";
import toast, { Toaster } from "react-hot-toast";
import Loading from "../../../comman/Loading";
import { withRouter, Link } from "react-router-dom";
import Table from "../../../components/table/Table";
import slidImg from "../../../assets/images/slider_img.png";
import slidImg2 from "../../../assets/images/c_img_1.png";
import nextIcImg from "../../../assets/images/nextIcon.png";
import prevIcImg from "../../../assets/images/prevIcon.png";
import { Button, Carousel, Modal } from "react-bootstrap";

const customerTableHead = [
  // '',
  "user Info",
  "email",
  "phone",
  "status",
];
const renderHead = (item, index) => <th key={index}>{item}</th>;
const renderBody = (item, index, props, showModel, smShow, blockUnblock) => {
  return (
    <>
      <tr key={index}>
        {/* <td>{}</td> */}
        <td className="name_with_img">
          <img src={item.imgUrl || avtar} />
          {item.name}
        </td>
        <td className="text-lowercase">{item.email}</td>
        <td>
          {`+${item.countryCode || "N/a"}`} {item.phone}
        </td>
        <td>
          <span className="status">{item.status}</span>
        </td>
        <td className="action_btn">
          <div className="popup_btn">
            <div className="d-flex justify-content-between flex-column">
              <Link to={`/app/users/view/${item.id}`}>
                <p className="pb-0 mb-0">
                  <i className={"bx bx-show"} style={{ minWidth: 23 }}></i>
                  <span>View User</span>
                </p>
              </Link>
              <a
                href="javascript:void(0)"
                onClick={() => showModel(true, item.id)}
              >
                <p className="pb-0 mb-0">
                  <i className={"bx bx-trash"} style={{ minWidth: 20 }}></i>{" "}
                  <span>Delete User</span>
                </p>
              </a>
              <a
                href="javascript:void(0)"
                onClick={() =>
                  blockUnblock(
                    item.id,
                    item.status == "active" ? "blocked" : "active"
                  )
                }
              >
                <p className="pb-0 mb-0">
                  <i className={"bx bx-block"} style={{ minWidth: 20 }}></i>{" "}
                  <span>
                    {item.status == "active" ? "Block" : "active"} User
                  </span>
                </p>
              </a>
            </div>

            {/* <a
              href="javascript:void(0)"
              onClick={() => showModel(true, item.id)}
            >
              <p className="pb-0 mb-0">
                <i className={"bx bx-trash"}></i> <span>Delete User</span>
              </p>
            </a>
            <a href="javascript:void(0)" onClick={() => alert(item.id)}>
              <p className="pb-0 mb-0">
                <i className={"bx bx-block"}></i> <span>Block User</span>
              </p>
            </a> */}
          </div>
          {/* <i className="bx bx-dots-vertical-rounded "></i> */}
        </td>
      </tr>
    </>
  );
};
const Category = (props) => {
  const [usersList, setUsersList] = useState([]);
  const [loading, setloading] = useState(false);
  const [userId, setUserId] = useState(null);
  const [smShow, setSmShow] = useState(false);
  const [getprevioususerList, setgetprevioususerList] = useState(false);
  const [deleteloading, setDeleteloading] = useState(false);
  const [pageNumber, setpageNumber] = useState(null);
  const [nextIcon, setnextIcon] = useState({
    nextIconBtn: (
      <span className="slider_btns">
        <img src={nextIcImg} />
      </span>
    ),
    prevIconBtn: (
      <span className="slider_btns">
        <img src={prevIcImg} />
      </span>
    ),
  });
  let _id = localStorage.getItem("businessId");
  useEffect(() => {
    setloading(true);
    Axios.get(GetBusinesseById + _id)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setUsersList([res.data.data]);
          setpageNumber(res.data.pageNo);
        }
        setloading(false);
        console.log(res, "data");
      })
      .catch((err) => {
        setloading(false);
        return toast.success("Something went wrong", {
          duration: 4000,
          position: "top-center",
        });
      });
  }, [getprevioususerList]);

  const fillterUserList = (data) => {
    setloading(true);
    setUsersList("");
    if (data) {
      setloading(false);
      setUsersList(data.items);
      console.log(data, "userlist");
    }
  };
  const DeleteUser = () => {
    setDeleteloading(true);
    Axios.delete(GetUserById + userId)
      .then((res) => {
        if (res.data.isSuccess == true) {
          return toast.success(res.data.message, {
            duration: 3000,
            position: "top-center",
          });
          // props.history.push("/")
        } else if (res.data.isSuccess == false) {
          return toast.info(res.data.message, {
            duration: 3000,
            position: "top-center",
          });
        }
        setDeleteloading(false);
      })
      .catch((err) => {
        setDeleteloading(false);
        return toast.error("Something went wrong", {
          duration: 4000,
          position: "top-center",
        });
      });
    setSmShow(false);
  };
  const showModel = (action, id) => {
    setSmShow(action);
    setUserId(id);
  };
  const refreshPage = () => {
    setgetprevioususerList(true);
  };

  const setgetprevioususerListFun = () => {
    setgetprevioususerList((prev) => !prev);
  };
  const { nextIconBtn, prevIconBtn } = nextIcon;
  console.log(usersList, "usersList, setUsersList");

  return (
    <>
      <Modal
        size="md"
        show={smShow}
        onHide={() => showModel(false)}
        centered
        aria-labelledby="example-modal-sizes-title-sm"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-sm">
            User Delete ?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Are you sure to Delete this user if "Yes" then Click on "Agree"</p>
        </Modal.Body>
        <Modal.Footer>
          <button onClick={() => showModel(false)}>Agree</button>
          <button onClick={() => showModel(false)}>Cancel</button>
        </Modal.Footer>
      </Modal>
      <TopNav
        searchBar={true}
        fillterUserListFun={fillterUserList}
        ApiURL={GetBusinesseById}
        setgetprevious={setgetprevioususerListFun}
      />
      <div className="table_container">
        <div className="user_header d-flex justify-content-between align-items-center">
          <h3 className="page-header">My Business</h3>
          <Link to="/app/my-business/create-category">
            <button className="add_new_user_btn">
              <i className={"bx bx-plus"}></i> Update Details
            </button>
          </Link>
        </div>
        <div className="row">
          <div className="col-12">
            <div className="card">
              <div className="card__body">
                {usersList && usersList.length > 0 ? (
                  <Table
                    limit="10"
                    headData={customerTableHead}
                    renderHead={(item, index) => renderHead(item, index)}
                    bodyData={usersList}
                    pageNumber={pageNumber}
                    totalCount={10}
                    renderBody={(item, index) =>
                      renderBody(item, index, props, showModel)
                    }
                  />
                ) : (
                  loading && <Loading />
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <Toaster />
    </>
  );
};

export default withRouter(Category);
