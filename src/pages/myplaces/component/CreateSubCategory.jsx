import React, { useState, useEffect } from "react";
import { withRouter, Link } from "react-router-dom";
import im1 from "../../../assets/images/uploadImg.png";
import TopNav from "../../../components/topnav/TopNav";
import Axios from "../../../Auth/Axios";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";

import Select from "@mui/material/Select";

import { TEXTFIELD } from "../../../comman/inputeFields";
import { CreateSubCategory, UploadImage } from "../../../Auth/ApiUrl";
import toast, { Toaster } from "react-hot-toast";
import { checkError } from "../../../Auth/helper";
import Loader2 from "../../../comman/Loader2";
import { Button } from "react-bootstrap";
import { FormHelperText } from "@mui/material";
export default function View(props) {
  const { id } = props.match.params;
  const [disabled, setDisabled] = useState(true);
  const [user, setUser] = useState();
  const [state, setState] = useState({
    name: "",
    description: "",
    tags: [],
    errors: {
      name: "",
      description: "",
    },
  });
  const [imgUrls, setimgUrls] = useState("");
  const [tagValue, settagValue] = useState("");
  const handleChange = (e) => {
    e.preventDefault();
    // const { name, value } = e.traget;
    switch (e.target.name) {
      case "name":
        errors.name = e.target.value.length < 1 ? "Name Required" : "";
        break;
      case "description":
        errors.description =
          e.target.value.length < 1 ? "Description Required" : "";
        break;
      case "tagValue":
        settagValue(e.target.value);
        break;
      default:
        break;
    }
    setState((prev) => ({ ...prev, errors, [e.target.name]: e.target.value }));
  };
  const addTage = () => {
    if (tagValue !== "" || undefined || null) {
      setState({ ...state, tags: [...tags, tagValue] });
    }
    settagValue("");
  };
  const removeTage = (ind) => {
    const { tags } = state;
    const tagsNe = tags.filter((d, i) => i !== ind);
    setState({ ...state, tags: tagsNe });
  };
  const [loading, setloading] = useState(false);
  // useEffect(() => {
  //   setloading(true);
  //   Axios.get(GetCategoriesById + id)
  //     .then((res) => {
  //       if (res.data.isSuccess == true) {
  //         setUser(res.data.data);
  //       }
  //       setloading(false);
  //       console.log(res, "data");
  //     })
  //     .catch((err) => {
  //       setloading(false);
  //       return toast.success("Something went wrong", {
  //         duration: 4000,
  //         position: "top-center",
  //       });
  //     });
  // }, []);

  const getFile = (e) => {
    setState({ ...state, imgUpload: e.target.files[0] });
    setloading(true);
    const formData = new FormData();
    formData.append("media", e.target.files[0]);
    Axios.post(UploadImage, formData)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setloading(false);
          setimgUrls(res.data.data.url);
        } else {
          return (
            toast.error(res.data.error, {
              duration: 4000,
              position: "top-right",
            }),
            setloading(false)
          );
        }
      })
      .catch((err) => {
        console.log(err, "87987");
      });
  };
  // const UploadImageCall = () => {};

  const submit = () => {
    const { name, description, tags } = state;
    let data = {
      name: name,
      imgUrl: imgUrls,
      tags: tags.join(","),
      categoryId: props.match.params.subCatId,
      description: description,
    };
    setloading(true);
    Axios.post(CreateSubCategory, data)
      .then((res) => {
        if (res.data.isSuccess == true) {
          return (
            (toast.success("Sub Category Added", {
              duration: 2000,
              position: "top-center",
            }),
            setTimeout(() => {
              props.history.push("/app/categories/list");
            }, 100)),
            setloading(false)
          );
        } else {
          return (
            toast.error(res.data.error, {
              duration: 4000,
              position: "top-right",
            }),
            setloading(false)
          );
        }
      })
      .catch((err) => {
        console.log(err, "87987");
      });
  };
  const { errors, name, imgUrl, description, type, tags } = state;

  return (
    <>
      {loading && <Loader2 />}
      <TopNav routeText={"Places"} navigationText={"Add New Place"} />
      <div className="user_view_page mb-3">
        <div className="user_view_header  align-items-center pb-0">
          <h5 className="">Add New Place </h5>
          <p>Enter the new New Place details below</p>
        </div>
        <div className="row align-items-center mb-2">
          <label className="col-md-4 create_catergory" htmlFor="fileImg">
            {imgUrls ? (
              <img
                src={imgUrls}
                style={{ width: "200px", height: "100px", objectFit: "cover" }}
              />
            ) : (
              <img src={im1} />
            )}
            <span className="text-muted">Upload Image</span>
            <input
              type="file"
              name="image"
              value={imgUrl}
              onChange={getFile}
              id="fileImg"
            />
          </label>
        </div>
        <div className="row">
          <div className="col-md-6">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Place Name"
              name="name"
              className="mt-0"
              value={name}
              notShowIcon={true}
              onChange={handleChange}
              errors={errors}
            />
          </div>
          <div className="col-md-6">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Description"
              name="description"
              value={description}
              className="mt-0"
              notShowIcon={true}
              onChange={handleChange}
              errors={errors}
            />
          </div>
          <div className="col-md-5">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Tags"
              name="tagValue"
              value={tagValue}
              className="mt-0"
              notShowIcon={true}
              onChange={handleChange}
            />
          </div>
          <div className="col-md-1">
            <button onClick={addTage}>
              <i className={"bx bx-plus"}></i>
            </button>
          </div>

          <div className="col-md-6">
            <span className="">
              {tags.map((d, ind) => (
                <span className="tags2 mb-2">
                  {d}
                  <i className="delete_tag" onClick={() => removeTage(ind)}>
                    <CloseRoundedIcon fontSize="small" />
                  </i>
                </span>
              ))}
            </span>
          </div>
        </div>
        <div className="row justify-content-center mt-4">
          <div className="col-md-3">
            <button
              className="business_btn2"
              onClick={() => props.history.goBack()}
            >
              Back
            </button>
          </div>
          <div className="col-md-3">
            <button onClick={submit}>Add Category</button>
          </div>
        </div>
      </div>
      <Toaster />
    </>
  );
}
