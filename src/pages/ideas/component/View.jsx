import React, { useState, useEffect } from "react";
import { withRouter, Link } from "react-router-dom";
import avtar from "../../../assets/images/user_img@2x.png";
import im1 from "../../../assets/images/c_img_1.png";
import TopNav from "../../../components/topnav/TopNav";
import Axios from "../../../Auth/Axios";
import {
  GET_IDAES_ById,
  CreateSubCategory,
  GetSubCategoryById,
} from "../../../Auth/ApiUrl";
import toast, { Toaster } from "react-hot-toast";
import slidImg from "../../../assets/images/slider_img.png";
import { Button, Carousel, Modal } from "react-bootstrap";
export default function View(props) {
  const { id } = props.match.params;
  const [user, setUser] = useState();
  const [userId, setUserId] = useState(null);
  const [smShow, setSmShow] = useState(false);
  const [subCat, setsubcat] = useState();

  const [loading, setloading] = useState(false);
  useEffect(() => {
    setloading(true);
    Axios.get(GET_IDAES_ById + id)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setUser(res.data.data);
        }
        setloading(false);
        console.log(res, "data");
      })
      .catch((err) => {
        setloading(false);
        return toast.success("Something went wrong", {
          duration: 4000,
          position: "top-center",
        });
      });
    let newUrl = CreateSubCategory + `?categoryId=${id}`;
    Axios.get(newUrl)
      .then((res) => {
        if (res.data.isSuccess == true) {
          console.log(res.data, "data");
          setsubcat(res.data.items);
        }
      })
      .catch((err) => {
        console.log(err, "err");
      });
  }, []);

  const DeleteUser = () => {
    setloading(true);
    Axios.delete(GetSubCategoryById + userId)
      .then((res) => {
        if (res.data.isSuccess == true) {
          return (
            toast.success(res.data.message, {
              duration: 3000,
              position: "top-right",
            }),
            setloading(false),
            props.history.push(`/app/categories`)
          );
        } else if (res.data.isSuccess == false) {
          return toast.info(res.data.message, {
            duration: 3000,
            position: "top-right",
          });
        }
        setloading(false);
      })
      .catch((err) => {
        setloading(false);
        return toast.error("Something went wrong", {
          duration: 4000,
          position: "top-riht",
        });
      });
    setSmShow(false);
  };
  const showModel = (action, id) => {
    setSmShow(action);
    setUserId(id);
  };
  return (
    <>
      <Modal
        size="md"
        show={smShow}
        onHide={() => showModel(false)}
        centered
        aria-labelledby="example-modal-sizes-title-sm"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-sm">
            Sub Category Delete ?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            Are you sure to Delete this Sub Category if "Yes" then Click on
            "Agree"
          </p>
        </Modal.Body>
        <Modal.Footer>
          <button onClick={() => DeleteUser()}>Agree</button>
          <button onClick={() => showModel(false)}>Cancel</button>
        </Modal.Footer>
      </Modal>
      <TopNav routeText={"Idea"} navigationText={"Idea Details"} />
      <div className="user_view_page mb-3 ">
        <div className="user_view_header d-flex justify-content-between align-items-center">
          <h5 className="">Idea </h5>
        </div>
        {user && user && (
          <div className="row m-0 p-0">
            {user?.ideaImages?.map((d, i) => {
              return (
                <>
                  <div className="col-md-3 gap-1 mb-4 pl-0" key={i}>
                    <div className="prefrered_cate">
                      <img src={d.imgUrl || im1} />
                    </div>
                  </div>
                </>
              );
            })}
            <div className="col-md-12">
              <h6>{user.name}</h6>
              <p>{user.description || "Description N/A"}</p>
            </div>
          </div>
        )}
      </div>
      <Toaster />
    </>
  );
}
