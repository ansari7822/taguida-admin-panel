import React, { useState, useEffect } from "react";
import { withRouter, Link, useParams } from "react-router-dom";
import im1 from "../../../assets/images/uploadImg.png";
import TopNav from "../../../components/topnav/TopNav";
import Axios from "../../../Auth/Axios";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import { Rating } from "react-simple-star-rating";
import Popover from "@mui/material/Popover";
import editIcon from "../../../assets/images/ic_cross.svg";
import im12 from "../../../assets/images/uploadImg.png";
import Select from "@mui/material/Select";
import CoverCrop from "../../../comman/CoverCrop";
import DataURIToBlob from "../../../comman/DataBlob";
import Dropzone from "react-dropzone";

import { TEXTFIELD, GoogleAutocomplete } from "../../../comman/inputeFields";
import {
  Add_Ideas,
  UploadImage,
  GET_IDAES_ById,
  GetCategories,
} from "../../../Auth/ApiUrl";
import toast, { Toaster } from "react-hot-toast";
import { checkError } from "../../../Auth/helper";
import Loader2 from "../../../comman/Loader2";
import { Button } from "react-bootstrap";
import { FormHelperText } from "@mui/material";
/**
 * Formats the size
 */
function formatBytes(bytes, decimals = 2) {
  if (bytes === 0) return "0 Bytes";
  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

  const i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}

function View(props) {
  const [disabled, setDisabled] = useState(true);
  const [allImages, setallImages] = useState([]);
  const [updatecover, setupdatecover] = useState({
    isOpen: false,
    image: null,
  });
  const [state, setState] = useState({
    name: "",
    description: "",
    email: "",
    phone: "",
    instagramUrl: "",
    facebookUrl: "",
    openingHours: "",
    type: [],
    address: "",
    categoryId: "",
    lat: "",
    lng: "",
    location: "Mohali",
    locationCoordinates: [],
    images: [],
    errors: {
      name: "",
      address: "",
      description: "",
      type: "",
    },
  });
  const [imgUrls, setimgUrls] = useState("");
  const { id } = useParams();
  const [rating, setRating] = useState(0); // initial rating value
  const [anchorEl, setAnchorEl] = useState(null);
  const [anchorEl2, setAnchorEl2] = useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClick2 = (event) => {
    setAnchorEl2(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleClose2 = () => {
    setAnchorEl2(null);
  };
  const open = Boolean(anchorEl);
  const open2 = Boolean(anchorEl2);
  const popid = open ? "simple-popover" : undefined;
  const popid2 = open2 ? "simple-popover" : undefined;

  useEffect(() => {
    if (id) {
      Axios.get(GET_IDAES_ById + id)
        .then((res) => {
          const response = res.data.data;
          if (res.data.isSuccess == true) {
            setloading(false);
            console.log(response);
            setState({
              ...state,
              name: response.name,
              categoryId: response.categoryId,
              address: response.location,
              lng: response.locationCoordinates[0],
              lat: response.locationCoordinates[1],
              description: response.description,
            });
            if (response?.ideaImages.length > 0) {
              let getImages = response?.ideaImages.map((d) => d.imgUrl);
              console.log(getImages, "getImages");
              setallImages(getImages);
            }
            // setallImages((prev) => [...prev, ...response.ideaImages.url]);
            setimgUrls(response.imgUrl);
          } else {
            return (
              toast.error(res.data.error, {
                duration: 4000,
                position: "top-right",
              }),
              setloading(false)
            );
          }
        })
        .catch((err) => {
          console.log(err, "87987");
        });
    }
  }, []);

  // Catch Rating value
  const handleRating = (rate) => {
    setRating(rate);
    // other logic
  };
  console.log(rating, "reating");
  const handleChange = (e) => {
    e.preventDefault();
    // const { name, value } = e.traget;
    switch (e.target.name) {
      case "name":
        errors.name = e.target.value.length < 1 ? "Name Required" : "";
        break;
      case "description":
        errors.description =
          e.target.value.length < 1 ? "Description Required" : "";
        break;
      case "categoryId":
        errors.categoryId = e.target.value.length < 1 ? "Type Required" : "";
        break;
      default:
        break;
    }
    setState((prev) => ({ ...prev, errors, [e.target.name]: e.target.value }));
  };
  console.log(id, "prps");
  const [loading, setloading] = useState(false);

  useEffect(() => {
    Axios.get(GetCategories)
      .then((res) => {
        if (res.data.isSuccess == true) {
          // setUsersList(res.data.items);
          let data = res.data.items.filter((d, i) => d.type == "idea");
          if (data) {
            let type = data.map((d) => {
              return {
                [d.id]: d.name,
              };
            });
            setState((prev) => ({ ...prev, type: type }));
          }

          console.log(data, "data");
        }
      })
      .catch((err) => console.log("err"));
  }, []);

  const getFile = (e, fd) => {
    // setState({ ...state, imgUpload: e.target.files[0] });
    setloading(true);
    const formData = new FormData();
    formData.append("media", fd);
    Axios.post(UploadImage, formData)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setloading(false);
          setallImages([...allImages, res.data.data.url]);
          setimgUrls(res.data.data.url);
        } else {
          return (
            toast.error(res.data.error, {
              duration: 4000,
              position: "top-right",
            }),
            setloading(false)
          );
        }
      })
      .catch((err) => {
        console.log(err, "87987");
      });
  };
  //croper

  const handleAcceptedFiles = (name) => (_files) => {
    const files = _files?.filter((file) => file.size < 5242880);
    if (files.length < _files.length) {
      // return toastr.error(props.t("max_file_size"));
    }
    files.map((file) =>
      Object.assign(file, {
        preview: URL.createObjectURL(file),
        formattedSize: formatBytes(file.size),
      })
    );
    if (name === "coverImage") {
      return setupdatecover((prevState) => ({
        isOpen: !prevState.isOpen,
        image: files[0],
      }));
    }
  };
  const onCoverCrop = (image_base64, data) => {
    const file = DataURIToBlob(image_base64);
    getFile("e", file);
  };

  console.log(allImages, "imgUrls");
  const submit = () => {
    const { name, description, address, lat, lng } = state;
    // let ctgid;
    // for (let i in categoryId) {
    //   ctgid = Number(i);
    // }
    let data = {
      name: name,
      images: allImages,
      categoryId: categoryId,
      location: address,
      locationCoordinates: [lng, lat],
      description: description,
    };
    setloading(true);
    let actionUrl = Add_Ideas;
    if (id) {
      Axios.put(GET_IDAES_ById + id, data)
        .then((res) => {
          if (res.data.isSuccess == true) {
            return (
              (toast.success("Idea Updated", {
                duration: 2000,
                position: "top-right",
              }),
              setTimeout(() => {
                props.history.push("/app/ideas/list");
              }, 100)),
              setloading(false)
            );
          } else {
            return (
              toast.error(res.data.error, {
                duration: 4000,
                position: "top-right",
              }),
              setloading(false)
            );
          }
        })
        .catch((err) => {
          console.log(err, "87987");
        });
    } else {
      Axios.post(Add_Ideas, data)
        .then((res) => {
          if (res.data.isSuccess == true) {
            return (
              (toast.success("Idea Added", {
                duration: 2000,
                position: "top-right",
              }),
              setTimeout(() => {
                props.history.push("/app/ideas/list");
              }, 100)),
              setloading(false)
            );
          } else {
            return (
              toast.error(res.data.error, {
                duration: 4000,
                position: "top-right",
              }),
              setloading(false)
            );
          }
        })
        .catch((err) => {
          console.log(err, "87987");
        });
    }
  };
  const handleAddress = (address) => {
    let errors = state.errors;
    console.log(address, errors);
    let add = address;
    errors.address = address.address < 1 ? "Required" : "";
    add.errors = errors;
    setState({ ...state, ...add });
    console.log(add, 123);
  };
  const removeImage = (i) => {
    let filterdata = allImages.filter((d) => d !== i);
    setallImages(filterdata);
  };
  const {
    errors,
    name,
    imgUrl,
    description,
    categoryId,
    address,
    type,
    lat,
    lng,
  } = state;
  console.log(lat, lng, "categoryId");
  return (
    <>
      {/* {loading && <Loader2 />} */}
      <CoverCrop
        {...updatecover}
        toggle={(image) =>
          setupdatecover((prevState) => ({ isOpen: !prevState.isOpen, image }))
        }
        onCoverCrop={onCoverCrop}
        imgRatio={4 / 3}
      />
      <TopNav
        routeText={"Date Ideas"}
        navigationText={id ? "Update Date Ideas" : "Add Date Ideas"}
      />
      <div className="user_view_page mb-3">
        <div
          className="user_view_header  align-items-center"
          style={{ paddingBottom: 18 }}
        >
          <h5 className="">{id ? "Update Category" : "Add New Date Ideas"} </h5>
          <p>Enter the new Date Ideas details below</p>
        </div>
        <div className="row align-items-center m-0">
          {allImages &&
            allImages.map((d, i) => {
              return (
                <div className="col-md-3 position-relative gap-1" key={i}>
                  <>
                    <img
                      src={editIcon}
                      onClick={() => removeImage(d)}
                      // onClick={() => alert("yes")}
                      className="delete_icon_img"
                      style={{ width: 30, height: 30, objectFit: "contain" }}
                    />
                    <label
                      className=" create_catergory2"
                      htmlFor="fileImg"
                      style={{ borderRadius: "9px" }}
                    >
                      {/* {loading && <Loader2 color="yellow" />} */}
                      {imgUrls ? (
                        <img
                          src={allImages[i]}
                          style={{
                            width: "200px",
                            height: "100px",
                            objectFit: "cover",
                          }}
                        />
                      ) : id ? (
                        <img
                          src={allImages[i]}
                          style={{
                            width: "200px",
                            height: "100px",
                            objectFit: "cover",
                          }}
                        />
                      ) : (
                        <img src={im12} />
                      )}

                      {/* <input
                        type="file"
                        name="image"
                        // value={imgUrls}
                        onChange={getFile}
                        id="fileImg"
                      /> */}
                      {/*
                      <Dropzone
                        onDrop={(acceptedFiles) => {
                          handleAcceptedFiles("coverImage")(acceptedFiles);
                        }}
                      >
                        {({ getRootProps, getInputProps }) => {
                          return (
                            <div
                              className="dropzone-single-image"
                              {...getRootProps()}
                            >
                              <a
                                href="/cover-image"
                                onClick={(e) => e.preventDefault()}
                              >
                                <input
                                  {...getInputProps()}
                                  id="formrow-profile-image-Input"
                                  multiple={false}
                                  accept="image/*"
                                  id="fileImg"
                                />
                              </a>
                            </div>
                          );
                        }}
                      </Dropzone> */}
                    </label>
                  </>
                </div>
              );
            })}
          {allImages.length !== 4 && (
            <div className="col-md-3 gap-1">
              <>
                <label className=" create_catergory2" htmlFor="fileImg2">
                  {/* <img
                  src={editIcon}
                  className="edit_icon_img"
                  style={{ width: 30, height: 30, objectFit: "contain" }}
                /> */}
                  {loading && <Loader2 color="yellow" />}
                  <img src={im12} />
                  <span className="text-muted">Upload Image</span>
                  {/* <input
                    type="file"
                    name="image"
                    // value={imgUrls}
                    onChange={getFile}
                    id="fileImg"
                  /> */}
                  <Dropzone
                    onDrop={(acceptedFiles) => {
                      handleAcceptedFiles("coverImage")(acceptedFiles);
                    }}
                  >
                    {({ getRootProps, getInputProps }) => {
                      return (
                        <div
                          className="dropzone-single-image"
                          {...getRootProps()}
                        >
                          <a
                            href="/cover-image"
                            onClick={(e) => e.preventDefault()}
                          >
                            <input
                              {...getInputProps()}
                              id="formrow-profile-image-Input"
                              multiple={false}
                              accept="image/*"
                              id="fileImg2"
                            />

                            {/* <div className="edit-iocn">Upload</div> */}
                          </a>
                        </div>
                      );
                    }}
                  </Dropzone>
                </label>
              </>
            </div>
          )}
          {/* <label className="col-md-4 create_catergory" htmlFor="fileImg">
          {imgUrls ? (
            <img
                src={imgUrls}
                style={{ width: "200px", height: "100px", objectFit: "cover" }}
              />
            ) : (
              <img src={im1} />
            )}
            <span className="text-muted">
              {id ? "Update Image" : "Upload Image"}
            </span>
            <input
              type="file"
              name="image"
              value={imgUrl}
              onChange={getFile}
              id="fileImg"
            />
          </label> */}
        </div>
        <div className="row form_row">
          <div className="col-md-6 mt-0">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Date Ideas Name"
              name="name"
              className="mt-0"
              value={name}
              notShowIcon={true}
              onChange={handleChange}
              errors={errors}
            />
          </div>

          <div className="col-md-4">
            <GoogleAutocomplete
              label="Address"
              address={address}
              InputLabelProps={{
                shrink: true,
              }}
              id="standard-error-helper-text"
              lat={lat}
              lng={lng}
              style={{ marginTop: "8px" }}
              errors={errors}
              name="address"
              onChange={handleAddress}
            />
          </div>

          <div className="col-md-6">
            <FormControl variant="standard" sx={{ m: 1, minWidth: "100%" }}>
              <InputLabel id="demo-simple-select-standard-label">
                Type
              </InputLabel>
              <Select
                labelId="demo-simple-select-standard-label"
                id="demo-simple-select-standard"
                value={categoryId}
                name="categoryId"
                onChange={handleChange}
                label="Category Type"
                InputLabelProps={{
                  shrink: true,
                }}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {type &&
                  type.length > 0 &&
                  type.map((d, i) => {
                    for (let dd in d) {
                      return (
                        <MenuItem value={dd}>
                          <em>{d[dd]}</em>
                        </MenuItem>
                      );
                    }
                  })}
              </Select>{" "}
            </FormControl>
          </div>

          {/* <div className="col-md-4">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="FAQ Website"
              name="faq"
              // value={description}
              className="mt-0"
              notShowIcon={true}
              onChange={handleChange}
              // errors={errors}
            />
          </div> */}
          {/* <div className="col-md-4">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Minimum age"
              name="age"
              type="number"
              // value={description}
              className="mt-0"
              notShowIcon={true}
              onChange={handleChange}
              // errors={errors}
            />
          </div>
          <div className="col-md-4">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Opening Hours"
              name="openingHours"
              value={openingHours}
              type="time"
              className="mt-0"
              notShowIcon={true}
              onChange={handleChange}
              errors={errors}
            />
          </div> */}
          {/* <div className="col-md-4">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Pricing"
              // name="description"
              // value={description}
              className="mt-0"
              notShowIcon={true}
              onChange={handleChange}
              // errors={errors}
            />
          </div> */}
          <div className="col-md-6">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Description"
              name="description"
              value={description}
              className="mt-0"
              notShowIcon={true}
              multiline
              rows={2}
              maxRows={4}
              onChange={handleChange}
              errors={errors}
            />
          </div>
          {/* <div className="col-md-4">
            <div className="add_link_div">
              <p className="m-0">Google Star Review </p>
              <Rating size={36} onClick={handleRating} ratingValue={rating} />
            </div>
          </div> */}
          {/* <div className="col-md-4 "></div> */}
          {/* <div className="col-md-4 ">
            <div className="add_link_div">
              <p>Others Links</p>
              <ul className="add_links_container">
                <li onClick={handleClick} aria-describedby={popid}>
                  <a href="javascript:void(0)">
                    <i className="bx bx-plus"></i> Add Instagram
                  </a>
                </li>
                <li onClick={handleClick2} aria-describedby={popid2}>
                  <a href="javascript:void(0)">
                    <i className="bx bx-plus"></i> Add Facebook
                  </a>
                </li>
                <Popover
                  id={popid}
                  open={open}
                  className="popover_input"
                  anchorEl={anchorEl}
                  onClose={handleClose}
                  anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "left",
                  }}
                >
                  <input
                    placeholder="Instagram "
                    onChange={handleChange}
                    name="instagramUrl"
                    value={instagramUrl}
                  />
                </Popover>
                <Popover
                  id={popid2}
                  open={open2}
                  className="popover_input"
                  anchorEl={anchorEl2}
                  onClose={handleClose2}
                  anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "left",
                  }}
                >
                  <input
                    placeholder="Facebook"
                    onChange={handleChange}
                    name="facebookUrl"
                    value={facebookUrl}
                  />
                </Popover>
              </ul>
            </div>
          </div> */}
        </div>
        <div className="row justify-content-center cat_btn_col2">
          <div className="col-md-3">
            <button
              className="business_btn2"
              onClick={() => props.history.goBack()}
            >
              Back
            </button>
          </div>
          <div className="col-md-3">
            {allImages.length < 1 ? (
              <button style={{ opacity: "60%" }}>
                {id ? "Updated Ideas" : "Add  Ideas"}
              </button>
            ) : (
              <button onClick={submit}>
                {id ? "Updated Ideas" : "Add  Ideas"}
              </button>
            )}
          </div>
        </div>
      </div>
      <Toaster />
    </>
  );
}
export default withRouter(View);
