import React, { useState, useEffect } from "react";
import logo from "../../assets/images/logo_sidebar@2x.png";
import formValid from "../../comman/formValid";
import MarkunreadIcon from "@mui/icons-material/Markunread";
import HttpsIcon from "@mui/icons-material/Https";
import Axios from "../../Auth/Axios";
import toast, { Toaster } from "react-hot-toast";
import { AdminLogin } from "../../Auth/ApiUrl";
import { TEXTFIELD, PASSWORDFIELD } from "../../comman/inputeFields";

import Loading from "../../comman/Loading";
import "./auth.css";
import { Redirect } from "react-router";
export default function Login(props) {
  const [state, setState] = useState({
    email: "",
    password: "",
    errors: {
      email: "",
      password: "",
    },
    disabled: true,
  });
  const [loading, setloading] = useState(false);
  const regExp = RegExp(/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/);
  const onSubmit = (e) => {
    e.preventDefault();
    if (formValid(this.state)) {
      console.log(this.state);
    } else {
      console.log("Form is invalid!");
    }
  };
  const handleChange = (e) => {
    e.preventDefault();
    // const { name, value } = e.traget;
    let errors = state.errors;
    switch (e.target.name) {
      case "email":
        errors.email = regExp.test(e.target.value)
          ? ""
          : "Email address is invalid";
        break;
      case "password":
        errors.password =
          e.target.value.length < 6 ? "Atleast 6 characaters required" : "";
        break;
      default:
        break;
    }
    setState((prev) => ({ ...prev, errors, [e.target.name]: e.target.value }));
  };
  const [activePanel, setactivePanel] = useState("admin");
  const submit = () => {
    const { email, password } = state;
    let data = {
      email: email,
      password: password,
      deviceId: "none",
      deviceType: "android",
    };
    setloading(true);
    Axios.post(AdminLogin, data)
      .then((res) => {
        if (res.data.isSuccess == true) {
          return (
            (toast.success("Log In Success", {
              duration: 2000,
              position: "top-center",
            }),
            localStorage.setItem("token", res.data.data.token),
            localStorage.setItem("role", res.data.data.role),
            localStorage.setItem("id", res.data.data.id),
            localStorage.setItem(
              "businessId",
              res?.data?.data?.business?.id || "null"
            ),
            // localStorage.setItem(
            //   "_id",
            //   res.data.data.business.id ? res.data.data.business.id : null
            // ),
            setTimeout(() => {
              props.history.push(
                res.data.data.role == "admin"
                  ? "/app/users"
                  : "/app/my-business"
              );
            }, 100)),
            setloading(false)
          );
        } else {
          return (
            toast.error(res.data.error, {
              duration: 4000,
              position: "top-center",
            }),
            setloading(false)
          );
        }
      })
      .catch((err) => {
        console.log(err, "87987");
      });
  };
  const switchPanel = (panelName) => {
    setactivePanel(panelName);
  };
  const { email, password, errors, disabled } = state;
  return (
    <div className="row m-0 ">
      <div className="col-md-7 log_img">{/* <img src={logImg} /> */}</div>
      <div className="col-md-5 login_form">
        <div className="logo_img">
          <img src={logo} />
        </div>
        {loading && <Loading absolute={"absolute"} />}
        <div className="">
          <TEXTFIELD
            id="standard-error-helper-text"
            label="Email"
            name="email"
            value={email}
            onChange={handleChange}
            Icon={MarkunreadIcon}
            errors={errors}
          />
          <PASSWORDFIELD
            name={"password"}
            value={password}
            type="password"
            label={"Password"}
            onChange={(e) => handleChange(e)}
            errors={errors}
            Icon={HttpsIcon}
          />
        </div>
        <button onClick={submit} className="web_btn">
          Login
        </button>
        <p
          className="switchPanel"
          onClick={() =>
            switchPanel(activePanel == "admin" ? "business" : "admin")
          }
        >{`Switch to ${
          activePanel == "admin" ? "Business" : "Admin"
        } Panel`}</p>
      </div>
      <Toaster />
    </div>
  );
}
