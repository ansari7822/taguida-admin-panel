import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import NotFoundPage from "../../comman/NotFoundPage";
import UserList from "./component/Users";
import View from "./component/View";
const Users = ({ match }) => (
  <Switch>
    <Redirect exact from={`${match.url}`} to={`${match.url}/list`} />
    <Route path={`${match.url}/list`} component={UserList} />
    <Route path={`${match.url}/view/:id`} component={View} />
    <Route component={NotFoundPage} />
  </Switch>
);

export default Users;
