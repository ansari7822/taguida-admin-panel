import React, { useState, useEffect } from "react";
import avtar from "../../../assets/images/user_img@2x.png";
import im1 from "../../../assets/images/c_img_1.png";
import TopNav from "../../../components/topnav/TopNav";
import Axios from "../../../Auth/Axios";
import { GetUserById } from "../../../Auth/ApiUrl";
import toast, { Toaster } from "react-hot-toast";

export default function View(props) {
  const { id } = props.match.params;
  const [user, setUser] = useState();
  const [loading, setloading] = useState(false);
  useEffect(() => {
    setloading(true);
    Axios.get(GetUserById + id)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setUser(res.data.data);
        }
        setloading(false);
        console.log(res, "data");
      })
      .catch((err) => {
        setloading(false);
        return toast.success("Something went wrong", {
          duration: 4000,
          position: "top-center",
        });
      });
  }, []);
  console.log(user, "dfds");
  return (
    <>
      <TopNav routeText={"Users"} navigationText={"User Details"} />
      <div className="user_view_page">
        <div className="user_view_header d-flex justify-content-between align-items-center">
          <h5 className="">User Details</h5>
          <div className="d-flex  align-items-center">
            <p className="status mb-0 text-capitalize">{user?.status}</p>
            {/* <i
              className="bx bx-dots-vertical-rounded "
              style={{ fontSize: "22px" }}
            ></i> */}
          </div>
        </div>
        <div className="row user_detail_container">
          <div className="col-md-2">
            <img src={(user && user.imgUrl) || avtar} />
          </div>
          <div className="col-md-3">
            <p>
              <b>Name</b>
            </p>
            <small>{user?.name}</small>
          </div>
          <div className="col-md-3">
            <p>
              <b>Email address</b>
            </p>
            <small>{user?.email}</small>
          </div>
          <div className="col-md-3">
            <p>
              <b>Phone number</b>
            </p>
            <small>
              +{user?.countryCode}
              {user?.phone}
            </small>
          </div>
        </div>
        {/* <div className="row">
          <div className="col-md-12 prefrered_header">
            <h5>Preferred Categories</h5>
          </div>
          <div className="col-md-3 prefrered_cate">
            <img src={im1} />
            <p>Date ideas</p>
          </div>
          <div className="col-md-3 prefrered_cate">
            <img src={im1} />
            <p>Date ideas</p>
          </div>
          <div className="col-md-3 prefrered_cate">
            <img src={im1} />
            <p>Date ideas</p>
          </div>
        </div> */}
      </div>
      <Toaster />
    </>
  );
}
