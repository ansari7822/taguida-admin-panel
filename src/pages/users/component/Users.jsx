import React, { useState, useEffect } from "react";
import Pagination from "@mui/material/Pagination";
import avtar from "../../../assets/images/user_img@2x.png";
import Table from "../../../components/table/Table";
import customerList from "../../../assets/JsonData/customers-list.json";
import TopNav from "../../../components/topnav/TopNav";
import { GetUsersList, GetUserById } from "../../../Auth/ApiUrl";
import Axios from "../../../Auth/Axios";
import toast, { Toaster } from "react-hot-toast";
import Loading from "../../../comman/Loading";
import { withRouter, Link } from "react-router-dom";
import { Button, Modal } from "react-bootstrap";
import PaginationCom from "../../../comman/pagination";
const customerTableHead = [
  // '',
  "user Info",
  "email",
  "phone",
  "status",
  "",
];

const renderHead = (item, index) => <th key={index}>{item}</th>;
const renderBody = (item, index, props, showModel, smShow, blockUnblock) => {
  return (
    <>
      <tr key={index}>
        {/* <td>{}</td> */}
        <td className="name_with_img">
          <img src={item.imgUrl || avtar} />
          {item.name}
        </td>
        <td>{item.email}</td>
        <td>
          {`+${item.countryCode}`} {item.phone}
        </td>
        <td>
          <span className="status">{item.status}</span>
        </td>
        <td className="action_btn">
          <div className="popup_btn">
            <div className="d-flex justify-content-between flex-column">
              <Link to={`/app/users/view/${item.id}`}>
                <p className="pb-0 mb-0">
                  <i className={"bx bx-show"} style={{ minWidth: 23 }}></i>
                  <span>View User</span>
                </p>
              </Link>
              <a
                href="javascript:void(0)"
                onClick={() => showModel(true, item.id)}
              >
                <p className="pb-0 mb-0">
                  <i className={"bx bx-trash"} style={{ minWidth: 20 }}></i>{" "}
                  <span>Delete User</span>
                </p>
              </a>
              <a
                href="javascript:void(0)"
                onClick={() =>
                  blockUnblock(
                    item.id,
                    item.status == "active" ? "blocked" : "active"
                  )
                }
              >
                <p className="pb-0 mb-0">
                  <i className={"bx bx-block"} style={{ minWidth: 20 }}></i>{" "}
                  <span>
                    {item.status == "active" ? "Block" : "active"} User
                  </span>
                </p>
              </a>
            </div>

            {/* <a
              href="javascript:void(0)"
              onClick={() => showModel(true, item.id)}
            >
              <p className="pb-0 mb-0">
                <i className={"bx bx-trash"}></i> <span>Delete User</span>
              </p>
            </a>
            <a href="javascript:void(0)" onClick={() => alert(item.id)}>
              <p className="pb-0 mb-0">
                <i className={"bx bx-block"}></i> <span>Block User</span>
              </p>
            </a> */}
          </div>
          <i className="bx bx-dots-vertical-rounded "></i>
        </td>
      </tr>
    </>
  );
};

const Users = (props) => {
  const [usersList, setUsersList] = React.useState([]);
  const [loading, setloading] = useState(false);
  const [userId, setUserId] = useState(null);
  const [smShow, setSmShow] = useState(false);
  const [getprevioususerList, setgetprevioususerList] = useState(false);
  const [deleteloading, setDeleteloading] = useState(false);
  const [pageNumber, setpageNumber] = useState(null);
  const [state, setstate] = useState({
    pageNo: 1,
    pageSize: 0,
    total: 0,
    totalRecords: 0,
    count: null,
  });

  const { count, pageNo } = state;
  const getNextPage = (event, value) => {
    // e.preventDefault();
    setstate((prev) => ({ ...prev, pageNo: value }));
    // setPage(value);
  };
  const getIdealist = () => {
    setloading(true);
    let newUr = GetUsersList + "?pageSize=1000" + "&" + `pageNo=${pageNo}`;
    Axios.get(newUr)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setUsersList(res.data.items);
          let countPage;
          if (res.data.totalRecords) {
            countPage =
              (res.data.totalRecords + res.data.pageSize - 1) /
              res.data.pageSize;
          }
          setstate({ ...res.data, count: Math.floor(countPage) });
          setpageNumber(res.data.pageNo);
        }
        setloading(false);
        console.log(res, "data");
      })
      .catch((err) => {
        setloading(false);
        return toast.success("Something went wrong", {
          duration: 4000,
          position: "top-center",
        });
      });
  };
  useEffect(() => {
    getIdealist();
  }, [getprevioususerList, state.pageNo]);

  const fillterUserList = (data) => {
    setloading(true);
    setUsersList("");
    if (data) {
      setloading(false);
      setUsersList(data.items);
      console.log(data, "userlist");
    }
  };
  const DeleteUser = () => {
    setDeleteloading(true);
    Axios.delete(GetUserById + userId)
      .then((res) => {
        if (res.data.isSuccess == true) {
          return (
            toast.success(res.data.message, {
              duration: 3000,
              position: "top-center",
            }),
            props.history.push("/")
          );
        } else if (res.data.isSuccess == false) {
          return toast.info(res.data.message, {
            duration: 3000,
            position: "top-center",
          });
        }
        setDeleteloading(false);
      })
      .catch((err) => {
        setDeleteloading(false);
        return toast.error("Something went wrong", {
          duration: 4000,
          position: "top-center",
        });
      });
    setSmShow(false);
  };
  const showModel = (action, id) => {
    setSmShow(action);
    setUserId(id);
  };
  const refreshPage = () => {
    setgetprevioususerList(true);
  };
  const blockUnblock = (id, sts) => {
    console.log(id, sts, "98908890");
    let body = {
      status: sts,
    };
    let newUr = GetUsersList + "/" + id;
    Axios.put(newUr, body)
      .then((res) => {
        console.log(res.data, "09980");
        if (res.data.isSuccess == true) {
          return (
            toast.success("Success", {
              duration: 1000,
              position: "top-center",
            }),
            refreshPage()
          );
          // props.history.push("/")
        } else if (res.data.isSuccess == false) {
          return toast.info(res.data.message, {
            duration: 3000,
            position: "top-center",
          });
        }
        setDeleteloading(false);
      })
      .catch((err) => {
        return toast.error("Something went wrong", {
          duration: 4000,
          position: "top-center",
        });
      });
  };
  const setgetprevioususerListFun = () => {
    setgetprevioususerList((prev) => !prev);
  };
  console.log(usersList, 908089098890);

  return (
    <>
      <Modal
        size="md"
        show={smShow}
        onHide={() => showModel(false)}
        centered
        aria-labelledby="example-modal-sizes-title-sm"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-sm">
            User Delete ?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Are you sure to Delete this user if "Yes" then Click on "Agree"</p>
        </Modal.Body>
        <Modal.Footer>
          <button onClick={() => DeleteUser()}>Agree</button>
          <button onClick={() => showModel(false)}>Cancel</button>
        </Modal.Footer>
      </Modal>
      <TopNav
        searchBar={true}
        selectAll={true}
        ApiURL={GetUsersList}
        sendNotificationBtn={true}
        fillterUserListFun={fillterUserList}
        setgetprevious={setgetprevioususerListFun}
      />
      <div className="table_container">
        <div className="user_header d-flex justify-content-between align-items-center">
          <h3 className="page-header">users</h3>
          {/* <button className="add_new_user_btn">
            <i className={"bx bx-plus"}></i> New User
          </button> */}
        </div>
        <div className="row">
          <div className="col-12">
            <div className="card">
              <div className="card__body">
                {deleteloading && <Loading deleting={"Deleting"} />}
                {usersList && usersList.length > 0 ? (
                  <Table
                    limit="10"
                    headData={customerTableHead}
                    renderHead={(item, index) => renderHead(item, index)}
                    bodyData={usersList}
                    pageNumber={pageNumber}
                    totalCount={10}
                    renderBody={(item, index) =>
                      renderBody(
                        item,
                        index,
                        props,
                        showModel,
                        smShow,
                        blockUnblock
                      )
                    }
                  />
                ) : (
                  loading && <Loading />
                )}
                {!loading && usersList.length <= 0 && (
                  <p className="mb-0 text-center">No User Found</p>
                )}
              </div>
              {/* <PaginationCom
                count={count}
                getNextPage={getNextPage}
                loading={loading}
                usersList={usersList}
              /> */}
            </div>
          </div>
        </div>
      </div>
      <Toaster />
    </>
  );
};

export default withRouter(Users);
