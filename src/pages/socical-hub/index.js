import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import NotFoundPage from "../../comman/NotFoundPage";
import BusinessesList from "./component/Businesses";
import View from "./component/View";
const Business = ({ match }) => (
    <Switch>
        <Redirect exact from={`${match.url}`} to={`${match.url}/list`} />
        <Route path={`${match.url}/list`} component={BusinessesList} />
        <Route path={`${match.url}/view/:id`} component={View} />
        <Route component={NotFoundPage} />
    </Switch>
);

export default Business;
