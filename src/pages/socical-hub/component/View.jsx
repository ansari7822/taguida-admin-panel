import React, { useState, useEffect } from "react";
import avtar from "../../../assets/images/user_img@2x.png";
import im1 from "../../../assets/images/m_img.png";
import deleteBtn from "../../../assets/images/ic_m_delete.svg";
import editIcon from "../../../assets/images/Group 10469.svg";
import TopNav from "../../../components/topnav/TopNav";
import { withRouter, Link } from "react-router-dom";
import im12 from "../../../assets/images/uploadImg.png";
import Dropzone from "react-dropzone";
import { TEXTFIELD, PASSWORDFIELD } from "../../../comman/inputeFields";
import { Button, Modal } from "react-bootstrap";
import Loader2 from "../../../comman/Loader2";
import ImageCrop from "../../../comman/CropImageBeforeUpload";
import Axios from "../../../Auth/Axios";
import DataURIToBlob from "../../../comman/DataBlob";
import { checkError } from "../../../Auth/helper";
import {
  GetsocialHubVideosList,
  UploadImage,
  PostsocialHubTypes,
} from "../../../Auth/ApiUrl";
import MailIcon from "@mui/icons-material/Mail";
import CallIcon from "@mui/icons-material/Call";
import HttpsIcon from "@mui/icons-material/Https";
import PersonIcon from "@mui/icons-material/Person";
import toast, { Toaster } from "react-hot-toast";
import CoverCrop from "../../../comman/CoverCrop";

/**
 * Formats the size
 */
function formatBytes(bytes, decimals = 2) {
  if (bytes === 0) return "0 Bytes";
  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

  const i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}

export default function View(props) {
  const { id } = props.match.params;
  const [user, setUser] = useState();
  const [state, setState] = useState({
    name: "",
    businessName: "",
    videoLink: "",
    errors: {
      name: "",
      // videoLink: "",
      businessName: "",
    },
  });
  const [localVideLink, setlocalVideLink] = useState();
  const [imgUrls, setimgUrls] = useState("");
  const [disabled, setDisabled] = useState(true);
  const [loading, setloading] = useState(false);
  const [userId, setUserId] = useState(null);
  const [smShow, setSmShow] = useState(false);
  const [arrtistImg, setarrtistImg] = useState();
  const [addNewBusiness, setaddNewBusiness] = useState(false);
  const [selectedFiles, setselectedFiles] = useState([]);
  const [isOpen, setisOpen] = useState(false);
  const [cropImg, setcropImg] = useState();
  const [updatecover, setupdatecover] = useState({
    isOpen: false,
    image: null,
  });
  const [updatecover2, setupdatecover2] = useState({
    isOpen: false,
    image: null,
  });

  const getFile = (e, fd) => {
    // setState({ ...state, imgUpload: e.target.files[0] });
    setloading(true);
    const formData = new FormData();
    // formData.append("media", e.target.files[0]);
    formData.append("media", fd);
    Axios.post(UploadImage, formData)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setloading(false);
          setimgUrls(res.data.data.url);
        } else {
          return (
            toast.error(res.data.error, {
              duration: 4000,
              position: "top-right",
            }),
            setloading(false)
          );
        }
      })
      .catch((err) => {
        console.log(err, "87987");
      });
  };
  const getFile2 = (e, fd) => {
    setloading(true);
    const formData = new FormData();
    // formData.append("media", e.target.files[0]);
    formData.append("media", fd);
    Axios.post(UploadImage, formData)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setloading(false);
          // setimgUrls(res.data.data.url);
          setarrtistImg(res.data.data.url);
        } else {
          return (
            toast.error(res.data.error, {
              duration: 4000,
              position: "top-right",
            }),
            setloading(false)
          );
        }
      })
      .catch((err) => {
        console.log(err, "87987");
      });
  };

  const getVideoFileLink = (e) => {
    setloading(true);
    const formData = new FormData();
    formData.append("media", e.target.files[0]);
    Axios.post(UploadImage, formData)
      .then((res) => {
        console.log(res.data);
        if (res.data.isSuccess == true) {
          setlocalVideLink(res.data.data.url);
          setloading(false);
        } else {
          return (
            toast.error(res.data.error, {
              duration: 4000,
              position: "top-right",
            }),
            setloading(false)
          );
        }
      })
      .catch((err) => {
        console.log(err, "87987");
      });
  };
  useEffect(() => {
    setloading(true);
    Axios.get(PostsocialHubTypes + "/" + props.match.params.id)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setUser(res.data.data.socialHubVideos);
        }
        setloading(false);
        console.log(res, "data");
      })
      .catch((err) => {
        setloading(false);
        return toast.success("Something went wrong", {
          duration: 4000,
          position: "top-center",
        });
      });
  }, []);

  const handleChange = (e) => {
    e.preventDefault();
    let errors = state.errors;
    switch (e.target.name) {
      case "name":
        errors.name = e.target.value.length < 1 ? "Title Required" : "";
        break;
      case "businessName":
        errors.businessName =
          e.target.value.length < 1 ? "Artist Name Required" : "";
        break;
      // case "videoLink":
      //   errors.videoLink =
      //     e.target.value.length < 1 ? "Video Link required" : "";
      //   break;
      default:
        break;
    }
    // let err = checkError(errors, state);
    // setDisabled(err.disabled);
    setState((prev) => ({ ...prev, errors, [e.target.name]: e.target.value }));
  };
  const redirect = () => props.history.push("/app/social-hub");
  console.log(props.match.params, "897");
  const addBusinessCall = () => {
    const { businessName, videoLink, name } = state;
    let data = {
      title: name,
      artistName: businessName,
      thumbnail: imgUrls,
      artistImage: arrtistImg,
      videoLink: videoLink || localVideLink,
      socialHubTypeId: props.match.params.id,
    };

    Axios.post(GetsocialHubVideosList, data)
      .then((res) => {
        console.log(res.data, "9090890890890890");
        if (res.data.isSuccess == true) {
          toast.success("Video Added Successfully", {
            duration: 4000,
            position: "top-right",
          });
          setTimeout(() => {
            redirect();
          }, 100);
        } else {
          toast.error(res.data.error, {
            duration: 4000,
            position: "top-right",
          });
        }
        setloading(false);
        console.log(res, "data");
      })
      .catch((err) => {
        setloading(false);
        return toast.success("Something went wrong", {
          duration: 4000,
          position: "top-center",
        });
      });
  };

  const DeleteUser = () => {
    setloading(true);
    Axios.delete(GetsocialHubVideosList + "/" + userId)
      .then((res) => {
        if (res.data.isSuccess == true) {
          return (
            toast.success(res.data.message, {
              duration: 3000,
              position: "top-right",
            }),
            setloading(false),
            redirect()
          );
        } else if (res.data.isSuccess == false) {
          return (
            toast.info(res.data.message, {
              duration: 3000,
              position: "top-right",
            }),
            setloading(false)
          );
        }
      })
      .catch((err) => {
        setloading(false);
        return toast.error("Something went wrong", {
          duration: 4000,
          position: "top-right",
        });
      });
    setSmShow(false);
  };
  const showModel = (action, id) => {
    setSmShow(action);
    setUserId(id);
  };

  //croper
  const handleAcceptedFiles = (name) => (_files) => {
    const files = _files?.filter((file) => file.size < 5242880);

    if (files.length < _files.length) {
      // return toastr.error(props.t("max_file_size"));
    }
    setaddNewBusiness(false);
    files.map((file) =>
      Object.assign(file, {
        preview: URL.createObjectURL(file),
        formattedSize: formatBytes(file.size),
      })
    );

    if (name === "coverImage") {
      return setupdatecover((prevState) => ({
        isOpen: !prevState.isOpen,
        image: files[0],
      }));
    }
  };
  //croper
  const handleAcceptedFiles2 = (name) => (_files) => {
    const files = _files?.filter((file) => file.size < 5242880);

    if (files.length < _files.length) {
      // return toastr.error(props.t("max_file_size"));
    }
    setaddNewBusiness(false);
    files.map((file) =>
      Object.assign(file, {
        preview: URL.createObjectURL(file),
        formattedSize: formatBytes(file.size),
      })
    );

    if (name === "coverImage") {
      return setupdatecover2((prevState) => ({
        isOpen: !prevState.isOpen,
        image: files[0],
      }));
    }
  };

  const onCoverCrop = (image_base64, data) => {
    const file = DataURIToBlob(image_base64);

    // onUploadFile({ file }, uploadFileSuccess("coverImage"));
    getFile("e", file);
    setselectedFiles((prevState) => [
      prevState[0],
      { preview: image_base64 },
      prevState[2],
    ]);
    setaddNewBusiness(data);
  };
  const onCoverCrop2 = (image_base64, data) => {
    const file = DataURIToBlob(image_base64);

    // onUploadFile({ file }, uploadFileSuccess("coverImage"));
    getFile2("e", file);
    setselectedFiles((prevState) => [
      prevState[0],
      { preview: image_base64 },
      prevState[2],
    ]);
    setaddNewBusiness(data);
  };
  const { errors, name, businessName, password } = state;
  return (
    <>
      <CoverCrop
        {...updatecover}
        toggle={(image) =>
          setupdatecover((prevState) => ({ isOpen: !prevState.isOpen, image }))
        }
        onCoverCrop={onCoverCrop}
        imgRatio={4 / 3}
      />
      <CoverCrop
        {...updatecover2}
        toggle={(image) =>
          setupdatecover2((prevState) => ({ isOpen: !prevState.isOpen, image }))
        }
        onCoverCrop={onCoverCrop2}
        imgRatio={1 / 1}
      />
      <Modal
        size="md"
        show={smShow}
        onHide={() => showModel(false)}
        centered
        aria-labelledby="example-modal-sizes-title-sm"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-sm">
            Social Hub Video Delete ?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            Are you sure to Delete this Video if "Yes" then Click on "Agree"
          </p>
        </Modal.Body>
        <Modal.Footer>
          <button onClick={() => DeleteUser()}>Agree</button>
          <button onClick={() => showModel(false)}>Cancel</button>
        </Modal.Footer>
      </Modal>
      <Modal
        size="md"
        show={addNewBusiness}
        onHide={() => setaddNewBusiness(false)}
        centered
        className="business_modal"
        aria-labelledby="example-modal-sizes-title-sm"
      >
        <div
          className="business_modal_container"
          style={{ borderRadius: "60px" }}
        >
          <div className="add_new_business_form">
            <h5>Add New Video</h5>
            <span>Add video and video details below</span>
          </div>

          <div className="socialHub_field mt-3">
            <div className="row align-items-center mb-2">
              <label className="col-md-12 create_catergory2" htmlFor="fileImg">
                <img
                  src={editIcon}
                  className="edit_icon_img"
                  style={{ width: 30, height: 30, objectFit: "contain" }}
                />
                {loading && <Loader2 color="yellow" />}
                {imgUrls ? (
                  <img
                    src={imgUrls}
                    style={{
                      width: "200px",
                      height: "100px",
                      objectFit: "cover",
                    }}
                  />
                ) : (
                  <img src={im12} />
                )}
                <span className="text-muted">Upload Thumbnail</span>
                {/* <input
                  type="file"
                  name="image"
                  // value={imgUrls}
                  onChange={getFile}
                  id="fileImg"
                /> */}

                <Dropzone
                  onDrop={(acceptedFiles) => {
                    handleAcceptedFiles("coverImage")(acceptedFiles);
                  }}
                >
                  {({ getRootProps, getInputProps }) => {
                    return (
                      <div
                        className="dropzone-single-image"
                        {...getRootProps()}
                      >
                        <a
                          href="/cover-image"
                          onClick={(e) => e.preventDefault()}
                        >
                          <input
                            {...getInputProps()}
                            id="formrow-profile-image-Input"
                            multiple={false}
                            accept="image/*"
                            id="fileImg"
                          />

                          {/* <div className="edit-iocn">Upload</div> */}
                        </a>
                      </div>
                    );
                  }}
                </Dropzone>
              </label>
            </div>

            <TEXTFIELD
              id="standard-error-helper-text"
              label="Add Title"
              name="name"
              value={state.name}
              onChange={handleChange}
              // placeholder="Name"
              notShowIcon={true}
              errors={errors}
            />
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Artist Name"
              name="businessName"
              value={state.businessName}
              onChange={handleChange}
              notShowIcon={true}
              errors={errors}
            />
            <div className="d-flex align-items-center justify-content-between">
              {/* <div> */}
              <TEXTFIELD
                id="standard-error-helper-text"
                label="Video Link"
                name="videoLink"
                value={state.videoLink}
                notShowIcon={true}
                onChange={handleChange}
                errors={errors}
              />
              {/* </div> */}
              <div>
                <span>or</span>
              </div>
              <div className="">
                <label
                  className="col-md-12 upload_video upload_btn"
                  htmlFor="fileVideo"
                >
                  <span>Upload video</span>

                  <input
                    type="file"
                    name="image"
                    accept="video/*"
                    onChange={getVideoFileLink}
                    id="fileVideo"
                  />
                </label>
                {localVideLink && (
                  <a href={localVideLink} target="_blank">
                    {localVideLink.slice(0, 14)}
                  </a>
                )}
              </div>
            </div>
            <div className="col-md-12">
              <label
                className="col-md-12 upload_video upload_btn22"
                htmlFor="fileImg2"
              >
                <span>Upload Artist Image</span>
                <Dropzone
                  onDrop={(acceptedFiles) => {
                    handleAcceptedFiles2("coverImage")(acceptedFiles);
                  }}
                >
                  {({ getRootProps, getInputProps }) => {
                    return (
                      <div
                        className="dropzone-single-image"
                        {...getRootProps()}
                      >
                        <a
                          href="/cover-image"
                          onClick={(e) => e.preventDefault()}
                        >
                          <input
                            {...getInputProps()}
                            id="formrow-profile-image-Input"
                            multiple={false}
                            accept="image/*"
                            id="fileImg2"
                          />
                          {/* <div className="edit-iocn">Upload</div> */}
                        </a>
                      </div>
                    );
                  }}
                </Dropzone>
                {/* <input
                  type="file"
                  name="image"
                  // value={imgUrls}
                  onChange={getFile2}
                  id="fileImg2"
                /> */}
              </label>
              <small>{arrtistImg ? arrtistImg.slice(0, 30) : ""}</small>
            </div>
          </div>
          <div className="btn_div ">
            <button
              className="business_btn2"
              onClick={() => setaddNewBusiness(false)}
            >
              Back
            </button>
            <Button
              className="business_btn "
              // disabled={disabled}
              style={{ backgroundColor: "#1d1d1d" }}
              onClick={addBusinessCall}
            >
              Add
            </Button>
          </div>
        </div>
      </Modal>
      <TopNav routeText={"Social Hub"} navigationText={"Social Hub Details"} />
      {loading && <Loader2 color="yellow" />}
      <div className="user_view_page">
        <div className="user_view_header d-flex justify-content-between align-items-center">
          <h5 className="">Social Hub</h5>
          <div className="d-flex  align-items-center">
            <button
              className="add_new_user_btn"
              onClick={() => setaddNewBusiness(true)}
            >
              <i className={"bx bx-plus"}></i> Add Video
            </button>
          </div>
        </div>

        <div className="row social_card gx-3">
          {user && user.length > 0 ? (
            user.map((d, i) => {
              return (
                <div className="col-md-4 col-sm-12 pb-4 " key={i}>
                  <div className="col-md-12 box_of_video ">
                    <div className="artistImage">
                      <img src={d.thumbnail || im1} />
                    </div>
                    <div className="social_card_content">
                      <div className="d-flex flex-column">
                        <div className="d-flex social_hub_artist_img align-items-center">
                          <img src={d.artistImage || avtar} />
                          <p className="mb-0 " style={{ marginLeft: "10px" }}>
                            <a href={d.videoLink} target="_blank">
                              {d.title}{" "}
                            </a>
                            <p className="mb-0">{d.artistName}</p>
                          </p>
                        </div>
                      </div>
                      <img
                        onClick={() => showModel(true, d.id)}
                        style={{ cursor: "pointer" }}
                        src={deleteBtn}
                      />
                    </div>
                  </div>
                </div>
              );
            })
          ) : (
            <p>No Videos Available !</p>
          )}
        </div>
      </div>
      <Toaster />
    </>
  );
}
