import React, { useState, useEffect } from "react";
import avtar from "../../../assets/images/user_img@2x.png";
import Table from "../../../components/table/Table";
import customerList from "../../../assets/JsonData/customers-list.json";
import TopNav from "../../../components/topnav/TopNav";
import { checkError } from "../../../Auth/helper";
import { useForm } from "react-hook-form";
import Toastt from "../../../comman/toaster";
import Loader2 from "../../../comman/Loader2";
// import * as yup from "yup";
import {
  GetsocialHubVideosList,
  GetUserById,
  GetsocialHubTypesById,
  PostsocialHubTypes,
} from "../../../Auth/ApiUrl";
import Axios from "../../../Auth/Axios";
import toast, { Toaster } from "react-hot-toast";
import Loading from "../../../comman/Loading";
import { withRouter, Link } from "react-router-dom";
import { TEXTFIELD, PASSWORDFIELD } from "../../../comman/inputeFields";
import { Button, Modal } from "react-bootstrap";
const customerTableHead = ["Sr No.", "Name", "Total Videos ", "status", ""];

const renderHead = (item, index) => <th key={index}>{item}</th>;
const renderBody = (item, index, props, showModel, editaddNewBusiness) => {
  console.log(item, "0879879");
  return (
    <>
      <tr key={index}>
        <td>{index + 1}</td>
        <td className="name_with_img">{item.name}</td>
        <td>{item.socialHubVideos.length}</td>
        <td>
          <span className="status">{"active"}</span>
        </td>
        <td className="action_btn">
          <div className="popup_btn">
            <div className="d-flex justify-content-between flex-column">
              <Link to={`/app/social-hub/view/${item.id}`}>
                <p className="pb-0 mb-0">
                  <i className={"bx bx-show"} style={{ minWidth: 20 }}></i>{" "}
                  <span>View Detail</span>
                </p>
              </Link>
              <a
                href="javascript:void(0)"
                onClick={() => editaddNewBusiness("edit", true, item.id)}
              >
                <p className="pb-0 mb-0">
                  <i className={"bx bx-edit"} style={{ minWidth: 20 }}></i>{" "}
                  <span>Edit Detail</span>
                </p>
              </a>
              <a
                href="javascript:void(0)"
                disabled
                onClick={() => showModel(true, item.id)}
              >
                <p className="pb-0 mb-0">
                  <i className={"bx bx-trash"} style={{ minWidth: 20 }}></i>{" "}
                  <span>Delete Category</span>
                </p>
              </a>
            </div>
          </div>
          <i className="bx bx-dots-vertical-rounded "></i>
        </td>
      </tr>
    </>
  );
};

const Businesses = (props) => {
  const [usersList, setUsersList] = useState([]);
  const [loading, setloading] = useState(false);
  const [userId, setUserId] = useState(null);
  const [smShow, setSmShow] = useState(false);
  const [editId, setEditId] = useState(null);
  const [deleteloading, setDeleteloading] = useState(false);
  const [pageNumber, setpageNumber] = useState(null);
  const [addLoding, setaddLoding] = useState(false);
  const [disabled, setDisabled] = useState(true);
  const [addNewBusiness, setaddNewBusiness] = useState(false);
  const [getprevioususerList, setgetprevioususerList] = useState(false);
  const [process, setprocess] = useState("add");
  const [state, setState] = useState({
    name: "",
    errors: {
      name: "",
    },
  });

  const getBusinessList = () => {
    setloading(true);
    Axios.get(PostsocialHubTypes)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setUsersList(res.data.items);
          setpageNumber(res.data.pageNo);
        }
        setloading(false);
      })
      .catch((err) => {
        setloading(false);
        return toast.success("Something went wrong", {
          duration: 4000,
          position: "top-center",
        });
      });
  };
  useEffect(() => {
    getBusinessList();
  }, [getprevioususerList]);
  const setgetprevioususerListFun = () => {
    setgetprevioususerList((prev) => !prev);
  };

  const redirect = () => props.history.push("/app/social-hub");
  const addBusinessCall = () => {
    const { name } = state;
    let data = {
      name: name,
    };
    setaddLoding(true);
    if (process == "add") {
      Axios.post(PostsocialHubTypes, data)
        .then((res) => {
          console.log(res.data, "9090890890890890");
          if (res.data.isSuccess == true) {
            return (
              toast.success("Category Added Successfully", {
                duration: 4000,
                position: "top-right",
              }),
              setaddNewBusiness(false),
              setaddLoding(false),
              redirect()
            );
          } else {
            return (
              toast.error(res.data.error, {
                duration: 4000,
                position: "top-right",
              }),
              setaddLoding(false),
              setloading(false)
            );
          }
        })
        .catch((err) => {
          return (
            toast.success("Something went wrong", {
              duration: 4000,
              position: "top-center",
            }),
            setaddLoding(false),
            setloading(false)
          );
        });
    } else {
      Axios.put(GetsocialHubTypesById + editId, data)
        .then((res) => {
          console.log(res.data, "9090890890890890");
          if (res.data.isSuccess == true) {
            return (
              Toastt.success("Category Updated Successfully"),
              setaddNewBusiness(false),
              setaddLoding(false),
              redirect()
            );
          } else {
            return (
              Toastt.error(res.data.error),
              setaddLoding(false),
              setloading(false)
            );
          }
        })
        .catch((err) => {
          return Toastt.error(err), setaddLoding(false), setloading(false);
        });
    }
  };
  const DeleteUser = () => {
    setDeleteloading(true);
    Axios.delete(PostsocialHubTypes + "/" + userId)
      .then((res) => {
        if (res.data.isSuccess == true) {
          return (
            toast.success(res.data.message, {
              duration: 3000,
              position: "top-right",
            }),
            setDeleteloading(false),
            redirect()
          );
        } else if (res.data.isSuccess == false) {
          return (
            toast.info(res.data.message, {
              duration: 3000,
              position: "top-right",
            }),
            setDeleteloading(false)
          );
        }
      })
      .catch((err) => {
        setDeleteloading(false);
        return toast.error("Something went wrong", {
          duration: 4000,
          position: "top-right",
        });
      });
    setSmShow(false);
  };
  const showModel = (action, id) => {
    setSmShow(action);
    setUserId(id);
  };
  const fillterUserList = (data) => {
    setloading(true);
    setUsersList("");
    if (data) {
      setloading(false);
      setUsersList(data.items);
      console.log(data, "userlist");
    }
  };
  const regExp = RegExp(/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/);
  const handleChange = (e) => {
    e.preventDefault();
    let errors = state.errors;
    switch (e.target.name) {
      case "name":
        errors.name = e.target.value.length < 1 ? "Name Required" : "";
        break;
      default:
        break;
    }
    let err = checkError(errors, state);
    setDisabled(err.disabled);
    setState((prev) => ({ ...prev, errors, [e.target.name]: e.target.value }));
  };
  const editaddNewBusiness = (edit, action, id) => {
    setaddNewBusiness(action);
    setprocess(edit);
    setEditId(id);
    Axios.get(GetsocialHubTypesById + id)
      .then((res) => {
        const response = res.data.data;
        if (res.data.isSuccess == true) {
          console.log(res.data.data);
          setState({ ...state, name: response.name });
        } else if (res.data.isSuccess == false) {
          return toast.info(res.data.message, {
            duration: 3000,
            position: "top-right",
          });
        }
      })
      .catch((err) => {
        return toast.error("Something went wrong", {
          duration: 4000,
          position: "top-right",
        });
      });
    console.log(edit, id, "edit");
  };
  const addProcess = () => {
    setaddNewBusiness(true);
    setprocess("add");
  };
  console.log(process);
  const { errors, name, businessName, email, password } = state;
  return (
    <>
      <Modal
        size="md"
        show={smShow}
        onHide={() => showModel(false)}
        centered
        aria-labelledby="example-modal-sizes-title-sm"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-sm">
            Social Hub Category Delete ?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            Are you sure to Delete this Category if "Yes" then Click on "Agree"
          </p>
        </Modal.Body>
        <Modal.Footer>
          <button onClick={() => DeleteUser()}>Agree</button>
          <button onClick={() => showModel(false)}>Cancel</button>
        </Modal.Footer>
      </Modal>

      {/* add new business */}

      <Modal
        size="md"
        show={addNewBusiness}
        onHide={() => setaddNewBusiness(false)}
        centered
        className="business_modal"
        aria-labelledby="example-modal-sizes-title-sm"
      >
        <div
          className="business_modal_container "
          style={{ borderRadius: "60px" }}
        >
          <div className="add_new_business_form">
            <h5>{process == "add" ? "Add New Category" : "Update Category"}</h5>
            {addLoding && <Loader2 />}
          </div>
          <div className="business_input_field ">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Add Name"
              name="name"
              value={state.name}
              onChange={handleChange}
              notShowIcon={true}
              errors={errors}
            />{" "}
          </div>
          <div className="btn_div ">
            <button
              className="business_btn2"
              onClick={() => setaddNewBusiness(false)}
            >
              Back
            </button>
            <Button
              className="business_btn "
              disabled={disabled}
              style={{ backgroundColor: "#1d1d1d" }}
              onClick={addBusinessCall}
            >
              {process == "add" ? "Add" : "Update"}
            </Button>
          </div>
        </div>
      </Modal>
      <TopNav
        searchBar={true}
        selectAll={true}
        // sendNotificationBtn={true}
        fillterUserListFun={fillterUserList}
        ApiURL={GetsocialHubVideosList}
        setgetprevious={setgetprevioususerListFun}
      />
      <div className="table_container">
        <div className="user_header d-flex justify-content-between align-items-center">
          <h3 className="page-header">social hub categories</h3>
          <button className="add_new_user_btn" onClick={() => addProcess()}>
            <i className={"bx bx-plus"}></i> New Category
          </button>
        </div>
        <div className="row">
          <div className="col-12">
            <div className="card">
              <div className="card__body">
                {deleteloading && <Loading deleting={"Deleting"} />}
                {usersList.length > 0 ? (
                  <Table
                    limit="10"
                    headData={customerTableHead}
                    renderHead={(item, index) => renderHead(item, index)}
                    bodyData={usersList}
                    pageNumber={pageNumber}
                    renderBody={(item, index) =>
                      renderBody(
                        item,
                        index,
                        props,
                        showModel,
                        editaddNewBusiness
                      )
                    }
                  />
                ) : (
                  loading && <Loading />
                )}
                {!loading && usersList.length <= 0 && (
                  <p className="mb-0 text-center">No Business Found</p>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <Toaster />
    </>
  );
};

export default withRouter(Businesses);
