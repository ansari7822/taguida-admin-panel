import React, { useState, useEffect } from "react";
import avtar from "../../../assets/images/user_img@2x.png";
import Table from "../../../components/table/Table";
import customerList from "../../../assets/JsonData/customers-list.json";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import MenuItem from "@mui/material/MenuItem";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";
import Select from "@mui/material/Select";
import TopNav from "../../../components/topnav/TopNav";
import { checkError } from "../../../Auth/helper";
// import * as yup from "yup";
import {
  GetBusinessesList,
  GetUserById,
  GetCategories,
  CreateBusinessByUser,
} from "../../../Auth/ApiUrl";
import Axios from "../../../Auth/Axios";
import toast, { Toaster } from "react-hot-toast";
import Loading from "../../../comman/Loading";
import MailIcon from "@mui/icons-material/Mail";
import CallIcon from "@mui/icons-material/Call";
import HttpsIcon from "@mui/icons-material/Https";
import PersonIcon from "@mui/icons-material/Person";
import { withRouter, Link } from "react-router-dom";
import { TEXTFIELD, PASSWORDFIELD } from "../../../comman/inputeFields";
import { Button, Modal } from "react-bootstrap";
const customerTableHead = [
  // '',
  "Business Info",
  "email",
  "phone ",
  "status",
  "",
];

const renderHead = (item, index) => <th key={index}>{item}</th>;
const renderBody = (item, index, props, showModel, smShow) => {
  return (
    <>
      <tr key={index}>
        {/* <td>{}</td> */}
        <td className="name_with_img">
          {/* <img src={item.imgUrl || avtar} /> */}
          {item.name}
        </td>
        <td>{item.email}</td>
        <td>{item.phone}</td>
        <td>
          <span className="status">{item.status}</span>
        </td>
        <td className="action_btn">
          <div className="popup_btn">
            <div className="d-flex justify-content-between flex-column">
              <Link to={`/app/businesses/view/${item.id}`}>
                <p className="pb-0 mb-0">
                  <i className={"bx bx-show"} style={{ minWidth: 20 }}></i>{" "}
                  <span>View Details</span>
                </p>
              </Link>
              {/* <a
                href="javascript:void(0)"
                disabled
                onClick={() => showModel(true, item.id)}
              >
                <p className="pb-0 mb-0">
                  <i className={"bx bx-trash"} style={{ minWidth: 20 }}></i>{" "}
                  <span>Delete User</span>
                </p>
              </a> */}
              {/* <a href="javascript:void(0)" onClick={() => alert(item.id)}>
                <p className="pb-0 mb-0">
                  <i className={"bx bx-block"} style={{ minWidth: 20 }}></i>{" "}
                  <span>Edit</span>
                </p>
              </a> */}
            </div>
          </div>
          <i className="bx bx-dots-vertical-rounded "></i>
        </td>
      </tr>
    </>
  );
};

const Businesses = (props) => {
  const [usersList, setUsersList] = useState([]);
  const [loading, setloading] = useState(false);
  const [userId, setUserId] = useState(null);
  const [smShow, setSmShow] = useState(false);
  const [deleteloading, setDeleteloading] = useState(false);
  const [pageNumber, setpageNumber] = useState(null);
  const [disabled, setDisabled] = useState(true);
  const [addNewBusiness, setaddNewBusiness] = useState(false);
  const [getprevioususerList, setgetprevioususerList] = useState(false);
  const [state, setState] = useState({
    name: "",
    businessName: "",
    phone: "",
    categoryId: "",
    email: "",
    type: [],
    countryCode: "",
    password: "",
    errors: {
      name: "",
      phone: "",
      countryCode: "",
      email: "",
      password: "",
      businessName: "",
    },
  });

  const getBusinessList = () => {
    setloading(true);
    let newUr = GetBusinessesList + "?pageSize=1000" + "&";
    Axios.get(newUr)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setUsersList(res.data.items);
          setpageNumber(res.data.pageNo);
        }
        setloading(false);
        console.log(res, "data");
      })
      .catch((err) => {
        setloading(false);
        return toast.success("Something went wrong", {
          duration: 4000,
          position: "top-center",
        });
      });
  };
  useEffect(() => {
    getBusinessList();
  }, [getprevioususerList]);

  const redirect = () => props.history.push("/app/businesses");
  const addBusinessCall = () => {
    const {
      businessName,
      email,
      password,
      name,
      countryCode,
      phone,
      categoryId,
    } = state;
    let data = {
      businessName,
      email,
      password,
      name,
      categoryId,
      countryCode,
      phone,
    };

    Axios.post(CreateBusinessByUser, data)
      .then((res) => {
        console.log(res.data, "9090890890890890");
        if (res.data.isSuccess == true) {
          toast.success("Business Added Successfully", {
            duration: 4000,
            position: "top-right",
          });
          setTimeout(() => {
            redirect();
          }, 100);
        } else {
          toast.error(res.data.error, {
            duration: 4000,
            position: "top-right",
          });
        }
        setloading(false);
        console.log(res, "data");
      })
      .catch((err) => {
        setloading(false);
        return toast.success("Something went wrong", {
          duration: 4000,
          position: "top-center",
        });
      });
  };
  const DeleteUser = () => {
    setDeleteloading(true);
    Axios.delete(GetUserById + userId)
      .then((res) => {
        if (res.data.isSuccess == true) {
          return (
            toast.success(res.data.message, {
              duration: 3000,
              position: "top-center",
            }),
            props.history.push("/")
          );
        } else if (res.data.isSuccess == false) {
          return toast.info(res.data.message, {
            duration: 3000,
            position: "top-center",
          });
        }
        setDeleteloading(false);
      })
      .catch((err) => {
        setDeleteloading(false);
        return toast.error("Something went wrong", {
          duration: 4000,
          position: "top-center",
        });
      });
    setSmShow(false);
  };
  const showModel = (action, id) => {
    setSmShow(action);
    setUserId(id);
  };
  const fillterUserList = (data) => {
    setloading(true);
    setUsersList("");
    if (data) {
      setloading(false);
      setUsersList(data.items);
      console.log(data, "userlist");
    }
  };
  const regExp = RegExp(/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/);
  const handleChange = (e) => {
    e.preventDefault();
    let errors = state.errors;
    switch (e.target.name) {
      case "email":
        errors.email = regExp.test(e.target.value)
          ? ""
          : "Email address is invalid";
        break;
      case "name":
        errors.name = e.target.value.length < 1 ? "Name Required" : "";
        break;
      case "businessName":
        errors.businessName = e.target.value.length < 1 ? "Name Required" : "";
        break;
      case "phone":
        errors.phone = e.target.value.length < 1 ? "Phone Number Required" : "";
        break;
      case "countryCode":
        errors.countryCode =
          e.target.value.length < 1 ? "CountryCode Required" : "";
        break;
      case "password":
        errors.password =
          e.target.value.length < 6 ? "Password 6 characaters required" : "";
        break;
      default:
        break;
    }
    let err = checkError(errors, state);
    setDisabled(err.disabled);
    setState((prev) => ({ ...prev, errors, [e.target.name]: e.target.value }));
  };
  const setgetprevioususerListFun = () => {
    setgetprevioususerList((prev) => !prev);
  };
  useEffect(() => {
    Axios.get(GetCategories)
      .then((res) => {
        if (res.data.isSuccess == true) {
          // setUsersList(res.data.items);
          let data = res.data.items.filter((d, i) => d.type == "business");
          if (data) {
            let type = data.map((d) => {
              return {
                [d.id]: d.name,
              };
            });
            setState((prev) => ({ ...prev, type: type }));
          }
        }
      })
      .catch((err) => console.log("err"));
  }, []);
  const { errors, name, businessName, email, password, type, categoryId } =
    state;
  return (
    <>
      <Modal
        size="md"
        show={smShow}
        onHide={() => showModel(false)}
        centered
        aria-labelledby="example-modal-sizes-title-sm"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-sm">
            User Delete ?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Are you sure to Delete this user if "Yes" then Click on "Agree"</p>
        </Modal.Body>
        <Modal.Footer>
          <button onClick={() => DeleteUser()}>Agree</button>
          <button onClick={() => showModel(false)}>Cancel</button>
        </Modal.Footer>
      </Modal>

      {/* add new business */}
      <Modal
        size="md"
        show={addNewBusiness}
        onHide={() => setaddNewBusiness(false)}
        centered
        className="business_modal"
        aria-labelledby="example-modal-sizes-title-sm"
      >
        <div
          className="business_modal_container"
          style={{ borderRadius: "60px" }}
        >
          <div className="add_new_business_form">
            <h5>Add New Business</h5>
            <span>Enter the business details below</span>
          </div>
          <div className="business_input_field">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Name"
              name="name"
              value={state.name}
              onChange={handleChange}
              Icon={PersonIcon}
              // placeholder="Name"
              errors={errors}
            />

            <TEXTFIELD
              id="standard-error-helper-text"
              label="Business Name"
              name="businessName"
              value={state.businessName}
              onChange={handleChange}
              // placeholder="Business Name"
              Icon={PersonIcon}
              errors={errors}
            />
            <div className="col-md-12">
              <FormControl
                variant="standard"
                sx={{ m: 1, minWidth: "100%", textAlign: "left" }}
              >
                <InputLabel id="demo-simple-select-standard-label">
                  Category Type
                </InputLabel>
                <Select
                  labelId="demo-simple-select-standard-label"
                  id="demo-simple-select-standard"
                  value={categoryId}
                  name="categoryId"
                  onChange={handleChange}
                  label="Category Type"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  // InputProps={{
                  //   startAdornment: (
                  //     <InputAdornment>
                  //       <IconButton className="business_bt">
                  //         <AccountCircle />
                  //       </IconButton>
                  //     </InputAdornment>
                  //   ),
                  // }}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  {type &&
                    type.length > 0 &&
                    type.map((d, i) => {
                      for (let dd in d) {
                        return (
                          <MenuItem value={dd}>
                            <em>{d[dd]}</em>
                          </MenuItem>
                        );
                      }
                    })}
                </Select>{" "}
              </FormControl>
            </div>
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Email"
              name="email"
              value={state.email}
              // placeholder="Email"
              onChange={handleChange}
              Icon={MailIcon}
              errors={errors}
            />
            <div className="row">
              <div className="col-md-3">
                <TEXTFIELD
                  id="standard-error-helper-text"
                  label="Phone "
                  name="countryCode"
                  // placeholder="Code"
                  type="number"
                  value={state.countryCode}
                  onChange={handleChange}
                  Icon={CallIcon}
                  // errors={errors}
                />
              </div>
              <div className="col-md-9">
                <TEXTFIELD
                  id="standard-error-helper-text"
                  label=" "
                  type="number"
                  name="phone"
                  // placeholder="Phone Number"
                  notShowIcon={true}
                  value={state.phone}
                  onChange={handleChange}
                  // Icon={MarkunreadIcon}
                  errors={errors}
                />
              </div>
            </div>
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Password"
              name="password"
              type="password"
              value={state.password}
              onChange={handleChange}
              // placeholder="Password"
              Icon={HttpsIcon}
              errors={errors}
            />
          </div>
          <div className="btn_div ">
            <button
              className="business_btn2"
              onClick={() => setaddNewBusiness(false)}
            >
              Back
            </button>
            <Button
              className="business_btn "
              disabled={disabled}
              style={{ backgroundColor: "#1d1d1d" }}
              onClick={addBusinessCall}
            >
              Add
            </Button>
          </div>
        </div>
      </Modal>
      <TopNav
        searchBar={true}
        fillterClearUrl={"/app/businesses/list"}
        setgetprevious={setgetprevioususerListFun}
        fillterUserListFun={fillterUserList}
        ApiURL={GetBusinessesList}
      />
      <div className="table_container">
        <div className="user_header d-flex justify-content-between align-items-center">
          <h3 className="page-header">businesses</h3>
          <button
            className="add_new_user_btn"
            onClick={() => setaddNewBusiness(true)}
          >
            <i className={"bx bx-plus"}></i> New Business
          </button>
        </div>
        <div className="row">
          <div className="col-12">
            <div className="card">
              <div className="card__body">
                {deleteloading && <Loading deleting={"Deleting"} />}
                {usersList.length > 0 ? (
                  <Table
                    limit="10"
                    headData={customerTableHead}
                    renderHead={(item, index) => renderHead(item, index)}
                    bodyData={usersList}
                    pageNumber={pageNumber}
                    renderBody={(item, index) =>
                      renderBody(item, index, props, showModel, smShow)
                    }
                  />
                ) : (
                  loading && <Loading />
                )}
                {!loading && usersList.length <= 0 && (
                  <p className="mb-0 text-center">No Business Found</p>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <Toaster />
    </>
  );
};

export default withRouter(Businesses);
