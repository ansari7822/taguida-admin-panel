import React, { useState, useEffect } from "react";
import { withRouter, Link } from "react-router-dom";
import avtar from "../../../assets/images/user_img@2x.png";
import im1 from "../../../assets/images/c_img_1.png";
import TopNav from "../../../components/topnav/TopNav";
import Axios from "../../../Auth/Axios";
import {
  GetCategoriesById,
  CreateSubCategory,
  GetSubCategoryById,
} from "../../../Auth/ApiUrl";
import toast, { Toaster } from "react-hot-toast";
import slidImg from "../../../assets/images/slider_img.png";
import { Button, Carousel, Modal } from "react-bootstrap";
export default function View(props) {
  const { id } = props.match.params;
  const [user, setUser] = useState();
  const [userId, setUserId] = useState(null);
  const [smShow, setSmShow] = useState(false);
  const [subCat, setsubcat] = useState();

  const [loading, setloading] = useState(false);
  useEffect(() => {
    setloading(true);
    Axios.get(GetCategoriesById + id)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setUser(res.data.data);
        }
        setloading(false);
        console.log(res, "data");
      })
      .catch((err) => {
        setloading(false);
        return toast.success("Something went wrong", {
          duration: 4000,
          position: "top-center",
        });
      });
    let newUrl = CreateSubCategory + `?categoryId=${id}`;
    Axios.get(newUrl)
      .then((res) => {
        if (res.data.isSuccess == true) {
          console.log(res.data, "data");
          setsubcat(res.data.items);
        }
      })
      .catch((err) => {
        console.log(err, "err");
      });
  }, []);

  const DeleteUser = () => {
    setloading(true);
    Axios.delete(GetSubCategoryById + userId)
      .then((res) => {
        if (res.data.isSuccess == true) {
          return (
            toast.success(res.data.message, {
              duration: 3000,
              position: "top-right",
            }),
            setloading(false),
            props.history.push(`/app/categories`)
          );
        } else if (res.data.isSuccess == false) {
          return toast.info(res.data.message, {
            duration: 3000,
            position: "top-right",
          });
        }
        setloading(false);
      })
      .catch((err) => {
        setloading(false);
        return toast.error("Something went wrong", {
          duration: 4000,
          position: "top-riht",
        });
      });
    setSmShow(false);
  };
  const showModel = (action, id) => {
    setSmShow(action);
    setUserId(id);
  };
  return (
    <>
      <Modal
        size="md"
        show={smShow}
        onHide={() => showModel(false)}
        centered
        aria-labelledby="example-modal-sizes-title-sm"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-sm">
            Sub Category Delete ?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            Are you sure to Delete this Sub Category if "Yes" then Click on
            "Agree"
          </p>
        </Modal.Body>
        <Modal.Footer>
          <button onClick={() => DeleteUser()}>Agree</button>
          <button onClick={() => showModel(false)}>Cancel</button>
        </Modal.Footer>
      </Modal>
      <TopNav routeText={"Categories"} navigationText={"Category Details"} />
      <div className="user_view_page mb-3 ">
        <div className="user_view_header d-flex justify-content-between align-items-center">
          <h5 className="">Category </h5>
          <Link
            to={`/app/categories/create-subcategory/${props.match.params.id}`}
          >
            <button className="add_new_user_btn">
              <i className={"bx bx-plus"}></i> Add New Sub-Category
            </button>
          </Link>
        </div>
        {user && user && (
          <div className="row m-0 p-0">
            <div className="col-md-4 view_catergory" style={{ height: "100%" }}>
              <img src={user?.imgUrl || im1} />
            </div>
            <div className="col-md-8">
              <h6>{user.name}</h6>
              <p>{user.description || "Description N/A"}</p>
            </div>
          </div>
        )}
        <div className="row">
          <div className="col-md-12 prefrered_header d-flex ">
            <h5>{`${"All Sub-Categories Details "} ${" "} `} </h5>
            <span> (scroll horizontali )</span>
          </div>
        </div>
        <div
          className="row flex-nowrap sub_cat_scroll"
          style={{ overflowX: "scroll" }}
        >
          {subCat &&
            subCat?.map((dkey, i) => (
              <div className="col-md-4 col-sm-12" key={i}>
                <div className="view_card">
                  <Carousel nextIcon={false} prevIcon={false}>
                    <Carousel.Item>
                      <img
                        className="d-block w-100"
                        src={dkey.imgUrl || slidImg}
                        alt="First slide"
                      />
                    </Carousel.Item>
                  </Carousel>
                  <div className="slider_content">
                    <div className="d-flex justify-content-between align-items-center">
                      <h5>{dkey.name}</h5>

                      <div className="categories_action_btn ">
                        <i
                          className="bx bx-dots-vertical-rounded "
                          style={{ fontSize: "20px" }}
                        ></i>
                        <span className="category_poppu_bnt">
                          <Link
                            to={`/app/categories/updated-subcat/${dkey.id}`}
                          >
                            <p className="pb-0 mb-0">
                              <i
                                className={"bx bx-show"}
                                style={{ minWidth: 20 }}
                              ></i>{" "}
                              <span>Edit</span>
                            </p>
                          </Link>
                          <a
                            href="javascript:void(0)"
                            disabled
                            onClick={() => showModel(true, dkey.id)}
                          >
                            <p className="pb-0 mb-0">
                              <i
                                className={"bx bx-trash"}
                                style={{ minWidth: 20 }}
                              ></i>{" "}
                              <span>Delete</span>
                            </p>
                          </a>
                        </span>
                      </div>
                    </div>
                    <div className="tags_container">
                      {dkey.tags
                        ? dkey.tags
                            .split(",")
                            .slice(0, 3)
                            .map((d) => <span className="tags">{d}</span>)
                        : ""}
                    </div>
                    <p className="mb-0">
                      {dkey.description ||
                        "Lorem ipsum dolor sit amet, consectetur adipisicin"}
                    </p>
                  </div>
                </div>
              </div>
            ))}
        </div>
      </div>
      <Toaster />
    </>
  );
}
