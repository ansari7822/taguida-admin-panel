import React, { useState, useEffect } from "react";
import avtar from "../../../assets/images/user_img@2x.png";
import customerList from "../../../assets/JsonData/customers-list.json";
import TopNav from "../../../components/topnav/TopNav";
import { GetCategories, GetCategoriesById } from "../../../Auth/ApiUrl";
import Axios from "../../../Auth/Axios";
import toast, { Toaster } from "react-hot-toast";
import Loading from "../../../comman/Loading";
import { withRouter, Link } from "react-router-dom";
import slidImg from "../../../assets/images/slider_img.png";
import slidImg2 from "../../../assets/images/c_img_1.png";
import nextIcImg from "../../../assets/images/nextIcon.png";
import prevIcImg from "../../../assets/images/prevIcon.png";
import { Button, Carousel, Modal } from "react-bootstrap";
import PaginationCom from "../../../comman/pagination";

const Category = (props) => {
  const [usersList, setUsersList] = useState([]);
  const [loading, setloading] = useState(false);
  const [userId, setUserId] = useState(null);
  const [smShow, setSmShow] = useState(false);
  const [getprevioususerList, setgetprevioususerList] = useState(false);
  const [deleteloading, setDeleteloading] = useState(false);
  const [pageNumber, setpageNumber] = useState(null);
  const [state, setstate] = useState({
    pageNo: 1,
    pageSize: 0,
    total: 0,
    totalRecords: 0,
    count: null,
  });
  const [nextIcon, setnextIcon] = useState({
    nextIconBtn: (
      <span className="slider_btns">
        <img src={nextIcImg} />
      </span>
    ),
    prevIconBtn: (
      <span className="slider_btns">
        <img src={prevIcImg} />
      </span>
    ),
  });
  const { count, pageNo } = state;
  const getNextPage = (event, value) => {
    // e.preventDefault();
    setstate((prev) => ({ ...prev, pageNo: value }));
    // setPage(value);
  };
  const getIdealist = () => {
    setloading(true);
    let newUr = GetCategories + "?pageSize=9" + "&" + `pageNo=${pageNo}`;
    Axios.get(newUr)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setUsersList(res.data.items);
          let countPage;
          if (res.data.totalRecords) {
            countPage =
              (res.data.totalRecords + res.data.pageSize - 1) /
              res.data.pageSize;
          }
          setstate({ ...res.data, count: Math.floor(countPage) });
          setpageNumber(res.data.pageNo);
        }
        setloading(false);
        console.log(res, "data");
      })
      .catch((err) => {
        setloading(false);
        return toast.success("Something went wrong", {
          duration: 4000,
          position: "top-center",
        });
      });
  };
  useEffect(() => {
    getIdealist();
  }, [getprevioususerList, state.pageNo]);

  const fillterUserList = (data) => {
    setloading(true);
    setUsersList("");
    if (data) {
      setloading(false);
      setUsersList(data.items);
      console.log(data, "userlist");
    }
  };
  const DeleteUser = () => {
    setDeleteloading(true);
    Axios.delete(GetCategoriesById + userId)
      .then((res) => {
        if (res.data.isSuccess == true) {
          return (
            toast.success(res.data.message, {
              duration: 3000,
              position: "top-center",
            }),
            setDeleteloading(false),
            props.history.push("/app/categories")
          );
        } else if (res.data.isSuccess == false) {
          return toast.info(res.data.message, {
            duration: 3000,
            position: "top-center",
          });
        }
        setDeleteloading(false);
      })
      .catch((err) => {
        setDeleteloading(false);
        return toast.error("Something went wrong", {
          duration: 4000,
          position: "top-center",
        });
      });
    setSmShow(false);
  };
  const showModel = (action, id) => {
    setSmShow(action);
    setUserId(id);
  };
  const refreshPage = () => {
    setgetprevioususerList(true);
  };
  const blockUnblock = (id, sts) => {
    console.log(id, sts, "98908890");
    let body = {
      status: sts,
    };
    let newUr = GetCategories + "/" + id;
    Axios.put(newUr, body)
      .then((res) => {
        console.log(res.data, "09980");
        if (res.data.isSuccess == true) {
          return (
            toast.success("Success", {
              duration: 1000,
              position: "top-center",
            }),
            refreshPage()
          );
          // props.history.push("/")
        } else if (res.data.isSuccess == false) {
          return toast.info(res.data.message, {
            duration: 3000,
            position: "top-center",
          });
        }
        setDeleteloading(false);
      })
      .catch((err) => {
        return toast.error("Something went wrong", {
          duration: 4000,
          position: "top-center",
        });
      });
  };
  const setgetprevioususerListFun = () => {
    setgetprevioususerList((prev) => !prev);
  };
  const { nextIconBtn, prevIconBtn } = nextIcon;
  console.log(usersList, "usersList, setUsersList");
  return (
    <>
      <Modal
        size="md"
        show={smShow}
        onHide={() => showModel(false)}
        centered
        aria-labelledby="example-modal-sizes-title-sm"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-sm">
            User Delete ?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Are you sure to Delete this user if "Yes" then Click on "Agree"</p>
        </Modal.Body>
        <Modal.Footer>
          <button onClick={() => DeleteUser()}>Agree</button>
          <button onClick={() => showModel(false)}>Cancel</button>
        </Modal.Footer>
      </Modal>
      <TopNav
        searchBar={true}
        fillterUserListFun={fillterUserList}
        ApiURL={GetCategories}
        setgetprevious={setgetprevioususerListFun}
      />
      <div className="table_container">
        <div className="user_header d-flex justify-content-between align-items-center">
          <h3 className="page-header">categories</h3>
          <Link to="/app/categories/create-category">
            <button className="add_new_user_btn">
              <i className={"bx bx-plus"}></i> New category
            </button>
          </Link>
        </div>
        <div className="row">
          <div className="col-12">
            <div className="card">
              <div className="card__body">
                {deleteloading && <Loading deleting={"Deleting"} />}
                <div className="slider_container">
                  {loading && <Loading />}
                  {usersList &&
                    usersList?.map((d, i) => (
                      <div className="slider_card">
                        <Carousel nextIcon={false} prevIcon={false}>
                          <Carousel.Item>
                            <img
                              className="d-block w-100"
                              src={d.imgUrl || slidImg}
                              alt="First slide"
                            />
                          </Carousel.Item>
                        </Carousel>
                        <div className="slider_content">
                          <div className="d-flex justify-content-between align-items-center">
                            <h5>{d.name.slice(0, 19)}</h5>
                            <div className="categories_action_btn ">
                              <i
                                className="bx bx-dots-vertical-rounded "
                                style={{ fontSize: "20px" }}
                              ></i>
                              <span className="category_poppu_bnt">
                                <Link to={`/app/categories/view/${d.id}`}>
                                  <p className="pb-0 mb-0">
                                    <i
                                      className={"bx bx-show"}
                                      style={{ minWidth: 20 }}
                                    ></i>{" "}
                                    <span>View Details</span>
                                  </p>
                                </Link>
                                <Link to={`/app/categories/updated/${d.id}`}>
                                  <p className="pb-0 mb-0">
                                    <i
                                      className={"bx bx-show"}
                                      style={{ minWidth: 20 }}
                                    ></i>{" "}
                                    <span>Edit</span>
                                  </p>
                                </Link>
                                <a
                                  href="javascript:void(0)"
                                  disabled
                                  onClick={() => showModel(true, d.id)}
                                >
                                  <p className="pb-0 mb-0">
                                    <i
                                      className={"bx bx-trash"}
                                      style={{ minWidth: 20 }}
                                    ></i>{" "}
                                    <span>Delete Category</span>
                                  </p>
                                </a>
                              </span>
                            </div>
                          </div>
                          <p className="mb-0">
                            {(d.description && d.description?.slice(0, 90)) ||
                              "Lorem ipsum dolor sit amet, consectetur adipisicin"}
                            {d.description && d.description.length > 90
                              ? "..."
                              : null}
                          </p>
                        </div>
                      </div>
                    ))}
                </div>
                {!loading && usersList.length <= 0 && (
                  <p className="mb-0 text-center">No Business Found</p>
                )}
              </div>
              <PaginationCom
                count={count}
                getNextPage={getNextPage}
                loading={loading}
                usersList={usersList}
              />
            </div>
          </div>
        </div>
      </div>
      <Toaster />
    </>
  );
};

export default withRouter(Category);
