import React, { useState, useEffect } from "react";
import { withRouter, Link, useParams } from "react-router-dom";
import im1 from "../../../assets/images/uploadImg.png";
import TopNav from "../../../components/topnav/TopNav";
import Axios from "../../../Auth/Axios";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";

import CoverCrop from "../../../comman/CoverCrop";
import DataURIToBlob from "../../../comman/DataBlob";
import Dropzone from "react-dropzone";

import { TEXTFIELD } from "../../../comman/inputeFields";
import {
  CreateSubCategory,
  UploadImage,
  GetSubCategoryById,
} from "../../../Auth/ApiUrl";
import toast, { Toaster } from "react-hot-toast";
import Loader2 from "../../../comman/Loader2";

/**
 * Formats the size
 */
function formatBytes(bytes, decimals = 2) {
  if (bytes === 0) return "0 Bytes";
  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

  const i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}
function View(props) {
  const [disabled, setDisabled] = useState(true);
  const [user, setUser] = useState();
  const [updatecover, setupdatecover] = useState({
    isOpen: false,
    image: null,
  });
  const [state, setState] = useState({
    name: "",
    description: "",
    tags: [],
    errors: {
      name: "",
      description: "",
    },
  });
  const { updateCatId } = useParams();
  const [imgUrls, setimgUrls] = useState("");
  const [tagValue, settagValue] = useState("");
  console.log(updateCatId, "09098");
  useEffect(() => {
    if (updateCatId) {
      Axios.get(GetSubCategoryById + updateCatId)
        .then((res) => {
          const response = res.data.data;
          if (res.data.isSuccess == true) {
            setloading(false);
            console.log(response);
            setState({
              ...state,
              name: response.name,
              description: response.description,
              tags: response.tags ? response.tags.split(",") : [],
            });
            setimgUrls(response.imgUrl);
          } else {
            return (
              toast.error(res.data.error, {
                duration: 4000,
                position: "top-right",
              }),
              setloading(false)
            );
          }
        })
        .catch((err) => {
          console.log(err, "87987");
        });
    }
  }, []);

  const handleChange = (e) => {
    e.preventDefault();
    // const { name, value } = e.traget;
    switch (e.target.name) {
      case "name":
        errors.name = e.target.value.length < 1 ? "Name Required" : "";
        break;
      case "description":
        errors.description =
          e.target.value.length < 1 ? "Description Required" : "";
        break;
      case "tagValue":
        settagValue(e.target.value);
        break;
      default:
        break;
    }
    setState((prev) => ({ ...prev, errors, [e.target.name]: e.target.value }));
  };
  const addTage = () => {
    if (tagValue !== "" || undefined || null) {
      setState({ ...state, tags: [...tags, tagValue] });
    }
    settagValue("");
  };
  const removeTage = (ind) => {
    const { tags } = state;
    const tagsNe = tags.filter((d, i) => i !== ind);
    setState({ ...state, tags: tagsNe });
  };
  const [loading, setloading] = useState(false);
  // useEffect(() => {
  //   setloading(true);
  //   Axios.get(GetCategoriesById + id)
  //     .then((res) => {
  //       if (res.data.isSuccess == true) {
  //         setUser(res.data.data);
  //       }
  //       setloading(false);
  //       console.log(res, "data");
  //     })
  //     .catch((err) => {
  //       setloading(false);
  //       return toast.success("Something went wrong", {
  //         duration: 4000,
  //         position: "top-center",
  //       });
  //     });
  // }, []);

  const getFile = (e, fd) => {
    // setState({ ...state, imgUpload: e.target.files[0] });
    setloading(true);
    const formData = new FormData();
    formData.append("media", fd);
    Axios.post(UploadImage, formData)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setloading(false);
          setimgUrls(res.data.data.url);
        } else {
          return (
            toast.error(res.data.error, {
              duration: 4000,
              position: "top-right",
            }),
            setloading(false)
          );
        }
      })
      .catch((err) => {
        console.log(err, "87987");
      });
  };

  //croper

  const handleAcceptedFiles = (name) => (_files) => {
    const files = _files?.filter((file) => file.size < 5242880);
    if (files.length < _files.length) {
      // return toastr.error(props.t("max_file_size"));
    }
    files.map((file) =>
      Object.assign(file, {
        preview: URL.createObjectURL(file),
        formattedSize: formatBytes(file.size),
      })
    );
    if (name === "coverImage") {
      return setupdatecover((prevState) => ({
        isOpen: !prevState.isOpen,
        image: files[0],
      }));
    }
  };
  const onCoverCrop = (image_base64, data) => {
    const file = DataURIToBlob(image_base64);
    getFile("e", file);
  };

  const submit = () => {
    const { name, description, tags } = state;
    let data = {
      name: name,
      imgUrl: imgUrls,
      tags: tags.join(","),
      categoryId: props.match.params.subCatId,
      description: description,
    };
    setloading(true);

    if (updateCatId) {
      data.categoryId = updateCatId;
    }
    if (updateCatId) {
      Axios.put(GetSubCategoryById + updateCatId, data)
        .then((res) => {
          if (res.data.isSuccess == true) {
            return (
              (toast.success("Sub Category Updated", {
                duration: 2000,
                position: "top-right",
              }),
              setTimeout(() => {
                props.history.push("/app/categories/list");
              }, 100)),
              setloading(false)
            );
          } else {
            return (
              toast.error(res.data.error, {
                duration: 4000,
                position: "top-right",
              }),
              setloading(false)
            );
          }
        })
        .catch((err) => {
          console.log(err, "87987");
        });
    } else {
      Axios.post(CreateSubCategory, data)
        .then((res) => {
          if (res.data.isSuccess == true) {
            return (
              (toast.success("Sub Category Added", {
                duration: 2000,
                position: "top-right",
              }),
              setTimeout(() => {
                props.history.push("/app/categories/list");
              }, 100)),
              setloading(false)
            );
          } else {
            return (
              toast.error(res.data.error, {
                duration: 4000,
                position: "top-right",
              }),
              setloading(false)
            );
          }
        })
        .catch((err) => {
          console.log(err, "87987");
        });
    }
  };
  const { errors, name, imgUrl, description, type, tags } = state;
  console.log(tags, "tags");
  return (
    <>
      <CoverCrop
        {...updatecover}
        toggle={(image) =>
          setupdatecover((prevState) => ({ isOpen: !prevState.isOpen, image }))
        }
        onCoverCrop={onCoverCrop}
        imgRatio={4 / 3}
      />
      {loading && <Loader2 />}
      <TopNav
        routeText={"Categories"}
        navigationText={
          updateCatId ? "Update Sub-Category" : "Add New Sub-Category"
        }
      />
      <div className="user_view_page mb-3">
        <div className="user_view_header  align-items-center pb-0">
          <h5 className="">
            {updateCatId ? "Update Sub-Category" : "Add New Sub-Category"}
          </h5>
          <p>Enter the new Sub-Category details below</p>
        </div>
        <div className="row align-items-center mb-2">
          <label className="col-md-4 create_catergory" htmlFor="fileImg">
            {imgUrls ? (
              <img
                src={imgUrls}
                style={{ width: "200px", height: "100px", objectFit: "cover" }}
              />
            ) : (
              <img src={im1} />
            )}
            <span className="text-muted">
              {updateCatId ? "Update Image" : "Upload Image"}
            </span>
            {/* <input
              type="file"
              name="image"
              value={imgUrl}
              onChange={getFile}
              id="fileImg"
            /> */}
            <Dropzone
              onDrop={(acceptedFiles) => {
                handleAcceptedFiles("coverImage")(acceptedFiles);
              }}
            >
              {({ getRootProps, getInputProps }) => {
                return (
                  <div className="dropzone-single-image" {...getRootProps()}>
                    <a href="/cover-image" onClick={(e) => e.preventDefault()}>
                      <input
                        {...getInputProps()}
                        id="formrow-profile-image-Input"
                        multiple={false}
                        accept="image/*"
                        id="fileImg"
                      />

                      {/* <div className="edit-iocn">Upload</div> */}
                    </a>
                  </div>
                );
              }}
            </Dropzone>
          </label>
        </div>
        <div className="row">
          <div className="col-md-6">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Category Name"
              name="name"
              className="mt-0"
              value={name}
              notShowIcon={true}
              onChange={handleChange}
              errors={errors}
            />
          </div>

          <div className="col-md-5">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Tags"
              name="tagValue"
              value={tagValue}
              className="mt-0"
              notShowIcon={true}
              onChange={handleChange}
            />
          </div>
          <div className="col-md-1">
            <button onClick={addTage}>
              <i className={"bx bx-plus"}></i>
            </button>
          </div>

          <div className="col-md-12">
            <span className="">
              {tags && tags && tags.length > 0
                ? tags.map((d, ind) => {
                    return (
                      <span className="tags2 mb-2">
                        {d}
                        <i
                          className="delete_tag"
                          onClick={() => removeTage(ind)}
                        >
                          <CloseRoundedIcon fontSize="small" />
                        </i>
                      </span>
                    );
                  })
                : ""}
            </span>
          </div>
          <div className="col-md-6">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Description"
              name="description"
              value={description}
              className="mt-0"
              multiline
              rows={2}
              maxRows={4}
              notShowIcon={true}
              onChange={handleChange}
              errors={errors}
            />
          </div>
        </div>
        <div className="row justify-content-center mt-4">
          <div className="col-md-3">
            <button
              className="business_btn2"
              onClick={() => props.history.goBack()}
            >
              Back
            </button>
          </div>
          <div className="col-md-3">
            <button onClick={submit}>
              {updateCatId ? "Update Category" : "Add Category"}
            </button>
          </div>
        </div>
      </div>
      <Toaster />
    </>
  );
}
export default withRouter(View);
