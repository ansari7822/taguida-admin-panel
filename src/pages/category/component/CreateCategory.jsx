import React, { useState, useEffect } from "react";
import { withRouter, Link, useParams } from "react-router-dom";
import im1 from "../../../assets/images/uploadImg.png";
import TopNav from "../../../components/topnav/TopNav";
import Axios from "../../../Auth/Axios";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import CoverCrop from "../../../comman/CoverCrop";
import DataURIToBlob from "../../../comman/DataBlob";
import Dropzone from "react-dropzone";

import Select from "@mui/material/Select";

import { TEXTFIELD } from "../../../comman/inputeFields";
import {
  CreateCategory,
  UploadImage,
  GetCategoriesById,
} from "../../../Auth/ApiUrl";
import toast, { Toaster } from "react-hot-toast";
import { checkError } from "../../../Auth/helper";
import Loader2 from "../../../comman/Loader2";
import { Button } from "react-bootstrap";
import { FormHelperText } from "@mui/material";

/**
 * Formats the size
 */
function formatBytes(bytes, decimals = 2) {
  if (bytes === 0) return "0 Bytes";
  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

  const i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}
function View(props) {
  const [disabled, setDisabled] = useState(true);
  const [user, setUser] = useState();
  const [updatecover, setupdatecover] = useState({
    isOpen: false,
    image: null,
  });
  const [state, setState] = useState({
    name: "",
    description: "",
    type: "",
    errors: {
      name: "",
      description: "",
      type: "",
    },
  });
  const [imgUrls, setimgUrls] = useState("");
  const { id } = useParams();

  useEffect(() => {
    if (id) {
      Axios.get(GetCategoriesById + id)
        .then((res) => {
          const response = res.data.data;
          if (res.data.isSuccess == true) {
            setloading(false);
            console.log(response);
            setState({
              ...state,
              name: response.name,
              type: response.type,
              description: response.description,
            });
            setimgUrls(response.imgUrl);
          } else {
            return (
              toast.error(res.data.error, {
                duration: 4000,
                position: "top-right",
              }),
              setloading(false)
            );
          }
        })
        .catch((err) => {
          console.log(err, "87987");
        });
    }
  }, []);
  const handleChange = (e) => {
    e.preventDefault();
    // const { name, value } = e.traget;
    switch (e.target.name) {
      case "name":
        errors.name = e.target.value.length < 1 ? "Name Required" : "";
        break;
      case "description":
        errors.description =
          e.target.value.length < 1 ? "Description Required" : "";
        break;
      case "type":
        errors.type = e.target.value.length < 1 ? "Type Required" : "";
        break;
      default:
        break;
    }
    setState((prev) => ({ ...prev, errors, [e.target.name]: e.target.value }));
  };
  console.log(id, "prps");
  const [loading, setloading] = useState(false);

  const getFile = (e, fd) => {
    // setState({ ...state, imgUpload: e.target.files[0] });
    setloading(true);
    const formData = new FormData();
    // formData.append("media", e.target.files[0]);
    formData.append("media", fd);
    Axios.post(UploadImage, formData)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setloading(false);
          setimgUrls(res.data.data.url);
        } else {
          return (
            toast.error(res.data.error, {
              duration: 4000,
              position: "top-right",
            }),
            setloading(false)
          );
        }
      })
      .catch((err) => {
        console.log(err, "87987");
      });
  };
  // const UploadImageCall = () => {};

  console.log(imgUrls, "imgUrls");
  const submit = () => {
    const { name, description, type } = state;
    let data = {
      name: name,
      imgUrl: imgUrls,
      type: type,
      description: description,
    };
    setloading(true);
    let actionUrl = CreateCategory;
    if (id) {
      Axios.put(GetCategoriesById + id, data)
        .then((res) => {
          if (res.data.isSuccess == true) {
            return (
              (toast.success("Category Updated", {
                duration: 2000,
                position: "top-right",
              }),
              setTimeout(() => {
                props.history.push("/app/categories/list");
              }, 100)),
              setloading(false)
            );
          } else {
            return (
              toast.error(res.data.error, {
                duration: 4000,
                position: "top-right",
              }),
              setloading(false)
            );
          }
        })
        .catch((err) => {
          console.log(err, "87987");
        });
    } else {
      Axios.post(CreateCategory, data)
        .then((res) => {
          if (res.data.isSuccess == true) {
            return (
              (toast.success("Category Added", {
                duration: 2000,
                position: "top-right",
              }),
              setTimeout(() => {
                props.history.push("/app/categories/list");
              }, 100)),
              setloading(false)
            );
          } else {
            return (
              toast.error(res.data.error, {
                duration: 4000,
                position: "top-right",
              }),
              setloading(false)
            );
          }
        })
        .catch((err) => {
          console.log(err, "87987");
        });
    }
  };

  //croper
  const handleAcceptedFiles = (name) => (_files) => {
    const files = _files?.filter((file) => file.size < 5242880);
    if (files.length < _files.length) {
      // return toastr.error(props.t("max_file_size"));
    }
    files.map((file) =>
      Object.assign(file, {
        preview: URL.createObjectURL(file),
        formattedSize: formatBytes(file.size),
      })
    );
    if (name === "coverImage") {
      return setupdatecover((prevState) => ({
        isOpen: !prevState.isOpen,
        image: files[0],
      }));
    }
  };
  const onCoverCrop = (image_base64, data) => {
    const file = DataURIToBlob(image_base64);
    // onUploadFile({ file }, uploadFileSuccess("coverImage"));
    getFile("e", file);
    // setselectedFiles((prevState) => [
    //   prevState[0],
    //   { preview: image_base64 },
    //   prevState[2],
    // ]);
  };
  const { errors, name, imgUrl, description, type } = state;
  return (
    <>
      <CoverCrop
        {...updatecover}
        toggle={(image) =>
          setupdatecover((prevState) => ({ isOpen: !prevState.isOpen, image }))
        }
        onCoverCrop={onCoverCrop}
        imgRatio={4 / 3}
      />
      {loading && <Loader2 />}
      <TopNav
        routeText={"Categories"}
        navigationText={id ? "Update Category" : "Add New Category"}
      />
      <div className="user_view_page mb-3">
        <div className="user_view_header  align-items-center">
          <h5 className="">{id ? "Update Category" : "Add New Category"} </h5>
          <p>Enter the new category details below</p>
        </div>
        <div className="row align-items-center">
          <label className="col-md-4 create_catergory" htmlFor="fileImg">
            {imgUrls ? (
              <img
                src={imgUrls}
                style={{ width: "200px", height: "100px", objectFit: "cover" }}
              />
            ) : (
              <img src={im1} />
            )}
            <span className="text-muted">
              {id ? "Update Image" : "Upload Image"}
            </span>
            {/* <input
              type="file"
              name="image"
              value={imgUrl}
              onChange={getFile}
              id="fileImg"
            /> */}
            <Dropzone
              onDrop={(acceptedFiles) => {
                handleAcceptedFiles("coverImage")(acceptedFiles);
              }}
            >
              {({ getRootProps, getInputProps }) => {
                return (
                  <div className="dropzone-single-image" {...getRootProps()}>
                    <a href="/cover-image" onClick={(e) => e.preventDefault()}>
                      <input
                        {...getInputProps()}
                        id="formrow-profile-image-Input"
                        multiple={false}
                        accept="image/*"
                        id="fileImg"
                      />

                      {/* <div className="edit-iocn">Upload</div> */}
                    </a>
                  </div>
                );
              }}
            </Dropzone>
          </label>
          <div className="col-md-8 mt-0">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Category Name"
              name="name"
              className="mt-0"
              value={name}
              notShowIcon={true}
              onChange={handleChange}
              errors={errors}
            />
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Description"
              name="description"
              value={description}
              className="mt-0"
              multiline
              rows={2}
              maxRows={4}
              notShowIcon={true}
              onChange={handleChange}
              errors={errors}
            />
            <FormControl variant="standard" sx={{ m: 1, minWidth: "100%" }}>
              <InputLabel id="demo-simple-select-standard-label">
                Type
              </InputLabel>
              <Select
                labelId="demo-simple-select-standard-label"
                id="demo-simple-select-standard"
                value={type}
                name="type"
                onChange={handleChange}
                label="Type"
                helo
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={"business"}>Business</MenuItem>
                <MenuItem value={"place"}>Place</MenuItem>
                <MenuItem value={"idea"}>Idea</MenuItem>
              </Select>{" "}
            </FormControl>
          </div>
        </div>
        <div className="row justify-content-center cat_btn_col">
          <div className="col-md-3">
            <button
              className="business_btn2"
              onClick={() => props.history.goBack()}
            >
              Back
            </button>
          </div>
          <div className="col-md-3">
            {!imgUrls ? (
              <button style={{ opacity: "60%" }}>
                {id ? "Updated Category" : "Add Category"}
              </button>
            ) : (
              <button onClick={submit}>
                {id ? "Updated Category" : "Add Category"}
              </button>
            )}
          </div>
        </div>
      </div>
      <Toaster />
    </>
  );
}
export default withRouter(View);
