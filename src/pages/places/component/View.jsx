import React, { useState, useEffect } from "react";
import { withRouter, Link } from "react-router-dom";
import avtar from "../../../assets/images/user_img@2x.png";
import im1 from "../../../assets/images/c_img_1.png";
import TopNav from "../../../components/topnav/TopNav";
import Axios from "../../../Auth/Axios";
import {
  GET_PLACE_ById,
  CreateSubCategory,
  GetSubCategoryById,
} from "../../../Auth/ApiUrl";
import toast, { Toaster } from "react-hot-toast";
import slidImg from "../../../assets/images/slider_img.png";
import { Button, Carousel, Modal } from "react-bootstrap";
export default function View(props) {
  const { id } = props.match.params;
  const [user, setUser] = useState();
  const [userId, setUserId] = useState(null);
  const [smShow, setSmShow] = useState(false);
  const [subCat, setsubcat] = useState();

  const [loading, setloading] = useState(false);
  useEffect(() => {
    setloading(true);
    Axios.get(GET_PLACE_ById + id)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setUser(res.data.data);
        }
        setloading(false);
        console.log(res, "data");
      })
      .catch((err) => {
        setloading(false);
        return toast.success("Something went wrong", {
          duration: 4000,
          position: "top-center",
        });
      });
    let newUrl = CreateSubCategory + `?categoryId=${id}`;
    Axios.get(newUrl)
      .then((res) => {
        if (res.data.isSuccess == true) {
          console.log(res.data, "data");
          setsubcat(res.data.items);
        }
      })
      .catch((err) => {
        console.log(err, "err");
      });
  }, []);

  const DeleteUser = () => {
    setloading(true);
    Axios.delete(GetSubCategoryById + userId)
      .then((res) => {
        if (res.data.isSuccess == true) {
          return (
            toast.success(res.data.message, {
              duration: 3000,
              position: "top-right",
            }),
            setloading(false),
            props.history.push(`/app/categories`)
          );
        } else if (res.data.isSuccess == false) {
          return toast.info(res.data.message, {
            duration: 3000,
            position: "top-right",
          });
        }
        setloading(false);
      })
      .catch((err) => {
        setloading(false);
        return toast.error("Something went wrong", {
          duration: 4000,
          position: "top-riht",
        });
      });
    setSmShow(false);
  };
  const showModel = (action, id) => {
    setSmShow(action);
    setUserId(id);
  };
  return (
    <>
      <TopNav routeText={"Place"} navigationText={"Place Details"} />
      <div className="user_view_page mb-3 ">
        <div className="user_view_header d-flex justify-content-between align-items-center ">
          <h5 className="" style={{ paddingLeft: 12 }}>
            Place{" "}
          </h5>
        </div>
        {user && user && (
          <div className="row m-0 pl-0">
            {user?.placeImages?.map((d, i) => {
              return (
                <>
                  <div className="col-md-3 gap-1 mb-4 pl-0" key={i}>
                    <div className="prefrered_cate">
                      <img src={d.imgUrl || im1} />
                    </div>
                  </div>
                </>
              );
            })}
            {/* <div className="col-md-4 view_catergory mb-3">
              <img src={user?.placeImages[0]?.imgUrl || im1} />
            </div> */}
            <div className="col-md-10 ">
              <h6>{user.name}</h6>
              <p>{user.description || "Description N/A"}</p>
            </div>
          </div>
        )}
      </div>
      <Toaster />
    </>
  );
}
