import React, { useState, useEffect } from "react";
import { withRouter, Link, useParams } from "react-router-dom";
import im1 from "../../../assets/images/uploadImg.png";
import TopNav from "../../../components/topnav/TopNav";
import Axios from "../../../Auth/Axios";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import { Rating } from "react-simple-star-rating";
import Popover from "@mui/material/Popover";
import editIcon from "../../../assets/images/ic_cross.svg";
import im12 from "../../../assets/images/uploadImg.png";
import Select from "@mui/material/Select";
import CoverCrop from "../../../comman/CoverCrop";
import DataURIToBlob from "../../../comman/DataBlob";
import Dropzone from "react-dropzone";

import { TEXTFIELD, GoogleAutocomplete } from "../../../comman/inputeFields";
import {
  Add_Place,
  UploadImage,
  GET_PLACE_ById,
  GetCategories,
} from "../../../Auth/ApiUrl";
import toast, { Toaster } from "react-hot-toast";
import { checkError } from "../../../Auth/helper";
import Loader2 from "../../../comman/Loader2";
import { Button } from "react-bootstrap";
import { FormHelperText } from "@mui/material";

/**
 * Formats the size
 */
function formatBytes(bytes, decimals = 2) {
  if (bytes === 0) return "0 Bytes";
  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

  const i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}
let addButton = true;
function View(props) {
  const [disabled, setDisabled] = useState(true);
  const [allImages, setallImages] = useState([]);
  const [updatecover, setupdatecover] = useState({
    isOpen: false,
    image: null,
  });
  const [state, setState] = useState({
    name: "",
    description: "",
    email: "",
    phone: "",
    instagramUrl: "",
    facebookUrl: "",
    openingHours: "",
    thkName: "",
    thkDescription: "",
    thkImg: "",
    type: [],
    ImagePath: "",
    things: [],
    address: "",
    htName: "",
    htDescription: "",
    htImg: "",
    categoryId: "",
    lat: "",
    lng: "",
    tripType: "",
    location: "Mohali",
    locationCoordinates: [],
    images: [],
    errors: {
      name: "",
      address: "",
      description: "",
      type: "",
    },
  });
  const [thingsList, setthingsList] = useState([]);
  const [imgUrls, setimgUrls] = useState("");
  const [thingsMain, setthingsMain] = useState([]);
  const [hotelsMain, sethotelsMain] = useState([]);
  const { id } = useParams();
  const [rating, setRating] = useState(0); // initial rating value
  const [anchorEl, setAnchorEl] = useState(null);
  const [anchorEl2, setAnchorEl2] = useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClick2 = (event) => {
    setAnchorEl2(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleClose2 = () => {
    setAnchorEl2(null);
  };
  const open = Boolean(anchorEl);
  const open2 = Boolean(anchorEl2);
  const popid = open ? "simple-popover" : undefined;
  const popid2 = open2 ? "simple-popover" : undefined;

  useEffect(() => {
    let getImages = [];
    if (id) {
      Axios.get(GET_PLACE_ById + id)
        .then((res) => {
          const response = res.data.data;
          if (res.data.isSuccess == true) {
            for (let i = 0; i < response?.placeImages.length; i++) {
              getImages.push(response?.placeImages[i].imgUrl);
              setallImages(getImages);
            }
            setloading(false);
            setState({
              ...state,
              name: response.name,
              categoryId: response.categoryId,
              address: response.location,
              lng: response.locationCoordinates[0] || 0,
              lat: response.locationCoordinates[1] || 0,
              description: response.description,
              tripType: response.tripType,
            });
            sethotelsMain([...response.placeHotels]);
            setthingsMain([...response.placeThings]);

            // setallImages([response?.placeImages[0]?.imgUrl]);
            setimgUrls(response.imgUrl);
          } else {
            return (
              toast.error(res.data.error, {
                duration: 4000,
                position: "top-right",
              }),
              setloading(false)
            );
          }
        })
        .catch((err) => {
          console.log(err, "87987");
        });
    }
  }, []);

  // Catch Rating value
  const handleRating = (rate) => {
    setRating(rate);
    // other logic
  };
  console.log(hotelsMain, "reating");
  const handleChange = (e) => {
    e.preventDefault();
    // const { name, value } = e.traget;
    switch (e.target.name) {
      case "name":
        errors.name = e.target.value.length < 1 ? "Name Required" : "";
        break;
      case "description":
        errors.description =
          e.target.value.length < 1 ? "Description Required" : "";
        break;
      case "categoryId":
        errors.categoryId = e.target.value.length < 1 ? "Type Required" : "";
        break;
      case "thkName":
        errors.thkName = e.target.value.length < 1 ? "Name Required" : "";
        break;
      case "thkDescription":
        errors.thkDescription =
          e.target.value.length < 1 ? "Description Required" : "";
        break;
      case "thkImg":
        errors.thkImg = e.target.value.length < 1 ? "Image Required" : "";
        break;
      default:
        break;
    }
    setState((prev) => ({ ...prev, errors, [e.target.name]: e.target.value }));
  };
  const [loading, setloading] = useState(false);

  useEffect(() => {
    Axios.get(GetCategories)
      .then((res) => {
        if (res.data.isSuccess == true) {
          // setUsersList(res.data.items);
          let data = res.data.items.filter((d, i) => d.type == "place");
          if (data) {
            let type = data.map((d) => {
              return {
                [d.id]: d.name,
              };
            });
            setState((prev) => ({ ...prev, type: type }));
          }

          console.log(data, "data");
        }
      })
      .catch((err) => console.log("err"));
  }, [state.categoryId]);

  const getFile = (e, fd) => {
    // setState({ ...state, imgUpload: fd });
    setloading(true);
    const formData = new FormData();

    formData.append("media", fd);
    Axios.post(UploadImage, formData)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setallImages([...allImages, res.data.data.url]);
          setimgUrls(res.data.data.url);
          setloading(false);
        } else {
          // toast.error(res.data.error, {
          //   duration: 4000,
          //   position: "top-right",
          // }),
          return setloading(false);
        }
      })
      .catch((err) => {
        console.log(err, "87987");
      });
  };
  const getFile2 = (e) => {
    setloading(true);
    const formData = new FormData();
    formData.append("media", e.target.files[0]);
    setState((prev) => ({ ...prev, ImagePath: e.target.files[0] }));
    Axios.post(UploadImage, formData)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setState((prev) => ({
            ...prev,
            ...allImages,
            thkImg: res.data.data.url,
          }));
          setloading(false);
        } else {
          return setloading(false);
        }
      })
      .catch((err) => {
        console.log(err, "87987");
      });
  };
  const getFile3 = (e) => {
    setloading(true);
    const formData = new FormData();
    formData.append("media", e.target.files[0]);
    Axios.post(UploadImage, formData)
      .then((res) => {
        if (res.data.isSuccess == true) {
          setState((prev) => ({
            ...prev,
            htImg: res.data.data.url,
          }));
          setloading(false);
        } else {
          return setloading(false);
        }
      })
      .catch((err) => {
        console.log(err, "87987");
      });
  };
  // const UploadImageCall = () => {};

  console.log(allImages, "imgUrls");
  const submit = () => {
    const { name, description, address, lat, lng, tripType } = state;
    // let ctgid;
    // for (let i in categoryId) {
    //   ctgid = Number(i);
    // }
    let data = {
      name: name,
      images: allImages,
      categoryId: categoryId,
      location: address,
      // placeThings: thingsMain,
      // placeHotels: hotelsMain,
      things: thingsMain,
      hotels: hotelsMain,
      tripType: tripType,
      locationCoordinates: [lng, lat],
      description: description,
    };

    setloading(true);

    if (id) {
      Axios.put(GET_PLACE_ById + id, data)
        .then((res) => {
          if (res.data.isSuccess == true) {
            return (
              (toast.success("Place Updated", {
                duration: 2000,
                position: "top-right",
              }),
              setTimeout(() => {
                props.history.push("/app/places/list");
              }, 100)),
              setloading(false)
            );
          } else {
            return (
              toast.error(res.data.error, {
                duration: 4000,
                position: "top-right",
              }),
              setloading(false)
            );
          }
        })
        .catch((err) => {
          console.log(err, "87987");
        });
    } else {
      Axios.post(Add_Place, data)
        .then((res) => {
          if (res.data.isSuccess == true) {
            return (
              (toast.success("Place Added", {
                duration: 2000,
                position: "top-right",
              }),
              setTimeout(() => {
                props.history.push("/app/places/list");
              }, 100)),
              setloading(false)
            );
          } else {
            return (
              toast.error(res.data.error, {
                duration: 4000,
                position: "top-right",
              }),
              setloading(false)
            );
          }
        })
        .catch((err) => {
          console.log(err, "87987");
        });
    }
  };

  const handleAddItem = (event) => {
    const { thkName, thkDescription, thkImg } = state;
    event.preventDefault();
    let data = {
      name: thkName,
      description: thkDescription,
      imgUrl: thkImg,
    };
    setthingsMain((prev) => [...prev, data]);
    setState((prev) => ({
      ...prev,
      thkName: "",
      thkDescription: "",
      thkImg: "",
    }));
  };
  const handleHotelAddItem = (event) => {
    const { htName, htDescription, htImg } = state;
    event.preventDefault();
    let data = {
      name: htName,
      description: htDescription,
      imgUrl: htImg,
    };
    sethotelsMain((prev) => [...prev, data]);
    setState((prev) => ({
      ...prev,
      htName: "",
      htDescription: "",
      htImg: "",
    }));
  };
  const deleteThings = (valueOfindex) => {
    let filterd = thingsMain.filter((d, i) => i !== valueOfindex);
    console.log(filterd);
    setthingsMain(filterd);
  };
  const deleteHotels = (valueOfindex) => {
    let filterd = hotelsMain.filter((d, i) => i !== valueOfindex);
    console.log(filterd);
    sethotelsMain(filterd);
  };
  const handleAddress = (address) => {
    let errors = state.errors;
    console.log(address, errors);
    let add = address;
    errors.address = address.address < 1 ? "Required" : "";
    add.errors = errors;
    setState({ ...state, ...add });
    console.log(add, 123);
  };
  const removeImage = (i) => {
    let filterdata = allImages.filter((d) => d !== i);
    setallImages(filterdata);
  };
  const scrlltoLeft = () => {
    setTimeout(() => {
      const flavoursContainer = document.getElementById("neid");
      flavoursContainer.scrollLeft += 3000;
    }, 1000);
  };
  useEffect(() => {
    scrlltoLeft();
  }, [allImages]);
  const {
    errors,
    name,
    imgUrl,
    description,
    categoryId,
    address,
    type,
    thkName,
    thkDescription,
    thkImg,
    lat,
    things,
    lng,
    htName,
    htDescription,
    htImg,
    ImagePath,
    tripType,
  } = state;
  useEffect(() => {
    // if (thkName && thkDescription && thkImg) {
    //   addButton = false;
    // }
  }, [state]);
  //croper
  const handleAcceptedFiles = (name) => (_files) => {
    const files = _files?.filter((file) => file.size < 5242880);
    if (files.length < _files.length) {
      // return toastr.error(props.t("max_file_size"));
    }
    files.map((file) =>
      Object.assign(file, {
        preview: URL.createObjectURL(file),
        formattedSize: formatBytes(file.size),
      })
    );
    if (name === "coverImage") {
      return setupdatecover((prevState) => ({
        isOpen: !prevState.isOpen,
        image: files[0],
      }));
    }
  };
  const onCoverCrop = (image_base64, data) => {
    const file = DataURIToBlob(image_base64);
    getFile("e", file);
  };
  return (
    <>
      <CoverCrop
        {...updatecover}
        toggle={(image) =>
          setupdatecover((prevState) => ({ isOpen: !prevState.isOpen, image }))
        }
        onCoverCrop={onCoverCrop}
        imgRatio={4 / 3}
      />
      {loading && <Loader2 />}
      <TopNav
        routeText={"Place"}
        navigationText={id ? "Update Place" : "Add Place"}
      />
      <div className="user_view_page mb-3">
        <div
          className="user_view_header  align-items-center"
          style={{ paddingBottom: 18 }}
        >
          <h5 className="">{id ? "Update Place" : "Add New Place"} </h5>
          <p>Enter the new Place details below</p>
        </div>
        <div
          className="d-flex align-items-center m-0 businessImageScroll"
          id="neid"
        >
          {allImages &&
            allImages.map((d, i) => {
              return (
                <div
                  className="col-md-3 position-relative "
                  style={{ marginRight: 20 }}
                  key={i}
                >
                  <>
                    <img
                      src={editIcon}
                      onClick={() => removeImage(d)}
                      // onClick={() => alert("yes")}
                      className="delete_icon_img"
                      style={{ width: 30, height: 30, objectFit: "contain" }}
                    />
                    <label
                      className=" create_catergory2"
                      htmlFor="fileImg"
                      style={{ borderRadius: "9px" }}
                    >
                      {/* {loading && <Loader2 color="yellow" />} */}
                      {imgUrls || allImages ? (
                        <img
                          src={allImages[i]}
                          style={{
                            width: "200px",
                            height: "100px",
                            objectFit: "cover",
                          }}
                        />
                      ) : (
                        <img src={im12} />
                      )}

                      {/* <input
                        type="file"
                        name="image"
                        // value={imgUrls}
                        onChange={getFile}
                        id="fileImg"
                      /> */}
                    </label>
                  </>
                </div>
              );
            })}
          {allImages.length !== 10 && (
            <div className="col-md-3 gap-1">
              <>
                <label className=" create_catergory2" htmlFor="fileImg">
                  {/* {loading && <Loader2 color="yellow" />} */}
                  <img src={im12} />
                  <span className="text-muted">Upload Image</span>
                  {/* <input
                    type="file"
                    name="image"
                    onChange={getFile}
                    id="fileImg"
                  /> */}
                  <Dropzone
                    onDrop={(acceptedFiles) => {
                      handleAcceptedFiles("coverImage")(acceptedFiles);
                    }}
                  >
                    {({ getRootProps, getInputProps }) => {
                      return (
                        <div
                          className="dropzone-single-image"
                          {...getRootProps()}
                        >
                          <a
                            href="/cover-image"
                            onClick={(e) => e.preventDefault()}
                          >
                            <input
                              {...getInputProps()}
                              id="formrow-profile-image-Input"
                              multiple={false}
                              accept="image/*"
                              id="fileImg"
                            />
                          </a>
                        </div>
                      );
                    }}
                  </Dropzone>
                </label>
              </>
            </div>
          )}
        </div>
        <div className="row form_row">
          <div className="col-md-4 mt-0">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Place Name"
              name="name"
              className="mt-0"
              value={name}
              notShowIcon={true}
              onChange={handleChange}
              errors={errors}
            />
          </div>

          <div className="col-md-4">
            <GoogleAutocomplete
              label="Address"
              address={address}
              InputLabelProps={{
                shrink: true,
              }}
              id="standard-error-helper-text"
              lat={lat}
              lng={lng}
              style={{ marginTop: "8px" }}
              errors={errors}
              name="address"
              onChange={handleAddress}
            />
          </div>

          <div className="col-md-4">
            <FormControl variant="standard" sx={{ m: 1, minWidth: "100%" }}>
              <InputLabel id="demo-simple-select-standard-label">
                Type
              </InputLabel>
              <Select
                labelId="demo-simple-select-standard-label"
                id="demo-simple-select-standard"
                value={categoryId}
                name="categoryId"
                onChange={handleChange}
                label="Category Type"
                InputLabelProps={{
                  shrink: true,
                }}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {type &&
                  type.length > 0 &&
                  type.map((d, i) => {
                    for (let dd in d) {
                      return (
                        <MenuItem value={dd}>
                          <em>{d[dd]}</em>
                        </MenuItem>
                      );
                    }
                  })}
              </Select>{" "}
            </FormControl>
          </div>
          <div className="col-md-4">
            <FormControl variant="standard" sx={{ m: 1, minWidth: "100%" }}>
              <InputLabel id="demo-simple-select-standard-label">
                Trip Type
              </InputLabel>
              <Select
                labelId="demo-simple-select-standard-label"
                id="demo-simple-select-standard"
                value={tripType}
                name="tripType"
                onChange={handleChange}
                label="Trip Type"
                InputLabelProps={{
                  shrink: true,
                }}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value="Romantic">
                  <em>Romantic </em>
                </MenuItem>
                <MenuItem value="Family holiday">
                  <em>Family holiday</em>
                </MenuItem>
                <MenuItem value="Solo Trips">
                  <em>Solo Trips </em>
                </MenuItem>
                <MenuItem value="Group Holiday">
                  <em>Group Holiday </em>
                </MenuItem>
              </Select>{" "}
            </FormControl>
          </div>

          <div className="col-md-8">
            <TEXTFIELD
              id="standard-error-helper-text"
              label="Description"
              name="description"
              value={description}
              className="mt-0"
              multiline
              rows={2}
              maxRows={4}
              notShowIcon={true}
              onChange={handleChange}
              errors={errors}
            />
          </div>
          <div className="col-md-12 things">
            <div className="row">
              <div className="col-md-12 py-2 " style={{ paddingLeft: "20px" }}>
                <b className="text-muted pl-2">Things</b>
              </div>
              <div className="col-md-3">
                <TEXTFIELD
                  id="standard-error-helper-text"
                  label="Name"
                  name="thkName"
                  value={thkName}
                  className="mt-0"
                  notShowIcon={true}
                  onChange={handleChange}
                  errors={errors}
                />
              </div>
              <div className="col-md-3">
                <TEXTFIELD
                  id="standard-error-helper-text"
                  label="Description"
                  name="thkDescription"
                  value={thkDescription}
                  multiline
                  rows={2}
                  maxRows={4}
                  className="mt-0"
                  notShowIcon={true}
                  onChange={handleChange}
                  errors={errors}
                />
              </div>
              <div className="col-md-2 d-flex align-items-center">
                <div className="">
                  <label
                    className="col-md-12 upload_video upload_btn"
                    htmlFor="fileVideo"
                  >
                    {/* <span>Upload Image</span> */}
                    <i className={"bx bx-plus"}></i>
                    <input
                      type="file"
                      name="image"
                      onChange={getFile2}
                      id="fileVideo"
                    />
                  </label>
                  <small>{thkImg.slice(0, 20)}</small>
                </div>
                {/* <TEXTFIELD
                  id="standard-error-helper-text"
                  label="Image"
                  name="image"
                  type="file"
                  value={ImagePath}
                  notShowIcon={true}

                /> */}
              </div>
              <div className="col-md-2 d-flex align-items-center">
                <button
                  // style={{ cursor: addButton ? "not-allowed" : "pointer" }}
                  classname="business_btn"
                  onClick={handleAddItem}
                >
                  Add
                </button>
              </div>
            </div>
            {thingsMain.map((dk, i) => {
              return (
                <div className="row" key={i}>
                  <div className="col-md-3">
                    <TEXTFIELD
                      id="standard-error-helper-text"
                      label="Name"
                      disabled
                      value={dk.name}
                      className="mt-0"
                      notShowIcon={true}
                    />
                  </div>
                  <div className="col-md-3">
                    <TEXTFIELD
                      id="standard-error-helper-text"
                      label="Description"
                      name="thkDescription"
                      value={dk.description}
                      className="mt-0"
                      disabled
                      notShowIcon={true}
                    />
                  </div>
                  <div className="col-md-2 ">
                    <div className="">
                      <label
                        className="col-md-12 upload_video upload_btn"
                        htmlFor="fileImage"
                      >
                        <i className={"bx bx-file"}></i>
                        <input
                          type="file"
                          disabled
                          name="image"
                          id="fileImage"
                        />
                      </label>
                      <small>{dk.imgUrl.slice(0, 20)}</small>
                    </div>
                  </div>
                  <div className="col-md-2">
                    <button
                      classname="business_btn"
                      onClick={() => deleteThings(i)}
                    >
                      <i className="bx bx-trash"></i>
                    </button>
                  </div>
                </div>
              );
            })}
          </div>
          <div className="col-md-12 hotel">
            <div className="row">
              <div className="col-md-12 py-2" style={{ paddingLeft: "20px" }}>
                <b className="text-muted pl-2">Hotels</b>
              </div>
              <div className="col-md-3">
                <TEXTFIELD
                  id="standard-error-helper-text"
                  label="Name"
                  name="htName"
                  value={htName}
                  onChange={handleChange}
                  className="mt-0"
                  notShowIcon={true}
                  errors={errors}
                />
              </div>
              <div className="col-md-3">
                <TEXTFIELD
                  id="standard-error-helper-text"
                  label="Description"
                  name="htDescription"
                  value={htDescription}
                  className="mt-0"
                  multiline
                  rows={2}
                  maxRows={4}
                  notShowIcon={true}
                  onChange={handleChange}
                  errors={errors}
                />
              </div>
              <div className="col-md-2 d-flex align-items-center">
                <div className="">
                  <label
                    className="col-md-12 upload_video upload_btn"
                    htmlFor="fileI"
                  >
                    <i className={"bx bx-plus"}></i>
                    <input
                      type="file"
                      name="image"
                      onChange={getFile3}
                      id="fileI"
                    />
                  </label>
                  <small>{htImg.slice(0, 20)}</small>
                </div>
              </div>
              <div className="col-md-2 d-flex align-items-center">
                <button
                  // style={{ cursor: addButton ? "not-allowed" : "pointer" }}
                  classname="business_btn"
                  onClick={handleHotelAddItem}
                >
                  {" "}
                  Add
                </button>
              </div>
            </div>
            {hotelsMain.map((dk, i) => {
              return (
                <div className="row" key={i}>
                  <div className="col-md-3">
                    <TEXTFIELD
                      id="standard-error-helper-text"
                      label="Name"
                      value={dk.name}
                      className="mt-0"
                      disabled
                      notShowIcon={true}
                    />
                  </div>
                  <div className="col-md-3">
                    <TEXTFIELD
                      id="standard-error-helper-text"
                      label="Description"
                      name="thkDescription"
                      value={dk.description}
                      className="mt-0"
                      disabled
                      notShowIcon={true}
                    />
                  </div>
                  <div className="col-md-2">
                    <div className="">
                      <label
                        className="col-md-12 upload_video upload_btn"
                        htmlFor="fileVideo"
                      >
                        <i className={"bx bx-file"}></i>
                        <input
                          type="file"
                          disabled
                          name="image"
                          id="fileVideo"
                        />
                      </label>
                      <small>{dk.imgUrl.slice(0, 20)}</small>
                    </div>
                  </div>
                  <div className="col-md-2">
                    <button
                      classname="business_btn"
                      onClick={() => deleteHotels(i)}
                    >
                      <i className={"bx bx-trash"}></i>
                    </button>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <div className="row justify-content-center cat_btn_col2">
          <div className="col-md-3">
            <button
              className="business_btn2"
              onClick={() => props.history.goBack()}
            >
              Back
            </button>
          </div>
          <div className="col-md-3">
            {allImages.length < 1 ? (
              <button style={{ opacity: "60%" }}>
                {id ? "Updated Place" : "Add Place"}
              </button>
            ) : (
              <button onClick={submit}>
                {id ? "Updated Place" : "Add Place"}
              </button>
            )}
          </div>
        </div>
      </div>
      <Toaster />
    </>
  );
}
export default withRouter(View);
