import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import NotFoundPage from "../../comman/NotFoundPage";
import Category from "./component/Category";
import View from "./component/View";
import CreateCategory from "./component/CreateCategory";
import CreateSubCategory from "./component/CreateSubCategory";
const Categorys = ({ match }) => (
  <Switch>
    <Redirect exact from={`${match.url}`} to={`${match.url}/list`} />
    <Route path={`${match.url}/list`} component={Category} />
    <Route path={`${match.url}/view/:id`} component={View} />
    <Route
      path={`${match.url}/places/:subCatId`}
      component={CreateSubCategory}
    />
    <Route
      path={`${match.url}/updated-subcat/:updateCatId`}
      component={CreateSubCategory}
    />
    <Route path={`${match.url}/create-category`} component={CreateCategory} />
    <Route path={`${match.url}/updated/:id`} component={CreateCategory} />
    <Route component={NotFoundPage} />
  </Switch>
);

export default Categorys;
